import React, { Component } from "react"
import { connect } from "react-redux"
import Promise from "promise"
import _ from "lodash"
import NotificationSystem from 'react-notification-system';

//import { initializeGames, saveGames, elementOverflow } from "../actions/ete/admin"


var timeOut

class ImoDirectory extends Component {
    
    notificationSystem: null

    constructor(props) {
        super();

        this.state = {
            inserted:false,
            coins:0,
            active:false,
            perfect:false,
            complete:false,
			start:true,
            zoom:20,
            selected:[],
            match:[],
            imgSize:{},
			element:{}
        };
        
        this.actionStart = this.actionStart.bind(this)
		
        this.actionSetOption = this.actionSetOption.bind(this)

        this.actionSetMatch = this.actionSetMatch.bind(this)

        //this.actionDeleteProduct = this.actionDeleteProduct.bind(this);
        //this.deleteProduct = this.deleteProduct.bind(this);
        //this.addNotification = this.addNotification.bind(this);

        //console.log('Constructor', props)
		
		setTimeout(()=>{ 
			document.documentElement.scrollTop = document.getElementById('display-content').getBoundingClientRect().top
		}, 2000)
		
		//console.log(document.getElementById('display-content').getBoundingClientRect().top)
		
		//console.log(contentDisplay)
    }

    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*const params = {
                limit : this.state.limitProducts,
                json: 1
            }
        this.props.dispatch( { type:'' } );
        this.props.dispatch(getProducts(params));*/

        //console.log('Component will mount')

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        _this.notificationSystem = _this.refs.notificationSystem;

        //_this.props.dispatch(initializeGames());
        
		//_this.props.dispatch(elementOverflow());
        //console.log('Component Did Mount');
    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        const _this = this

        const { games } = nextProps;
        
        if(games.game) {

            let { element, coins, active, sets } = games.game
			
			coins = (_this.state.coins > coins) ? _this.state.coins : coins
			
			let complete = ( active >= sets.length ) ? true  : false
			
            _this.setState({..._this.state, coins, element:games.element, complete})

        }
        
        //this.setState({...this.state})
        //this.setState( {test:'dkjasndkas'} );
    }

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }


    callAudio( type, time=5000) {

        const audio = new Audio('/audio/'+type);
              audio.play();

        setTimeout(()=>{

            audio.pause()

        }, time)
        
    }
    
    stateChange( state ) {
        
        this.setState( _.merge( this.state, state ) )

    }
	
	/*
	 * Start Game
	 * */
	actionStart(e) {
		
		const _this = this
		
		_this.setState({..._this.state, start:false})
		
	}	
	
	/*
	 * Select Option 
	 * */
    actionSetOption(e) {
		
		clearTimeout(timeOut)
		
        const _this = this

        const _params = {
            key : parseInt(e.currentTarget.getAttribute('data-key'))
        }
        
        let selected = []

        if(_this.state.complete) {
            return;
        } else if(  _.isEmpty(_.intersection(_this.state.match, [_params.key])) )  {
			
/*if( _.isEmpty(_.intersection(_this.state.selected,[_params.key] )) ) {
}*/
/*_.remove(_this.state.selected, function(n) {
    return ( (n == _params.key) && !_.isEmpty(_.intersection(selected,[_params.key] )) )
});*/
//console.log(_.remove([1,2,3], (n)=>{ return n!=2 }))
//selected = _.concat(_this.state.selected, _params.key)


            selected = _.isEmpty(_.intersection(_this.state.selected,[_params.key] )) ? _.concat(_this.state.selected, _params.key) : _.remove( _this.state.selected, (n)=>{ return n != _params.key } )
            selected = _.takeRight(selected, 2)

            _this.callAudio( 'flip.mp3' )

			
			return _this.setState( {...this.state, selected, zoom:20},  ()=>{
				timeOut = setTimeout(()=>{
					return _this.setState({...this.state, zoom:false}, ()=>{
						timeOut = setTimeout(()=>{
							return _this.setState({...this.state, selected:[]})
						},10000)
					})
				}, 5000)
			} ) 
		

        }



    }

    actionSetMatch(e) {
		
		clearTimeout(timeOut)
		
        const _this = this

        const state = this.state
		
		let coins = this.state.coins
		
		let inserted = false
		
        let game = this.props.games.game

        let active = (state.active) ? state.active : game.active
            active = (active > game.sets.length) ? game.sets.length : active
	
		
        if(game.name == 'pair') {

            let answer = game.sets[active].answer 
			
			let options = game.sets[active].options
			
            let match = state.match

            let paired = false; 

            _.forEach(answer, (value, key)=>{
                if(_.isEqual(answer[key], state.selected)) {
                    match = _.concat(match, value)
                    paired = true
                } else if( _.isEqual(_.reverse(answer[key]), state.selected) ) {
					match = _.concat(match, value)
					paired = true
				}
            })
            
//console.log(!paired)
//(_.isEqual(_.flattenDeep(answer), match))
//let perfect = (_.flattenDeep(answer).length == match.length) ? true : false

            let next = (parseInt(active + 1) >= game.sets.length) ? 'FINISH' : parseInt(active + 1)

            let perfect = (_.difference(_.flattenDeep(answer), match).length < 1) ? true : false
				
				match = (perfect) ? _.times(options.length) : match
			
			if( !_.isEmpty(match) && (match.length % 4)==0 && _this.state.selected.length > 1 ) {
				
				_this.callAudio( 'drop-coin.mp3' )
				coins = parseInt(state.coins + 1)
				inserted = true
			
			}
			
			if( _this.state.selected.length < 2  ) {
				
				return
				
			} else if(!paired) {

                _this.callAudio( 'fail.mp3' )

                return _this.setState({...state, selected:[]})

            }  else if((next == 'FINISH') && perfect) {

				return _this.setState( {...state, match, coins, inserted, perfect, complete:true},()=>{
					
						let params = {
							coins,
							active: parseInt(active + 1),
							status:'finished'
						}
						//_this.props.dispatch(saveGames( params ))
					})
				

            } else if(perfect) {
 
                _this.callAudio( 'perfect.wav' )
				
				return _this.setState({...state, match, perfect, inserted }, ()=>{
                   
					timeOut = setTimeout(()=>{
                        
						return _this.setState({...state,
										match:[],
										selected:[], 
										coins,
										inserted:false,
										active:parseInt(active + 1)
                                    }, ()=>{
											
								//_this.props.dispatch(elementOverflow())
								
								let params = {
										coins,
										active: parseInt(active + 1),
										status:'unfinished'
									}
								//_this.props.dispatch(saveGames( params ))
						})
                    }, 5000)
                })
            }


            _this.callAudio( 'paired.wav' )
			
				
			return _this.setState({..._this.state, match, coins, inserted}, ()=>{
					
					timeOut = setTimeout(()=>{
						return _this.setState({..._this.state, inserted:false})
					}, 2000)
					
				})
        
//return _this.setState({...state, match, coins:parseInt(state.coins + 1)})
//console.log(answer);
//console.log(_.intersection(answer[0], state.selected));

        }
        
    }


    render() {
		
		const state = this.state

        let game = this.props.games.game

//console.log(state, this.props)

        let wrapperClass = (state.perfect) ? 'match-perfect' : ''
			
			//wrapperStyle = (state.perfect) ? { backgroundImage: "url()" } : {}
			
        let gamePlayed, echoCoins, piggyBank, overlay

		let stickyClass = ''
		

		if( state.element.piggyBankPosition ) {
			if( (state.element.piggyBankPosition.top > window.innerHeight+50) )
				stickyClass = 'sticky'
			//console.log( state.element.piggyBankPosition.bottom, window.innerHeight+50 )
		}
			
		
        if(game.sets) {
			
			/*
			 * Overlay Messages
			 * */
			overlay = (()=>{
					
					let completeClass = (state.complete) ? 'complete' : '';
					
//console.log(  (state.complete || (game.active >= game.sets.length)), completeClass )
					
					let contentBox = (()=>{
							
							if( state.complete ) {
								
								this.callAudio( 'perfect.wav', 20000 )
								
								return <div className="col-4 offset-4 box-message">
									<p>You have successfully completed the pair games. <br />
										<a href="/liberty-index">
											Click here to evaluate your winning echos.
										</a>
									</p>
									<p>
										<img className="col-4" src="/img/celebration-1.gif" />
										<img className="col-4" src="/img/celebration-3.gif" />
										<img className="col-4" src="/img/celebration-2.gif" />
									</p>
								</div>
							} else if( state.start) {
								return <div className="col-4 offset-4 box-message">
										<p className="text-center">Press <button class="btn btn-match btn-small">Match</button></p>
										<p>when you find a pair of cards that share the same side of an argument.</p>
										<p className="text-center"><button onClick={this.actionStart} class="btn btn-danger btn-lg">Start Now!</button></p>    	
									</div>
							}
							
						})()
					
					
					if( state.start || state.complete ) {
						return <div className={ "overlay " + completeClass } >
									{ contentBox }
							   </div>
					}
					
				})()
			
			
            /*
             * Coins
             */
            echoCoins = (()=>{
				
				let coins = _.padStart(state.coins, 2, 0)
					
				let coinBank = (()=>{
								if(stickyClass == '') {
									return <a href="/liberty-index">Number of 
										<img src="https://escapetheecho.org/templates/frontend/css/images/echoo-logo.png" className="img-echoo-logo" alt="Echo" style={ {marginTop:'13px'} }/> s : 
										<span className="echoTotalWrapper coin-bank"> { coins } </span>
									</a>
								}
								})()

                return <div className="col-md-6 offset-md-6 echos text-right">
							{ coinBank }
                            <a href="#">SAVE my 
                                <img src="https://escapetheecho.org/templates/frontend/css/images/echoo-logo.png" className="img-echoo-logo" alt="Echo" /> s ?
                            </a>
                            <a href="#">What are  
                                <img src="https://escapetheecho.org/templates/frontend/css/images/echoo-logo.png" className="img-echoo-logo" alt="Echo" /> s ?
                            </a>
                    </div>
            })()

            
            /*
             * Games Played
             */
            gamePlayed = (()=>{
                
				if(game && !state.complete) {
                    
                    let matchingWrapper = document.getElementById('matching-wrapper')

                    let screenWidth = matchingWrapper.clientWidth

                    let active = (state.active) ? state.active : game.active
                        active = (active > game.sets.length) ? game.sets.length : active

                    let setLength = game.sets[active].options.length
					
					let division = setLength
					
						if(setLength > 8) {
							division = 8
						} else if(screenWidth <= 1024) {
							division = 6
						}
						
//let division = (setLength >= 8) ? 8 : setLength
//division = (setLength >= 24) ? 11 : division
//division = (setLength >= 32) ? 10 : division
// division = (screenWidth <= 1024 ) ? 6 : division

				
//console.log(screenWidth, division)

                    let boxWidth = Math.floor( (screenWidth / division) - 4)

//boxWidth = Math.floor(boxWidth - (game.sets[active].options.length))
//boxWidth = (boxWidth < 100) ? 100 : boxWidth 

                        boxWidth = (boxWidth > 200) ? 200 : boxWidth 

//console.log(boxWidth, (game.sets[active].options.length > 8) ? 8 : game.sets[active].options.length);
    
                    let boxHeight = Math.floor(478/310 * (boxWidth)) //(478/310) image ratio; 

                    let zoomPercent = state.zoom; 

                    let zoom = {
                        width: Math.floor(boxWidth + boxWidth*( zoomPercent/100 )),
                        height: Math.floor(boxHeight + boxHeight*( zoomPercent/100)),
                        top: -( boxWidth*( zoomPercent/100 ) )/2,
                        left: -( boxHeight*( zoomPercent/100) )/2
                    }
        

                    let modulus = ( game.sets[active].options.length <=8 ) ? Math.ceil((game.sets[active].options.length)/2) : 0; 

                    return game.sets[active].options.map( (image, i)=> {
                        
                        let contClass = (_.indexOf(state.selected, i) != '-1') ? 'selected' : ''
                            
                            contClass = (_.indexOf(state.match, i) != '-1') ? 'match' : contClass

                        let contStyle = { width:boxWidth, height:boxHeight }
							contStyle.backgroundImage = (_.indexOf(state.match, i) != -1) ? "url("+game.sets[active].bg + i +".jpg)" : ""
						
						let frontStyle = { width:boxWidth, height:boxHeight, backgroundImage: "url("+game.sets[active].giphy+")",  }
						let backStyle = { backgroundImage: "url("+image+")", width:boxWidth, height:boxHeight }
							
                        if(_.last(state.selected) == i) {

                            backStyle.width = zoom.width
                            backStyle.height = zoom.height
                            backStyle.top = zoom.top
                            backStyle.left = zoom.left

                            contStyle.zIndex = 9
                            contClass = contClass + ' zoom'
                        }

//console.log(_.last(state.selected), i, backStyle);
//let divClear = (modulus%i == 0) ? '<div className="clear"></div>' : ''

						if(image == "break")
							return <div className="clearfix"></div>
							
                        return <div className={ "flip-container "+contClass } style={ contStyle } onClick={this.actionSetOption} data-key={i}>
                                    <div className="flipper">
                                        <div className="front" style={ frontStyle }>
                                        </div>
                                        <div className="back" style={ backStyle }>
                                        </div>
                                    </div>
                                </div>
					})
				
				} else {
					
					return 	<div>
								<img class="loader" src="https://escapetheecho.org/images/loader.gif" />
							</div>
				}
				
			})();
            

            /*
             * Piggy Bank
             */
             piggyBank = (()=>{
				
				let imageUrl = (state.inserted) ? '/img/piggy-bank.gif' : '/img/piggy-bank.png'
				
                return <div className={ "col-2 piggy-bank " + stickyClass } >
                            <img src={ imageUrl } id="piggy-bank" />
							
							
							{
								(()=>{
									if(stickyClass) {
										
									let coins = _.padStart(state.coins, 2, 0)
									
									return  <div>
												<button className="btn btn-match" onClick={this.actionSetMatch}>Match</button>
												<div className="echos text-right">
													<a href="/liberty-index">
														Number of
														<img src="https://escapetheecho.org/templates/frontend/css/images/echoo-logo.png" className="img-echoo-logo" alt="Echo"/> s : 
													</a>
													<span className="coin-bank"> {coins} </span>
												</div>
											</div>
									}
								})()
							}
							
                            <div className="clear"></div>
                        </div>

             })()
        } 
            
        return ( 

<div>
	<section id="about">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <h2 className="section-heading text-uppercase">imo Directory</h2>
            
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <ul className="timeline">
              <li>
                <div className="timeline-image">
                  <img className="rounded-circle img-fluid" src="img/about/1.jpg" alt="" />
                </div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <a href="list.html">
            <h4 className="subheading text-uppercase">Best Local Foods</h4>
          </a>
                  </div>
                  <div className="timeline-body">
                    <p className="text-muted">You can view the list of best food you found in your local location.</p>
                  </div>
                </div>
              </li>
              <li className="timeline-inverted">
                <div className="timeline-image">
                  <img className="rounded-circle img-fluid" src="img/about/2.jpg" alt="" />
                </div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <a href="list.html">
            <h4 className="subheading text-uppercase">Home Services</h4>
          </a>
                  </div>
                  <div className="timeline-body">
                    <p className="text-muted">This will show the listing of services you wanted into your beloved house.</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="timeline-image">
                  <img className="rounded-circle img-fluid" src="img/about/3.jpg" alt="" />
                </div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <a href="list.html">
            <h4 className="subheading text-uppercase">Massage</h4>
          </a>
                  </div>
                  <div className="timeline-body">
                    <p className="text-muted">Be relax and enjoy the moment. Look for the list of massage services you wanted</p>
                  </div>
                </div>
              </li>
              
            </ul>
          </div>
        </div>
      </div>
    </section>


    <footer>
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <span className="copyright">Copyright © imodirectory 2018</span>
          </div>
    </div>
      </div>
    </footer>
	
    <NotificationSystem ref="notificationSystem" />
	
</div>
         );  
    }
}


export default connect((store) => {
    return {
        games : store.games
    }
})(ImoDirectory);

