import React, { Component } from "react"
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom"
import { connect } from "react-redux"

import Home from "./ShopApp/Home";
import Checkout from "./ShopApp/Checkout";


class ShopApp extends Component {
    

    constructor(props) {
        super();

        this.state = {
        };  
    }

    render() {
        
        return ( 

            <Router>
                <root>
                    <Switch>
                        <Route exact={true} path="/" component={ Home }/>
                        <Route exact={true} path="/checkout" component={ Checkout } />
                        <Route path="/:category" component={ Home }/>
                    </Switch>
                </root>
            </Router>
         );  
    }
		
}


export default connect((store) => {
    return {
		home:store.main
    }
})(ShopApp);
