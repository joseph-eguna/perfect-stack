import React, { Component } from "react"
import { connect } from "react-redux"

import Header from "./Sections/Header";
import Overlay from  "./Sections/Overlay";
import QuickView from "./Sections/QuickView";
import ModalView from "./Sections/ModalView";

class Workout extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    render() {

        //console.log(this.props);

        return ( 

<div>
    <div className="page-container ">
        
        <Header />

{/***WORKOUT CONTENT***/ }
<div className="page-content-wrapper ">
    <div className="content sm-gutter">
        <div className="container-fluid padding-25 sm-padding-10">
            <div className="row">
                <h4 className="col-md-12 text-white text-uppercase">Pre-conditioning workout - day 5</h4>
                <div className="col-md-12 row">

                    <div className="col-md-3 workout-days">
                        <div className="panel panel-default dark">
                            <div className="panel-heading">
                                <div className="panel-title">Day 1 - Rest and Recovery</div>
                            </div>
                            <div className="panel-body">
                                <span className="fa fa-play"></span>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                        90%
                                    </div>
                                </div>

                                <h6 className="text-uppercase">
                                    TOTAL MIN: 45 <br />
                                    REPETION: 2 <br />
                                    WEIGHTS: 120ln <br />
                                    STATUS: COMPLETED <br />
                                </h6>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-3 workout-days current">
                        <div className="panel panel-default dark">
                            <div className="panel-heading">
                                <div className="panel-title">Day 1 - Rest and Recovery</div>
                            </div>
                            <div className="panel-body">
                                <span className="fa fa-play"></span>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                        90%
                                    </div>
                                </div>

                                <h6 className="text-uppercase">
                                    TOTAL MIN: 45 <br />
                                    REPETION: 2 <br />
                                    WEIGHTS: 120ln <br />
                                    STATUS: COMPLETED <br />
                                </h6>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-3 workout-days upcoming">
                        <div className="panel panel-default dark">
                            <div className="panel-heading">
                                <div className="panel-title">Day 1 - Rest and Recovery</div>
                            </div>
                            <div className="panel-body">
                                <span className="fa fa-play"></span>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                        90%
                                    </div>
                                </div>

                                <h6 className="text-uppercase">
                                    TOTAL MIN: 45 <br />
                                    REPETION: 2 <br />
                                    WEIGHTS: 120ln <br />
                                    STATUS: COMPLETED <br />
                                </h6>
                            </div>
                        </div>
                    </div>
                        
                </div>
            </div>

            <div className="row">
            </div>
        </div>
    </div>
</div>

        <QuickView />
        
        <Overlay />

        <ModalView />
    </div>
</div>
         );  
    }
}


export default connect((store) => {
    return {
        overview: store.overview.payload
    }
})(Workout);

/*
<img src="http://img.over-blog-kiwi.com/1/06/42/88/20150408/ob_1a12a9_icono-videos.png" style={{maxWidth:"100%",display:"block",margin:"0 auto 20px",width:"70px"}}/>
<NotificationSystem ref="notificationSystem" />
*/