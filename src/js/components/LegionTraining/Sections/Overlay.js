import React, { Component } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"

class Overlay extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    render() {
        return ( 

        <div className="overlay hide" data-pages="search">
            <div className="overlay-content has-results m-t-20">
                <div className="container-fluid">
                    <img className="overlay-brand" src="/assets/img/logo.png" alt="logo" data-src="/assets/img/logo.png" data-src-retina="/assets/img/logo_2x.png" width="78" height="22" />
                    <a href="#" className="close-icon-light overlay-close text-black fs-16"> <i className="pg-close"></i> </a>
                </div>
                <div className="container-fluid">
                    <input id="overlay-search" className="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false" />
                    <br />
                    <div className="inline-block">
                        <div className="checkbox right">
                            <input id="checkboxn" type="checkbox" value="1" checked="checked" />
                            <label for="checkboxn"><i className="fa fa-search"></i> Search within page</label>
                        </div>
                    </div>
                    <div className="inline-block m-l-10">
                        <p className="fs-13">Press enter to search</p>
                    </div>
                </div>
                <div className="container-fluid"> 
                    <span>
                        <strong>suggestions :</strong>
                    </span> 
                    <span id="overlay-suggestions"></span>
                    <br />
                    <div className="search-results m-t-40">
                        <p className="bold">Pages Search Results</p>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="">
                                    <div className="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                                        <div> <img width="50" height="50" src="/assets/img/profiles/avatar.jpg" data-src="/assets/img/profiles/avatar.jpg" data-src-retina="/assets/img/profiles/avatar2x.jpg" alt="" /> </div>
                                    </div>
                                    <div className="p-l-10 inline p-t-5">
                                        <h5 className="m-b-5"><span className="semi-bold result-name">ice cream</span> on pages</h5>
                                        <p className="hint-text">via john smith</p>
                                    </div>
                                </div>
                                <div className="">
                                    <div className="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                                        <div>T</div>
                                    </div>
                                    <div className="p-l-10 inline p-t-5">
                                        <h5 className="m-b-5"><span className="semi-bold result-name">ice cream</span> related topics</h5>
                                        <p className="hint-text">via pages</p>
                                    </div>
                                </div>
                                <div className="">
                                    <div className="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
                                        <div><i className="fa fa-headphones large-text "></i> </div>
                                    </div>
                                    <div className="p-l-10 inline p-t-5">
                                        <h5 className="m-b-5"><span className="semi-bold result-name">ice cream</span> music</h5>
                                        <p className="hint-text">via pagesmix</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="">
                                    <div className="thumbnail-wrapper d48 circular bg-info text-white inline m-t-10">
                                        <div><i className="fa fa-facebook large-text "></i> </div>
                                    </div>
                                    <div className="p-l-10 inline p-t-5">
                                        <h5 className="m-b-5"><span className="semi-bold result-name">ice cream</span> on facebook</h5>
                                        <p className="hint-text">via facebook</p>
                                    </div>
                                </div>
                                <div className="">
                                    <div className="thumbnail-wrapper d48 circular bg-complete text-white inline m-t-10">
                                        <div><i className="fa fa-twitter large-text "></i> </div>
                                    </div>
                                    <div className="p-l-10 inline p-t-5">
                                        <h5 className="m-b-5">Tweats on<span className="semi-bold result-name"> ice cream</span></h5>
                                        <p className="hint-text">via twitter</p>
                                    </div>
                                </div>
                                <div className="">
                                    <div className="thumbnail-wrapper d48 circular text-white bg-danger inline m-t-10">
                                        <div><i className="fa fa-google-plus large-text "></i> </div>
                                    </div>
                                    <div className="p-l-10 inline p-t-5">
                                        <h5 className="m-b-5">Circles on<span className="semi-bold result-name"> ice cream</span></h5>
                                        <p className="hint-text">via google plus</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        );  
    }
}


export default connect((store) => {
    return {
        products: store.products.products
    }
})(Overlay);

/*
<NotificationSystem ref="notificationSystem" />
*/