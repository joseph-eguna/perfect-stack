import React, { Component } from "react"
import { connect } from "react-redux"
import Promise from "promise"
import _ from "lodash"
import qs from 'qs'
import axios from "axios"
import NotificationSystem from 'react-notification-system'
import { initialize } from "../actions/shopapp/products"
import { login } from "../actions/shopapp/register"
import cookie from 'react-cookies'

import Sidebar from "./ShopApp/sections/Sidebar";

class ShopApp extends Component {
    
    notificationSystem: null

    constructor(props) {
        super();

        this.state = {
            reloaded:false,
			products:[],
			selected:[],
			cart: (!_.isEmpty(cookie.load('logCart'))) ? cookie.load('logCart') : {},
			cartTotal: (!_.isEmpty(cookie.load('logCartTotal'))) ? cookie.load('logCartTotal') : { sub:0, items:0 },
            user: (!_.isEmpty(cookie.load('logUser'))) ? cookie.load('logUser') : null,
            error: null
        };

        //(!_.isEmpty(cookie.load('user'))) ? cookie.load('user') : null
        
        this.actionIncreaseDecreaseQty = this.actionIncreaseDecreaseQty.bind(this);

        this.actionAddToCart = this.actionAddToCart.bind(this);

        this.actionRemoveFromCart = this.actionRemoveFromCart.bind(this);

        this.actionLoggedIn = this.actionLoggedIn.bind(this);

        /*this.actionNavigation = this.actionNavigation.bind(this);
		this.actionIncreaseDecreaseQty = this.actionIncreaseDecreaseQty.bind(this);
		this.actionSelectToCart = this.actionSelectToCart.bind(this);
		
		this.actionAddToCart = this.actionAddToCart.bind(this);
		this.actionAddSelectedToCart = this.actionAddSelectedToCart.bind(this);*/
    }

    
    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*const params = {
                limit : this.state.limitProducts,
                json: 1
            }
        this.props.dispatch( { type:'' } );
        this.props.dispatch(getProducts(params));*/

        //console.log('Component will mount')

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        _this.notificationSystem = _this.refs.notificationSystem;

        _this.props.dispatch(initialize());
        
		//_this.props.dispatch(elementOverflow());
        //console.log('Component Did Mount');
    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        
        const _this = this
		
		let { products, children, user, error } = nextProps.store

		let letProducts = []

		//console.log(children)
		
		_.map(products, (v,i)=>{
			
			let letChildren = (!_.isUndefined(children[v.id.toString()])) ? children[v.id.toString()][v.id.toString()] : children[v.id.toString()]

			if(!_.isUndefined(letChildren) && !_.isEmpty(letChildren)) {

				_.map(letChildren, (vv,ii)=>{
					
					const { price, name, stock_status, stock_quantity, sku } = vv

	 				letProducts[vv.id] = { name, sku, price:parseFloat(price), stock_status, max_qty:stock_quantity, qty: 0 } 
				})
			}
			

			//products[v.id] = { qty: 0 } 
		})
		
        if(!_.isNull(user))
            cookie.save('logUser', user, { path: '/' })

        _this.setState( {..._this.state, reloaded:true, products:letProducts, error } )
			
	}

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }

    
    actionIncreaseDecreaseQty(e) {
		
		const _this = this;
		
		let product_id = parseInt(e.currentTarget.getAttribute('data-id'))
		
		let products = _this.state.products

		let selected = _this.state.selected
		
		let theSelected = []
		
		if(e.currentTarget.getAttribute('data-action') == 'add') {
			
			products[product_id].qty = (products[product_id].max_qty > products[product_id].qty)  ? products[product_id].qty + 1 : products[product_id].qty
			
			theSelected = { 
					name: products[product_id].name,  
					price:products[product_id].price,  
					qty: products[product_id].qty }

			selected = (products[product_id].qty > 0) ? {...selected, [product_id]:theSelected } : selected
			
			return _this.setState( {..._this.state, products, selected} )
		} 
		
		products[product_id].qty = (products[product_id].qty > 0) ? products[product_id].qty - 1 : 0
		
		theSelected = { 
					name: products[product_id].name,  
					price:products[product_id].price,  
					qty: products[product_id].qty }

		selected = (products[product_id].qty > 0) ? {...selected, [product_id]:theSelected } : selected

		return _this.setState( {..._this.state, products} )
		
	}
	
	
	actionAddToCart(e){
		
		const _this = this

		let products = _this.state.products

		let selected = _this.state.selected

		let cart = _this.state.cart

		let cartTotal = _this.state.cartTotal

		let intersection = [];


			if(!_.isEmpty(selected)) {
				_.map(selected, (v, i)=>{
					products[i].qty = 0
					cartTotal.sub = parseFloat(cartTotal.sub) + (v.qty * v.price)
					cartTotal.items = parseInt(cartTotal.items) + v.qty
				})
			}

			intersection = _.intersection(Object.keys(cart), Object.keys(selected))

			if(!_.isEmpty(intersection)) {
				_.map(intersection, (v, i)=>{
					selected[v].qty = parseInt(cart[v].qty) + parseInt(selected[v].qty)
				})
			}
			cart = Object.assign( cart, _this.state.selected)

        cookie.save('logCart', cart, { path: '/' })
        
        cookie.save('logCartTotal', cartTotal, { path: '/' })
        
		return _this.setState({..._this.state, cart, selected:[], products})

	}


	actionRemoveFromCart(e) {
		
		const _this = this

		let product_id = parseInt(e.currentTarget.getAttribute('data-id'))
		
		let { cart, cartTotal } = _this.state

			cartTotal.sub = cartTotal.sub - ( cart[product_id].qty * cart[product_id].price )

			cartTotal.items = cartTotal.items - cart[product_id].qty


			delete cart[product_id]
        
        cookie.save('logCart', cart, { path: '/' })
        
        cookie.save('logCartTotal', cartTotal, { path: '/' })
        
		return _this.setState({ ..._this.state, cart, cartTotal })
	}


	actionLoggedIn(e) {
        
        e.preventDefault()
        
        const _this = this
		
            let username = e.currentTarget.querySelectorAll('[name="username"]')[0].value
            let password  = e.currentTarget.querySelectorAll('[name="password"]')[0].value
        
        let _params = { 
            username,
            password
        }
		
        return _this.props.dispatch( login(_params) );

	}


    render() {
		
		const _this = this
		
		console.log(_this.state);
        
		//console.log(_this.state.products[4765])

		//console.log(Object.keys(_this.props.store.children["4940"]))
		//console.log(_this.props.store.children["4940"])
		
		//let idt = 4935;
		//console.log(Object.keys(_this.props.store.children), Object.keys(_this.props.store.children).indexOf(idt.toString()))

		let list = (()=>{

			return _.map( _this.props.store.products, (v, i)=> {
		
					let variations = (()=>{


							//console.log(Object.keys(_this.props.store.children), Object.keys(_this.props.store.children).indexOf(idt.toString()))
							//let parentId = Object.keys(_this.props.store.children).indexOf(v.id.toString())
						let items = _this.props.store.children[v.id.toString()]

						if( !_.isEmpty(items) && !_.isUndefined(items) ) {

							items = items[v.id.toString()]

							//console.log(items)

							let variation = (()=>{

								return _.map(items, (vv,ii)=>{

									//console.log(vv)
									let itemName = vv.name.substr(vv.name.length - 3)

									let qty = (!_.isEmpty(_this.state.products[vv.id])) ? _this.state.products[vv.id].qty : 0;

									return <tr>
			
												<td style={ {width:'33%'} } >
													<h5>{ itemName }</h5>
												</td>
												<td style={ {width:'35%'} }>

													<div className="quantity">
														<input type="number" className="input-text qty text restrict-max qty-multiple" min="0" max="1000" title="Quantity" name="quantity" value={ qty } />
														<span className="fa fa-minus decrement" data-id={ vv.id } data-action="minus" onClick={ this.actionIncreaseDecreaseQty }></span>
														<span className="fa fa-plus increment" data-id={ vv.id } data-action="add" onClick={ this.actionIncreaseDecreaseQty }></span>
													</div>
												</td>
												<td style={ {width:'32%'} }>
													<h5 className="subtotal">20</h5>
												</td>
											</tr>
								})

							})()

							return <table className="table table-bordered variations">
										<thead>
											<tr>
												<td colSpan="2">
													<h6 className="text-center">Select Options</h6>
												</td>
												<td>
													<h6 className="text-center">Total</h6>
												</td>
											</tr>
										</thead>
										<tbody>
											{ variation }
										</tbody>
									</table>

						} else if(!_.isEmpty(v.grouped_products)) {
							return <span></span>
						}

						return <h5>Price: {v.price}</h5>
							
					})()

				return 	<div className="col-lg-4 col-md-6 mb-4">
			            	<div className="card h-100x">
			                	<a data-href={v.id}>
			                		<img className="card-img-top" src="//vapehq.co.nz/wp-content/uploads/2018/03/Artboard-2-copy.jpg" alt="" />
			                	</a>
			                	<div className="card-body">
			                  		<h4 className="card-title">
			                    		<a data-href="">{ v.name }</a>
			                  		</h4>

			                  		{ variations }

			                  		<p className="card-text">
			                  			{ 'Ya ko kabayo kung mo work ini ako ra ini gi samplolan basin baja mo work sija, tan-awon ta' }
			                  		</p>
			                	</div>
			              	</div>
			            </div>
				})
		})()


		let cart = (()=>{
				
			let items = (()=>{

				let item = (()=>{

					return _.map(_this.state.cart, (v,i)=>{

						return <li className="woocommerce-mini-cart-item mini_cart_item">
				                    <a href="#" onClick={this.actionRemoveFromCart} data-id={i}>×</a>                                                 
				                    <a href="#">
				                        <img src="//vapehq.co.nz/wp-content/uploads/2018/03/Artboard-2-copy.jpg" alt="Nasty Juice Slow Blow" />
				                        <span className="data">
				                            <span className="title"> { v.name } </span>                                
				                            <span className="quantity">{v.qty} × 
				                                <span data-rp-wcdpd-public-descriptions-product-pricing="{&quot;rp_wcdpd_301fc886&quot;:&quot;Range Pricing&quot;}">
				                                    <span className="woocommerce-Price-amount amount">
				                                        <span className="woocommerce-Price-currencySymbol"> $</span>{v.price}
				                                    </span>
				                                </span>
				                            </span>
				                        </span>                          
				                    </a>
				                </li>	
					})

				})()


				return <ul className="woocommerce-mini-cart woocommerce cart_list product_list_widget ">
			                
			                { item }
			                
			            </ul>
			})()

			return <div className="header_cart collapse navbar-collapse" id="header-cart">
				        
				        { (()=>{
				        	if(!_.isEmpty( _this.state.cart )) {

				        		return	<div className="widget_shopping_cart_content">
				            
						            { items }

						            <p className="woocommerce-mini-cart__total total">
						                <strong>Subtotal: </strong> 
						                <span className="woocommerce-Price-amount amount">
						                    <span className="woocommerce-Price-currencySymbol">$</span> { _this.state.cartTotal.sub }
						                </span>
						            </p>

						            
						            <p className="woocommerce-mini-cart__buttons buttons">
						                <a href="#" className="button checkout wc-forward cl-btn btn-style-square btn-hover-darker">Checkout</a>
						            </p>
						        </div>
				        	} else {
				        		return <h3 style={{ textAlign: 'center',margin: '30px' }}>{ '{ Empty Cart }' }</h3>
				        	}

				        })() }

				        
				    </div>
		})()

		return <div className="wrapper">
					
                    <Sidebar />

					<div className="container">
						<nav className="navbar navbar-expand-lg navbar-light bg-light sticky">
				                <div className="container-fluid">

				                    <button type="button" id="toggleSidebar" className="btn btn-info categories">
				                    	<span className="fa fa-windows"></span>
				                    </button>
				                    <button type="button" class="btn btn-info add-to-cart" onClick={ this.actionAddToCart }>Add To Cart</button>
				                    <button className="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#header-cart" aria-expanded="false" aria-label="Toggle navigation">
				                        <span className="fa fa-shopping-cart"></span>
				                        <span className="badge">{ _this.state.cartTotal.items }</span>
				                    </button>

				                    { cart }

				                </div>
				        </nav>


				      	<div className="row product-content">
							<div className="col-lg-12">

					          	{
					          		(() => {
						          		if(!_.isEmpty(_this.props.store.products)) {
						        		
						        			return <div className="row">{ list }</div>  			
						          		
						          		} else {

						          			return <h2>LOADING...</h2>

						          		}
						          	})()
					          	}
					            

							</div>
							<nav style={ {margin:'auto'} }>
							  <ul className="pagination">
							    <li className="page-item">
							      <a className="page-link" href="#" aria-label="Previous">
							        <span aria-hidden="true">&laquo;</span>
							        <span className="sr-only">Previous</span>
							      </a>
							    </li>
							    <li className="page-item"><a className="page-link" href="#">1</a></li>
							    <li className="page-item"><a className="page-link" href="#">2</a></li>
							    <li className="page-item"><a className="page-link" href="#">3</a></li>
							    <li className="page-item">
							      <a className="page-link" href="#" aria-label="Next">
							        <span aria-hidden="true">&raquo;</span>
							        <span className="sr-only">Next</span>
							      </a>
							    </li>
							  </ul>
							</nav>
				    	</div>
				    </div> 

				    <div className="popup">
						<div className="box">
						<form style={ {marginBottom:'0px'} } name="register" onSubmit={ this.actionLoggedIn }>
					  		<h3>LOGIN</h3>
							<div className="form-group">
					    		<label for="username">Username/Email</label>
					    		<input type="text" onChange={ this.actionCookie } name="username" className="form-control" placeholder="Enter Username/Email:" autocomplete="off" />
					  		</div>
					  		<div className="form-group">
					    		<label for="password">Password</label>
					    		<input type="password" name="password" className="form-control" autocomplete="off"/>
					  		</div>
							<div className="form-group">
								<button type="submit" className="btn btn-primary mb-2">Login</button>
							</div>
						</form>
					    </div>
					</div>
				</div>
			  
	}
		
}


export default connect((store) => {
    return {
		store:store.main
    }
})(ShopApp);
