import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import Promise from "promise"
import _ from "lodash"
import qs from 'qs'
import axios from "axios"
import NotificationSystem from 'react-notification-system'
import { getProducts } from "../../actions/shopapp/products"

import Sidebar from "./sections/Sidebar";
import Navigation from "./sections/Navigation";

class Home extends Component {
    
    notificationSystem: null

    constructor(props) {
        super();

        let { products, productOffset, selected, cart, user, error, loading, headers } = props.store

        /*this.state = {
            reloaded:false,
			products:[],
			selected,
			cart,
			user,
			//cart: (!_.isEmpty(cookie.load('logCart'))) ? cookie.load('logCart') : {},
			//cartTotal: (!_.isEmpty(cookie.load('logCartTotal'))) ? cookie.load('logCartTotal') : { sub:0, items:0 },
            //user: (!_.isEmpty(cookie.load('logUser'))) ? cookie.load('logUser') : [],
            error: null
        };*/

        this.state = { products, productOffset, selected, cart, user, error }

        //(!_.isEmpty(cookie.load('user'))) ? cookie.load('user') : null
        
        this.actionIncreaseDecreaseQty = this.actionIncreaseDecreaseQty.bind(this);

        this.actionLoadmore = this.actionLoadmore.bind(this);

        //this.actionLoggedIn = this.actionLoggedIn.bind(this);

    }

    
    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*const params = {
                limit : this.state.limitProducts,
                json: 1
            }
        this.props.dispatch( { type:'' } );
        this.props.dispatch(getProducts(params));*/

        //console.log('Component will mount')

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this
        
        let _params = []

        _this.notificationSystem = _this.refs.notificationSystem;


        _params = _this.props.match.params

        _params.offset = 0

        _this.props.dispatch(getProducts( _params ));

        //_this.props.dispatch(elementOverflow());
        //console.log('Component Did Mount');
    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        
        const _this = this
		
		let { selected, cart, products, children, user, error, loading, headers } = nextProps.store

		let letProducts = []

		//console.log(children)
		
		_.map(products, (v,i)=>{
			
			let letChildren = (!_.isUndefined(children[v.id.toString()])) ? children[v.id.toString()][v.id.toString()] : children[v.id.toString()]

			if(!_.isUndefined(letChildren) && !_.isEmpty(letChildren)) {

				_.map(letChildren, (vv,ii)=>{
					
					const { price, name, stock_status, stock_quantity, backorders_allowed, sku, images } = vv

					let max_qty = (backorders_allowed == true) ? 1000 : stock_quantity

	 				letProducts[vv.id] = { 
	 					name, 
	 					sku, 
	 					images,
	 					price:parseFloat(price), 
	 					stock_status, 
	 					max_qty, 
	 					qty: 0 
	 				} 
				})
			}
			

			//products[v.id] = { qty: 0 } 
		})
		
		
        _this.setState( {..._this.state, products:letProducts, selected, cart, error, loading, headers } )
			
	}

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }

    
    actionIncreaseDecreaseQty(e) {
		
		const _this = this;
		
		let product_id = parseInt(e.currentTarget.getAttribute('data-id'))
		
		let products = _this.state.products

		let selected = _this.state.selected
		
		let theSelected = []
		
		if(e.currentTarget.getAttribute('data-action') == 'add') {
			
			products[product_id].qty = (products[product_id].max_qty > products[product_id].qty)  ? products[product_id].qty + 1 : products[product_id].qty
			
			//If out of stock
			if(products[product_id].qty == 0) {
				 _this.notificationSystem.addNotification({
		            message: products[product_id].name + ' is out of stock.',
		            level: 'error',
		            autoDismiss:3
		        });
			}

			theSelected = { 
					name: products[product_id].name,  
					price:products[product_id].price,  
					qty: products[product_id].qty,
					images: products[product_id].images
				}

			selected = (products[product_id].qty > 0) ? {...selected, [product_id]:theSelected } : selected

			return _this.setState( {..._this.state, products, selected} )
		} 
		
		products[product_id].qty = (products[product_id].qty > 0) ? products[product_id].qty - 1 : 0
		
		theSelected = { 
					name: products[product_id].name,  
					price:products[product_id].price,  
					qty: products[product_id].qty,
					images: products[product_id].images
				}

		if((products[product_id].qty > 0))
			selected = {...selected, [product_id]:theSelected }
		else
			delete selected[product_id]

		//selected = (products[product_id].qty > 0) ? {...selected, [product_id]:theSelected } : 

		return _this.setState( {..._this.state, products, selected} )
		
	}
	
	actionLoadmore(e) {

		e.preventDefault()

		let _this = this

		let productOffset = _this.state.productOffset + 1

		let _params = _this.props.match.params

			_params.offset = parseInt( productOffset )

		_this.setState({..._this.state, productOffset, loading:true})


        _this.props.dispatch(getProducts( _params, true ));

        //console.log(_this.state.headers, _this.state.productOffset)

	}

    render() {
		
		const _this = this
		
		//console.log(_this.props.store)

		//console.log(Object.keys(_this.props.store.children["4940"]))
		//console.log(_this.props.store.children["4940"])
		
		//let idt = 4935;
		//console.log(Object.keys(_this.props.store.children), Object.keys(_this.props.store.children).indexOf(idt.toString()))

		let list = (()=>{

			return _.map( _this.props.store.products, (v, i)=> {
		

				let imageLink = (!_.isEmpty(v.images)) ?  v.images[0].src : '//vapehq.co.nz/wp-content/uploads/2018/03/Artboard-2-copy.jpg'

				let description = (()=>{
					if(!_.isEmpty(v.meta_data)) {
						return _.map(v.meta_data, (v, i)=>{
							if(v.key == 'excerpt')
								return v.value.replace(/(<([^>]+)>)/g, "")
						})
					}
				})()

				let variations = (()=>{


					let items = _this.props.store.children[v.id.toString()]

					if( !_.isEmpty(items) && !_.isUndefined(items) ) {

						items = items[v.id.toString()]


						let variation = (()=>{

							return _.map(items, (vv,ii)=>{

								let itemName = vv.name.substr(vv.name.length - 3)

								let qty = (!_.isEmpty(_this.state.products[vv.id])) ? _this.state.products[vv.id].qty : 0;

								let total = (!_.isEmpty(_this.state.cart[vv.id])) ? parseFloat(_this.state.cart[vv.id].qty * _this.state.cart[vv.id].price) : 0 


								return <tr>
		
											<td style={ {width:'33%'} } >
												<h5>{ itemName }
												 	<span className="price">Price: { vv.price }</span>
												</h5>
											</td>
											<td style={ {width:'35%'} }>

												<div className="quantity">
													<input type="number" className="input-text qty text restrict-max qty-multiple" min="0" max="1000" title="Quantity" name="quantity" value={ qty } />
													<span className="fa fa-minus decrement" data-id={ vv.id } data-action="minus" onClick={ this.actionIncreaseDecreaseQty }></span>
													<span className="fa fa-plus increment" data-id={ vv.id } data-action="add" onClick={ this.actionIncreaseDecreaseQty }></span>
												</div>
											</td>
											<td style={ {width:'32%'} }>
												<h5 className="subtotal">{ total.toFixed(2) }</h5>
											</td>
										</tr>
							})

						})()

						return <table className="table table-bordered variations">
									<thead>
										<tr>
											<td colSpan="2">
												<h6 className="text-center">Select Options</h6>
											</td>
											<td>
												<h6 className="text-center">Total</h6>
											</td>
										</tr>
									</thead>
									<tbody>
										{ variation }
									</tbody>
								</table>

					} else if(!_.isEmpty(v.grouped_products)) {
						return <span></span>
					}

					return <h5>Price: {v.price}</h5>
						
				})()

				return 	<div className="col-lg-4 col-md-6 mb-4">
			            	<div className="card h-100x">
			                	<a data-href={v.id}>
			                		<img className="card-img-top" src={ imageLink } alt="" />
			                	</a>
			                	<div className="card-body">
			                  		<h4 className="card-title">
			                    		<a data-href="">{ v.name }</a>
			                  		</h4>

			                  		<p className="card-text">
			                  			{ description }
			                  		</p>

			                  		{ variations }
			                	</div>
			              	</div>
			            </div>
				})
		})()


		return <div className="wrapper">
					
                    <Sidebar />

					<div className="container">
						
						<Navigation 
							selected = { _this.state.selected }
							match = { _this.props.match } />


				      	<div className="row product-content">
							<div className="col-lg-12">

					          	{
					          		(() => {
						          		if(!_.isEmpty(_this.props.store.products)) {
						        		
						        			return <div className="row">
						        						{ list }

						        						{
						        							(()=>{
						        								
						        								const loadingText = (_this.state.loading !== true) ? 'MORE PRODUCTS' : 'LOADING...'  

						        								if(!_.isEmpty(_this.state.headers) && _this.state.productOffset < parseInt(_this.state.headers['x-wp-totalpages'] - 1))
						        									return <button onClick={ this.actionLoadmore } style={ {display:'block', margin:'0px auto 20px'} } className="btn btn-info">{ loadingText }</button>
						        							})()
						        						}
						        						
						        					</div>  			
						          		
						          		} else {

						          			return <h2>LOADING...</h2>

						          		}
						          	})()
					          	}
					            

							</div>
				    	</div>
				    </div> 

				    <NotificationSystem ref="notificationSystem" />

				</div>
			  
	}
		
}


export default connect((store) => {
    return {
		store:store.main
    }
})(Home);
