import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import Promise from "promise"
import _ from "lodash"
import qs from 'qs'
import axios from "axios"
import NotificationSystem from 'react-notification-system'
import { getPaymentGateways, createOrder } from "../../actions/shopapp/products"
import { login } from "../../actions/shopapp/register"
import cookie from 'react-cookies'


import Navigation from "./sections/Navigation";
import Popup from "./sections/Popup";

class Checkout extends Component {
    
    notificationSystem: null

    constructor(props) {

        super();
   
        //const {  } = props.checkout
        
        const { cart, cartTotal, user, paymentGateways, response, sidebar, error } = props.store
        
        this.state = { cart, cartTotal, user, paymentGateways, response, loading:false }

        
        this.actionPayOrder = this.actionPayOrder.bind(this)
        
        this.selectPaymentGateway = this.selectPaymentGateway.bind(this)

        this.actionChangeField = this.actionChangeField.bind(this)

        /*this.actionNavigation = this.actionNavigation.bind(this);
		this.actionAddSelectedToCart = this.actionAddSelectedToCart.bind(this);*/
    }

    
    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /**/

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        _this.notificationSystem = _this.refs.notificationSystem;
        
        _this.props.dispatch(getPaymentGateways());
        
        
		//_this.props.dispatch(elementOverflow());
        //console.log('Component Did Mount');
    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        
        const _this = this
		
        //let {  } = nextProps.checkout
        
        let { cart, cartTotal, user, paymentGateways, response, sidebar, error } = nextProps.store
        
        _this.setState( {..._this.state, cart, cartTotal, user, paymentGateways, response, loading:false } )
			
	}

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }

    actionPayOrder(e) {
        
        e.preventDefault()
        
        const _this = this
        
        const { cart, cartTotal, user } = _this.state
        
        const fields = { ...user.fields, paymentOption:_this.state.paymentGatewaySelected } 

        let _data = {
            cart,
            cartTotal,
            user,
            fields
        }
        
        /*
         * MUST SELECT PAYMENT OPTION
         */
        if( _.isUndefined(fields.paymentOption) ) {
             _this.notificationSystem.addNotification({
                message: 'You must select Payment Option',
                level: 'error',
                autoDismiss:3
            });
            return
        }

        /*
         * ON ACCOUNT PAYMENT
         */
        if(user.fields['_allowed_onaccount'] !== 'allowed' && fields.paymentOption == 'wcts_onaccount') {
             _this.notificationSystem.addNotification({
                message: 'You are not allowed to pay thru On-Account Payment! Contact VapeHQ administrator.',
                level: 'error',
                autoDismiss:3
            });
            return
        }

        //cookie.remove('logCart', cart, { path: '/' })
        //cookie.remove('logCartTotal', cartTotal, { path: '/' })
        
        _this.setState({..._this.state, loading:true})
        
        if(!_.isEmpty(cart))  
            _this.props.dispatch(createOrder( _data ));

        return
        
    }

    selectPaymentGateway(e) {
        
        const _this =  this
        
        e.preventDefault()
        
        let paymentGatewaySelected = e.currentTarget.getAttribute('value')
        
        _this.setState({..._this.state, paymentGatewaySelected})
        
    }


    actionChangeField(e) {

        const _this = this

        const name = e.currentTarget.getAttribute('name')

        const value = e.currentTarget.value

        const fields = { ..._this.state.user.fields, [name]:value }

        const user = { ..._this.state.user, fields }

        _this.setState( { ..._this.state, user } )
    }

    render() {
		
		const _this = this
		
		console.log(_this.props);
        
        const paymentGateways = (()=>{
                  
            if(!_.isEmpty(_this.state.paymentGateways)) {
                
                let list = (()=>{
                    return _.map( _this.state.paymentGateways, (v,i) => {
                        
                        //console.log(v.enabled)

                        if(v.enabled != true)
                            return 

                        let checked = (()=>{
                            if(v.id == _this.state.paymentGatewaySelected) {
                                return <span className="checked"><input type="checkbox" className="checkbox" onClick={this.selectPaymentGateway} value={v.id}/></span>
                            } 
                            return <input type="checkbox" className="checkbox" onClick={this.selectPaymentGateway} value={v.id}/>
                        })()
                        
                        return  <li>
                                    { checked }
                                    <span> { v.title } </span>
                                </li>
                    })
                })()
                
                return  <div>
                            <span className="title">Payment Options</span>
                            <ul>
                                { list }
                            </ul>
                        </div>
                    
            } else {
                return <h6>Loading Payment Options...</h6>
            }
            
            
        })()
                
        const cart = (()=>{
            
            const items = (()=>{
                return _.map(_this.state.cart, (v,i)=>{

                    const imageUrl = (!_.isEmpty(v.images)) ? v.images[0].src : "//vapehq.co.nz/wp-content/uploads/2018/03/Artboard-2-copy.jpg";
                        
                    return <li className="woocommerce-mini-cart-item mini_cart_item">
                                <a href="#">
                                    <img src={imageUrl} alt={ v.name } />
                                    <span className="data">
                                        <span className="title">
                                            <span> { v.name } </span>
                                        </span>
                                        <span className="quantity">
                                            <span> { v.qty } </span>
                                            <span> ×</span>
                                            <span>
                                                <span className="woocommerce-Price-amount amount">
                                                    <span className="woocommerce-Price-currencySymbol"> $</span>
                                                    <span> { v.price } </span>
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                })    
            })()
            
            return <div className="header_cart checkout">
                        <div className="widget_shopping_cart_content">
                            <ul className="woocommerce-mini-cart woocommerce cart_list product_list_widget ">
                                 { items }
                            </ul>
                            <p className="woocommerce-mini-cart__total total">
                                <strong>Subtotal: </strong>
                                    <span className="woocommerce-Price-amount amount">
                                        <span> { _this.state.cartTotal.sub } </span>
                                    </span>
                                    <br />

                                <strong>Total: </strong> 
                                    <span className="woocommerce-Price-amount amount">
                                        <span className="woocommerce-Price-currencySymbol">$</span>
                                        <span> { _this.state.cartTotal.sub } </span>
                                    </span>
                            </p>

                            <div className="payment-options">
                                { paymentGateways }
                            </div>
                            

                            <p className="woocommerce-mini-cart__buttons buttons">
                                <button type="submit" className="button checkout wc-forward cl-btn btn-style-square btn-hover-darker">
                                    Pay Now!
                                </button>
                            </p>
                        </div>
                    </div>
            
        })()
        
        const form = (()=>{
            
            let fields = (()=>{

                let fields = { 
                    billing_first_name: 'Firstname:',
                    billing_last_name: 'Lastname:',
                    billing_email:'Email Address:',
                    billing_company:'Company:',
                    billing_phone:'Phone No.:',
                    billing_address_1:'Address:',
                    billing_address_2:'Address Line 2:',
                    billing_city:'City:',
                    billing_postcode:'Postcode:',
                    billing_state:'State:',
                    billing_country:'Country:'
                }

                return _.map( fields, (v, i)=>{

                    let value = (!_.isEmpty(_this.state.user)) ? _this.state.user.fields[i] : ""

                    return <div className="form-group">
                                <label for="textfield"> {v} </label>
                                <input type="text" name={i} className="form-control" onChange={ this.actionChangeField } value={ value }/>
                            </div>
                })
            })()


            return <form style={ {marginTop:'175px'} } name="register" onSubmit={this.actionPayOrder}>
                        <h3>CHECKOUT</h3>
                        <h4>Billing & Shipping Address</h4><hr />

                        {fields}

                        { cart }
                    </form>
        })()
        
        const thanks = (()=>{
            
            let response = _this.state.response
            
            if(_.isEmpty(response))
                return
                
            let items = (()=>{
                return _.map(response.cart, (v,i)=>{
                    return <li className="woocommerce-mini-cart-item mini_cart_item">
                                <span>{ v.name }  { v.qty }×{ v.price }</span>
                          </li>
                })
            })()
            
            return <div style={ {marginTop:'150px'} }>
                        <h1>order#: <span className="text-danger"> { response.orderId } </span> has been process!</h1>
                        
                        <div className="header_cart checkout">
                            <div className="widget_shopping_cart_content">
                                <ul className="woocommerce-mini-cart woocommerce cart_list product_list_widget ">
                                    { items }
                                </ul>
                                <p className="woocommerce-mini-cart__total total">
                                    <strong>Total Paid: </strong> 
                                        <span className="woocommerce-Price-amount amount">
                                            <span className="woocommerce-Price-currencySymbol">$</span>
                                            <span> { response.cartTotal.sub } </span>
                                        </span>
                                </p>

                            </div>
                        </div>
                        
                        
                        <h5 className="text-center text-success">Thank your for your order!</h5>
                   </div>
            
        })()
        
        const loading = (_this.state.loading === true) ? 'loading' : ''
           
        return <div className="wrapper">
	               <div className={ "container " + loading }>
                    
                    <Navigation 
                        match = { _this.props.match } />
                    
                        {
                            (()=>{
                                if( !_.isEmpty(_this.state.cart)) {
                                    return <div> {form} </div>
                                } else if( !_.isEmpty(_this.state.response) ) {
                                    return <div> {thanks} </div>
                                }
                                return <div style={ {marginTop:'175px'} }> 
                                            <h5>{ '{ Empty Cart }' } 
                                                <Link to="/"  className="btn-st1">
                                                    Continue Shopping
                                                </Link>
                                            </h5> 
                                        </div>
                            })()
                        }
                
                    </div>

                    <NotificationSystem ref="notificationSystem" />

                </div>
			  
	}
		
}


export default connect((store) => {
    return {
		store:store.main,
        checkout: store.checkout
    }
})(Checkout);
