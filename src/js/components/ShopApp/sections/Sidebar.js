import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import _ from "lodash"

import { getProducts, getCategories } from "../../../actions/shopapp/products"

class Sidebar extends Component {
    
    //notificationSystem: null

    constructor(props) {
        super();

        this.state = {
            categories : [],
            sidebar:'inactive'
        };

        //(!_.isEmpty(cookie.load('user'))) ? cookie.load('user') : null
        
        this.actionSelectCategory = this.actionSelectCategory.bind(this)
  
        /*this.actionNavigation = this.actionNavigation.bind(this);
		this.actionAddSelectedToCart = this.actionAddSelectedToCart.bind(this);*/
    }

    
    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    /*addNotification(data){
        this.notificationSystem.addNotification(data);
    }*/

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*
         */

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        //_this.notificationSystem = _this.refs.notificationSystem;

        _this.props.dispatch(getCategories());

    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        
        const _this = this

        const { categories, sidebar } = nextProps.sidebar

        _this.setState( {..._this.state,  categories, sidebar } )
			
	}

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }


    actionSelectCategory(e) {

        const _this = this

        const category = e.currentTarget.getAttribute('data-id')

        const _params = { category, offset:0 }

        _this.props.dispatch(getProducts( _params ));

    }

    render() {
		
		const _this = this

        //console.log( _this.props );

        //console.log(_this.state)

        //<Link to="/checkout"  className="button checkout wc-forward cl-btn btn-style-square btn-hover-darker" >Checkout</Link>
        
        const categories = (()=>{

            const categories = _this.state.categories 

            let menus = (()=>{
                    
                    return _.map( categories[0], (v,i)=>{

                        let children = (()=>{

                            return <ul className="collapse list-unstyled" id={ v.slug }>
                                    {
                                        (()=>{
                                            return _.map(categories[v.id], (vv,ii)=>{
                                                return <li>
                                                        <Link to={"/" + vv.id}  data-id={ vv.id } className="" onClick={this.actionSelectCategory} > {vv.name} </Link>
                                                    </li>
                                            })
                                        })()
                                    }
                                    </ul>

                            
                        })()

                        return <li className="active">
                                    <a href={ '#'+v.slug } data-toggle="collapse" aria-expanded="false" className="dropdown-toggle">
                                        { v.name }
                                    </a>
                                    { children }
                                </li>
                    })
                })()

            if(!_.isEmpty(categories[0]))
                return <ul className="list-unstyled components">
                            
                            { menus }

                        </ul>

            return <div></div>

        })()
		
    return <nav id="sidebar" className={ _this.state.sidebar }>
                <div className="categories">
                    <div className="sidebar-header">
                        <h3>Categories</h3>
                    </div>

                    { categories }

                </div>
            </nav>
 			  
	}
		
}


export default connect((store) => {
    return {
		sidebar:store.main
    }
})(Sidebar);
