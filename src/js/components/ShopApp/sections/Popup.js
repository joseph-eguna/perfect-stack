import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import _ from "lodash"
import cookie from 'react-cookies'

import { login } from "../../../actions/shopapp/register"

class Popup extends Component {
    
    //notificationSystem: null

    constructor(props) {
        
        super();

        let { user } = props

        this.state = { 
            user,
            loading:false,
            error:null
         }

        this.actionLoggedIn = this.actionLoggedIn.bind(this)

        //(!_.isEmpty(cookie.load('user'))) ? cookie.load('user') : null
        /*this.actionNavigation = this.actionNavigation.bind(this);
		this.actionAddSelectedToCart = this.actionAddSelectedToCart.bind(this);*/
    }

    
    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    /*addNotification(data){
        this.notificationSystem.addNotification(data);
    }*/

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*
         */

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        //_this.notificationSystem = _this.refs.notificationSystem;

    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        
        const _this = this

        let { user, error } = nextProps.store

            user = (!_.isEmpty(cookie.load('logUser'))) ? cookie.load('logUser') : user

        _this.setState( {..._this.state, user, error, loading:false} )
			
	}

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }


    actionLoggedIn(e) {
        
        e.preventDefault()
        
        const _this = this
        
            let username = e.currentTarget.querySelectorAll('[name="username"]')[0].value
            let password  = e.currentTarget.querySelectorAll('[name="password"]')[0].value
        
        let _params = { 
            username,
            password
        }
        
        _this.setState( {..._this.state, loading:true} )
        
        return _this.props.dispatch( login(_params) );

    }

    render() {
		
		const _this = this
		
        //console.log(_this.props);

        const error = (()=>{
                if(_.isEmpty(_this.state.error))
                    return
                return <span className="response error"> {_this.state.error[0]} </span>
            })()

        const loading = (_this.state.loading === true) ? 'loading' : ''


        if(!_.isEmpty(_this.state.user)) 
            return <div></div>

        return <div className={ 'popup ' + loading }>
                    <div className="box">
                    <form style={ {marginBottom:'0px'} } name="register" onSubmit={ this.actionLoggedIn }>
                        <h3>LOGIN</h3>
                        <div className="form-group">
                            <label for="username">Username/Email</label>
                            <input type="text" name="username" className="form-control" placeholder="Enter Username/Email:" autocomplete="off" />
                        </div>
                        <div className="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" className="form-control" autocomplete="off"/>
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-primary mb-2">Login</button>
                        </div>
                        <div class="form-group"> { error } </div>
                    </form>
                    </div>
                </div>
 			  
	}
		
}


export default connect((store) => {
    return {
		store:store.main
    }
})(Popup);
