import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import cookie from 'react-cookies'
import NotificationSystem from 'react-notification-system'

import { login, logout } from "../../../actions/shopapp/register"
import { toggleSidebar } from "../../../actions/shopapp/actions"

class Navigation extends Component {
    
    notificationSystem: null

    constructor(props) {
        
        super();

        const _this = this

        let { selected, match } = props

        let { cart, cartTotal, user, showLogin, sidebar, error, loading } = props.store

        if(match.url == '/checkout' && _.isEmpty(user)) {
            showLogin = true
        }

        this.state = { match, cart, cartTotal, selected, user, showLogin, sidebar, error, loading } 


        this.actionAddToCart = this.actionAddToCart.bind(this)

        this.actionRemoveFromCart = this.actionRemoveFromCart.bind(this)

        this.actionLoggedIn = this.actionLoggedIn.bind(this)

        this.actionLogout = this.actionLogout.bind(this)
    
        this.toggleSidebar = this.toggleSidebar.bind(this)

        this.toggleLogin = this.toggleLogin.bind(this)

        /*this.actionNavigation = this.actionNavigation.bind(this);
		this.actionAddSelectedToCart = this.actionAddSelectedToCart.bind(this);*/
    }

    
    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*
         */

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        _this.notificationSystem = _this.refs.notificationSystem;

    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        
        const _this = this

        /*let { cart, cartTotal, selected, products, user } = nextProps

        let { sidebar } = nextProps.store*/

        let { selected, match } = nextProps

        let { cart, cartTotal, user, showLogin, sidebar, error } = nextProps.store

            //cart = (!_.isEmpty(cookie.load('logCart'))) ? cookie.load('logCart') : cart 

            //cartTotal = (!_.isEmpty(cookie.load('logCartTotal'))) ? cookie.load('logCartTotal') : cartTotal

            //user = (!_.isEmpty(cookie.load('logUser'))) ? cookie.load('logUser') : user


        if(!_.isEmpty(error)) {
            _.map(error, (v,i)=>{
                _this.notificationSystem.addNotification({
                    message: v,
                    level: 'error',
                    autoDismiss:3
                });
            })

            _this.props.dispatch( {type: 'CLEAR-ERROR', error:null} )
        }       

        if(match.url == '/checkout' && _.isEmpty(user))
            showLogin = true

        _this.setState( {..._this.state, match, cart, cartTotal, selected, user, showLogin, sidebar, error, loading:false} )
			
	}

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }

    actionAddToCart(e){
        
        const _this = this

        //let products = _this.state.products

        let selected = _this.state.selected

        let cart = _this.state.cart

        let cartTotal = _this.state.cartTotal

        let message = ''

        let intersection = [];


            if(!_.isEmpty(selected)) {
                _.map(selected, (v, i)=>{
                    //products[i].qty = 0
                    cartTotal.sub = parseFloat(cartTotal.sub) + (v.qty * v.price)
                    cartTotal.items = parseInt(cartTotal.items) + v.qty
                    message = message + v.qty + ' ' + v.name + ', '
                })
            }

            intersection = _.intersection(Object.keys(cart), Object.keys(selected))

            if(!_.isEmpty(intersection)) {
                _.map(intersection, (v, i)=>{
                    selected[v].qty = parseInt(cart[v].qty) + parseInt(selected[v].qty)
                })
            }
            cart = Object.assign( cart, _this.state.selected)


        _this.props.dispatch( {type: 'UPDATE-CART', cart, cartTotal, selected:[]} )

        _this.notificationSystem.addNotification({
            message: message + ' has successfully added to cart',
            level: 'success',
            autoDismiss:5
        });


        //cookie.save('logCart', cart, { path: '/' })
        
        //cookie.save('logCartTotal', cartTotal, { path: '/' })

        //return _this.setState({..._this.state, cart, selected:[]})

    }


    actionRemoveFromCart(e) {
        
        const _this = this

        let product_id = parseInt(e.currentTarget.getAttribute('data-id'))

        let { cart, cartTotal, selected } = _this.state

            cartTotal.sub = cartTotal.sub - ( cart[product_id].qty * cart[product_id].price )

            cartTotal.items = cartTotal.items - cart[product_id].qty


        _this.notificationSystem.addNotification({
            message: cart[product_id].name + ' has successfully deleted',
            level: 'error',
            autoDismiss:2
        });

        
        delete cart[product_id]

        _this.props.dispatch( {type: 'UPDATE-CART', cart, cartTotal, selected} )

        //cookie.save('logCart', cart, { path: '/' })
        
        //cookie.save('logCartTotal', cartTotal, { path: '/' })
        
        //return _this.setState({ ..._this.state, cart, cartTotal })
    }

    actionLoggedIn(e) {
        
        e.preventDefault()
        
        const _this = this
        
            let username = e.currentTarget.querySelectorAll('[name="username"]')[0].value
            let password  = e.currentTarget.querySelectorAll('[name="password"]')[0].value
        
        let _params = { 
            username,
            password
        }
        
        _this.setState( {..._this.state, loading:true} )
        
        return _this.props.dispatch( login(_params) );

    }

    actionLogout(e) {

        e.preventDefault()

        const _this = this

        _this.notificationSystem.addNotification({
            message: 'You have successfully logout',
            level: 'error',
            autoDismiss:2
        });

        
        _this.props.dispatch( logout() );

        //_this.setState({..._this.state, showLogin:true})

    }


    toggleSidebar() {

        const _this = this

        _this.props.dispatch(toggleSidebar( _this.state.sidebar ));

    }


    toggleLogin() {

        const _this = this

        const { match, user } = _this.state

        let showLogin = (_this.state.showLogin === true) ? false : true

            showLogin = (match.url == '/checkout' && _.isEmpty(user)) ? true : showLogin

        _this.props.dispatch( {type:'TOGGLE-LOGIN', showLogin} )

        //return _this.setState({..._this.state, showLogin})
    }

    render() {
		
		const _this = this

//console.log(_this.props)

/*
if(match.url == '/checkout' && _.isEmpty(user)) {
            showLogin = true
        }*/
        
        const register = (()=>{

            const loading = (_this.state.loading === true) ? 'loading' : ''

            if(_this.state.showLogin !== true)
                return

            return <div className={'popup ' + loading}>
                        <div className="box">

                    {
                        (()=>{
                            if(_this.state.match.url == '/checkout' && _.isEmpty(_this.state.user))
                                return <div>
                                        <Link to="/" ><span className="close">X</span></Link>
                                        <h6 className="text-danger">You must login before you can checkout</h6>
                                    </div>

                            return <span className="close" onClick={this.toggleLogin} >X</span>

                        })()
                    }
                            
                            <form style={ {marginBottom:'0px'} } name="register" onSubmit={ this.actionLoggedIn }>
                                <h3>LOGIN</h3>
                                <div className="form-group">
                                    <label for="username">Username/Email</label>
                                    <input type="text" name="username" className="form-control" placeholder="Enter Username/Email:" autocomplete="off" />
                                </div>
                                <div className="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" className="form-control" autocomplete="off"/>
                                </div>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-primary mb-2">Login</button>
                                </div>
                                <div class="form-group"></div>
                            </form>
                        </div>
                    </div>

        })()


        let cart = (()=>{
                
            let items = (()=>{

                let item = (()=>{

                    return _.map(_this.state.cart, (v,i)=>{

                        const imageUrl = (!_.isEmpty(v.images)) ? v.images[0].src : "//vapehq.co.nz/wp-content/uploads/2018/03/Artboard-2-copy.jpg";
                        return <li className="woocommerce-mini-cart-item mini_cart_item">
                                    <a className="remove" onClick={this.actionRemoveFromCart} data-id={i}>×</a>                                                 
                                    <a>
                                        <img src={imageUrl} alt={ v.name } />
                                        <span className="data">
                                            <span className="title"> { v.name } </span>                                
                                            <span className="quantity">{v.qty} × 
                                                <span data-rp-wcdpd-public-descriptions-product-pricing="{&quot;rp_wcdpd_301fc886&quot;:&quot;Range Pricing&quot;}">
                                                    <span className="woocommerce-Price-amount amount">
                                                        <span className="woocommerce-Price-currencySymbol"> $</span>{v.price}
                                                    </span>
                                                </span>
                                            </span>
                                        </span>                          
                                    </a>
                                </li>   
                    })

                })()


                return <ul className="woocommerce-mini-cart woocommerce cart_list product_list_widget ">
                            
                            { item }
                            
                        </ul>
            })()

            return <div className="header_cart collapse navbar-collapse" id="header-cart">
                        
                        { (()=>{
                            if(!_.isEmpty( _this.state.cart )) {

                                return  <div className="widget_shopping_cart_content">
                            
                                    { items }

                                    <p className="woocommerce-mini-cart__total total">
                                        <strong>Subtotal: </strong> 
                                        <span className="woocommerce-Price-amount amount">
                                            <span className="woocommerce-Price-currencySymbol">$</span> { _this.state.cartTotal.sub }
                                        </span>
                                    </p>

                                    
                                    <p className="woocommerce-mini-cart__buttons buttons">
                                        <Link to="/checkout"  className="button checkout wc-forward cl-btn btn-style-square btn-hover-darker" >Checkout</Link>
                                    </p>
                                </div>
                            } else {
                                return <h3 style={{ textAlign: 'center',margin: '30px' }}>{ '{ Empty Cart }' }</h3>
                            }

                        })() }

                        
                    </div>
        })()


        const myAccount = (()=>{

            let link = (()=>{

                if(!_.isEmpty(_this.state.user)) 
                    return <a href="#" onClick={ this.actionLogout }>Logout</a>
                return <a href="#" onClick={this.toggleLogin} >Login</a>
            })()

            return <div className="account">
                       <a href="#"><span className="fa fa-user"></span></a> | { link }
                    </div>
        })()

    return <nav className="navbar navbar-expand-lg navbar-light bg-light sticky">
                <div className="top-bar">
                    <h6>
                        <a href="#">VapeHQ</a>
                    </h6>
                    
                    { myAccount }

                </div>
                

            {
                (()=>{
                    if(_this.props.match.url == '/checkout')
                        return <div className="container-fluid">
                                    <Link to="/"  className="btn btn-info categories" >
                                        <span className="fa fa-home"></span>
                                    </Link>
                                </div>

                    return <div className="container-fluid">
                                <button type="button" onClick={ this.toggleSidebar } className="btn btn-info categories">
                                    <span className="fa fa-windows"></span>
                                </button>
                                <button type="button" className="btn btn-info add-to-cart" onClick={ this.actionAddToCart }>Add To Cart</button>
                                <button className="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#header-cart" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="fa fa-shopping-cart"></span>
                                    <span className="badge">{ _this.state.cartTotal.items }</span>
                                </button>

                                { cart }
                           </div>

                })()
            }
                    

                <NotificationSystem ref="notificationSystem" />

                { register }

            </nav>
 			  
	}
		
}


export default connect((store) => {
    return {
		store:store.main
    }
})(Navigation);
