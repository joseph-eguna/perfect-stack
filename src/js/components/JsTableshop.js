import React, { Component } from "react"
import { connect } from "react-redux"
import Promise from "promise"
import _ from "lodash"
import qs from 'qs'
import NotificationSystem from 'react-notification-system'

import { initialize, changeNavigation, addToCart, addProductsToCart } from "../actions/jstableshop/admin"


var timeOut

class JsTableShop extends Component {
    
    notificationSystem: null

    constructor(props) {
        super();

        this.state = {
            reloaded:false,
			products:[],
			selected:[]
        };

        this.actionNavigation = this.actionNavigation.bind(this);
		this.actionIncreaseDecreaseQty = this.actionIncreaseDecreaseQty.bind(this);
		this.actionSelectToCart = this.actionSelectToCart.bind(this);
		
		this.actionAddToCart = this.actionAddToCart.bind(this);
		this.actionAddSelectedToCart = this.actionAddSelectedToCart.bind(this);
    }

    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
     /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        /*const params = {
                limit : this.state.limitProducts,
                json: 1
            }
        this.props.dispatch( { type:'' } );
        this.props.dispatch(getProducts(params));*/

        //console.log('Component will mount')

    }

    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        _this.notificationSystem = _this.refs.notificationSystem;

        _this.props.dispatch(initialize());
        
		//_this.props.dispatch(elementOverflow());
        //console.log('Component Did Mount');
    }
    
    componentWillUnmount() {
        //console.log('Component Will Unmount');
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        const _this = this
		
		const { products:loadproducts, responses } = nextProps.store
		
		let products = []
		 
		 /*
		  * Assign initial qty value
		  * */
		 _.map(loadproducts, (v,i)=>{
				products[i] = { qty: 0 } 
				_.map(v.children,(vv, ii)=>{
						products[ii] = { price:vv.price, qty: 0, stock: vv.stock, backorders:vv.backorders, cart:vv.cart, parent_id:i } 
						/*if((responses.id) == ii) {
							products[ii] = {...products[ii], responses }
						}*/
					})
				return
			})
		
		//console.log( products, products[4112] )
		
		_this.setState( {..._this.state, reloaded:true, products, responses } )
		
		
        /*const { games } = nextProps;
        
        if(games.game) {

            let { element, coins, active, sets } = games.game
			
			coins = (_this.state.coins > coins) ? _this.state.coins : coins
			
			let complete = ( active >= sets.length ) ? true  : false
			
            _this.setState({..._this.state, coins, element:games.element, complete})

        }*/
        
        //this.setState({...this.state})
        //this.setState( {test:'dkjasndkas'} );
    }

    /*
     * 
     */
    shouldComponentUpdate(nextProps, nextState) {
        
        //console.log('Should Component Update', nextProps, nextState)
        return true;

    }
	
	/*
	 *  LOCAL FUNCTIONS
	 * */
	addToCartLoop( products ) {
		
		const _this = this
		
		let product = products[0]
		
		let stateproducts = _this.state.products
			stateproducts[product.id] = { ...stateproducts[product.id], status:'loading', buttonColor:'#FF9800' }
			
			_this.setState({..._this.state, products:stateproducts})
		
		let subtotal = (product.cart) ? parseFloat(product.cart.subtotal) : 0
			subtotal = subtotal + (parseInt(product.qty) * parseFloat(product.price))
			
		let _params = {
			product_id: product.id,
			parent_id:product.parent_id,
			quantity:product.qty,
			subtotal,
			action: 'woocommerce_add_to_cart'
		}
		
		
		console.log(product, _params)
		
		_this.props.dispatch(addToCart( qs.stringify(_params), _params ))
		
		if(products.length > 1) {
			products.shift()
			return this.addToCartLoop( products )
		}
		return;
	
		/*let _params = {
			product_id:product.id,
			quantity:product.qty,
			action: 'woocommerce_add_to_cart'
		}*/
		
		/*let executeAction = new Promise( (resolve, reject)=> {
					return resolve( _this.props.dispatch(addToCart( qs.stringify(_params), _params )) )
				} )
				
		return executeAction.then(()=>{
					if(products.length > 1) {
						products.shift()
						return this.addToCartLoop( products )
					}
					return;
				})*/
		
		//console.log(products, product)
	}
	
	actionIncreaseDecreaseQty(e) {
		
		const _this = this;
		
		let product_id = parseInt(e.currentTarget.getAttribute('data-id'))
		
		let products = _this.state.products
			
		if(e.currentTarget.getAttribute('data-action') == 'add') {
			products[product_id].qty = (products[product_id].stock > products[product_id].qty) ? products[product_id].qty + 1 : products[product_id].stock
			return _this.setState( {..._this.state, products} )
		} 
		products[product_id].qty = (products[product_id].qty > 0) ? products[product_id].qty - 1 : 0
		return _this.setState( {..._this.state, products} )
		
	}
	
	actionSelectToCart(e) {
		
		const _this = this
		
		let product_id = parseInt(e.currentTarget.getAttribute('value'))
		
		let selected = _this.state.selected
		    
		if(e.currentTarget.checked)
			selected.push(product_id)
		else
			_.pull(selected, product_id)
				
		selected = _.compact(_.uniq(selected))
		
		_this.setState( {..._this.state, selected} )
			
	}
	
		
	actionAddSelectedToCart(e){
		
		const _this = this
		
		let products = _.map(_this.state.selected, (v,i)=>{
				return {..._this.state.products[v], id:v}
			})
		
		//_this.props.dispatch( addProductsToCart(_this.state.selected) )
		_this.addToCartLoop( products )
 
	}
	
	
	actionAddToCart(e){
		
		const _this = this
		
		let product_id = parseInt(e.currentTarget.getAttribute('data-product-id'))
		
		let products = _this.state.products
			products[product_id] = { ...products[product_id], status:'loading', buttonColor:'#FF9800' }
		
		let parent_id = parseInt(products[product_id].parent_id)
		
		let subtotal = (products[product_id].cart) ? parseFloat(products[product_id].cart.subtotal) : 0
			subtotal = subtotal + (parseInt(products[product_id].qty) * parseFloat(products[product_id].price))
		
		let _params = {
			product_id,
			parent_id,
			quantity:_this.state.products[product_id].qty,
			subtotal,
			action: 'woocommerce_add_to_cart'
		}
		
				_this.setState({..._this.state, products})
				
		let output = _this.props.dispatch(addToCart( qs.stringify(_params), _params ))
			
			console.log(output)
		
		return
	}
	
	
	actionNavigation(e){
		
		const _this = this
		
		let _params = {
			taxonomy : e.currentTarget.getAttribute('data-taxonomy'),
			slug: e.currentTarget.getAttribute('data-slug'),
			action: 'jsTablehop'
		}
		
		_this.setState({..._this.state, reloaded:false})
		
		_this.props.dispatch(changeNavigation( _params ))
		
	}

    render() {
		
		const state = this.state
		
		//console.log(this.props.store);
		
		console.log(state);
		
		/*
		 * display product brands
		 * */
		let productBrands = (()=>{
				
				let menu = this.props.store.menu
				
				/* list */
				let list = (()=>{
									
							return _.map( menu, (v, i)=> {
								return <li className="cat-item">
											<a onClick={this.actionNavigation} data-taxonomy={ v.taxonomy } data-slug={ v.slug } title={ v.name }>
												{ v.name }					
											</a>
										</li>
								})
							})()
				
				/*content*/		
				return <div className="widget woocommerce widget_product_categories">
					<h3 className="widget-title" style={ {textAlign:'left'} }>Choose Brands</h3>
					<ul className="product-categories">
					
						{ list }
						
					</ul>
				</div>
				
				
            })()/*END*/
			
			
		/*
		 *  By Column Products
		 * */
		let productGrid = (()=>{
			
			let products = this.props.store.products
			
			let list = (()=>{
				
				return _.map(products, (v, i)=>{
					
					let price = ''
					let title = v.title
					
					return <li className="product_item style_normal" style={ {float:'Left'} }>
						<div className="inner-wrapper" style={ {padding:'15px'} }>
							<div className="cl-thumb-wrapper ">
						
								<a className="woocommerce-LoopProduct-link woocommerce-loop-product__link"></a>
								
								<div className="cl-actions">
									<div className="wrapper">
										<a data-quantity="1" className="button product_type_grouped" data-product_id={i} data-product_sku="1153110000000680081" aria-label="View products in the “Mooshake Banana 60ml” group" rel="nofollow">View products</a>			
										
										<a href={ "/wishlist/?add_to_wishlist="+i } data-product-id={i} data-product-type="grouped" className="cl-action add_to_wishlist ">
											<i className="cl-icon-heart-outline"></i>
										</a>
									
									</div>
								</div>
								{
									(()=>{
										let imageUrl = (v.img_url) ? v.img_url : 'https://via.placeholder.com/300x450'
										return <img src={ imageUrl } className="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" />	
									
									})()
								}

							</div>
				
							<h3 className="custom_font custom_font">
								{ title }
							</h3>
							
							<div className="cl-price-rating">
								<span className="price">
									<span className="woocommerce-Price-amount amount">{ price }</span>
								</span>
							</div>

						</div>
					</li>

					})
			
				})()
			
			return <ul className="shop-entries products columns-3 shop-products name- grid-entries animated-entries" data-grid-cols="3" data-columns-mobile="1">
					{ list }
				</ul>
			
			})()/*END*/
		
			 
		 /*
		  * Easy Order Form
		  * */
		  
		  let easyOrderForm = (()=>{
				
				let products = this.props.store.products
				
				let list = (()=>{
					
					return _.map(products,(v,i) => {
						
						let options = (()=>{
							
							if(!_.isEmpty(products[i].children)) {
							
									return _.map(products[i].children, (vv,ii)=>{
										
										let qty = (!_.isEmpty(this.state.products[ii])) ? this.state.products[ii].qty : 0;
										
										let addButton = (()=>{
												
												let style = { float: 'left', minWidth:'150px' }
													style.backgroundColor = (this.state.products[ii].buttonColor) ? this.state.products[ii].buttonColor : ''
												
												let text = (this.state.products[ii].status) ? this.state.products[ii].status : 'Add to cart'
												
												if(this.state.responses.id == ii) {
													style.backgroundColor = this.state.responses.buttonColor
													text = this.state.responses.status
												} 
													
												return <button onClick={this.actionAddToCart} style={ style } type="submit" name="add-to-cart" data-parent-id={ i } data-product-id={ ii } className="add-to-cart button alt cl-btn btn-style-square btn-hover-darker"> { text } </button>
											})()
										
										
										let productTotal = (()=>{
											
												let cart = (state.products[ii] && state.products[ii].cart) ? state.products[ii].cart : vv.cart
												if(cart) {
													return <h5 style={ {float:'right', color:'#eb5a46'} }>${ cart.subtotal.toFixed(2) } </h5>
												}
												return;
											})()
											
										return <tr>
													<td style={ {width:'70px', textAlign:'center', lineHeight:'50px'} }> 
														<h5>{ vv.title } </h5>
													</td>
													<td>
														
														<div className="quantity" style={ {overflow:'visible', float:'left',margin:'0 10px' } }>
															<span onClick={ this.actionIncreaseDecreaseQty } data-id={ii} data-action="add" class="cl-icon_other-plus inc-qty"></span>
															<input type="number" style={ {height:'52px'} }className="input-text qty text restrict-max" min="0" max={ vv.stock } title="Quantity" name="quantity" placeholder="0" value={ qty }/>
															<span onClick={ this.actionIncreaseDecreaseQty } data-id={ii} data-action="minus" class="cl-icon_other-minus dec-qty"></span>
														</div>
														
														{ addButton }
														
														{ productTotal }
													</td>
													<td style={ {width:'40px', textAlign:'center'} }>
														<input onClick={this.actionSelectToCart} type="checkbox" className="buy-multiple" name="buy[]" value={ ii } style={ {height:'40px'} } />
													</td>
												</tr>
									})
								
								}
								
							})()
						
						let imageUrl = (v.img_url) ? v.img_url : 'https://via.placeholder.com/300x450'
						
						return <tr>
									<td className="product-thumbnail" style={ {textAlign:'center'} }>
										<img src={ imageUrl } alt="Placeholder" className="woocommerce-placeholder wp-post-image" style={ {marginTop:'5px', width:'100px', height:'auto'} } />
									</td>

									<td className="product-name" data-title="Product" style={ {textAlign:'left'} }>
										<h4 style={ {textTransform:'uppercase', fontSize:'18px', lineHeight:'25px'} }>
											{ v.title }
										</h4>
										{ v.excerpt }
									</td>
									<td className="product-quantity" data-title="Quantity">	
										
										<table className="table table-bordered">
						  
											<thead>
												<tr><td colSpan="3"><h6 className="text-center">Select Options</h6></td></tr>
											</thead>
											<tbody>
												
												{ options }	
												<tr>
													<td colSpan="3">
														<a href="/cart" style={ {display:'block', textAlign:'center', lineHeight:'40px'} }>VIEW CART</a>
													</td>
												</tr>
											</tbody>
										</table>
										
																						
																
									</td>
								</tr>
						})
						
					})()
					
				return <div className="col-12">
					<h2 style={ {display:'block', marginTop:'40px', textAlign:'center', fontSize:'35px', textTransform: 'uppercase'} }>Easy Order Form</h2>
						<table className="table">
							<thead className="thead-dark">
								<tr>
								  <th scope="col" colSpan="2" className="text-center"><h3>PRODUCT</h3></th>
								  <th scope="col" style={ {width:'60%', textAlign:'right'} }>
									<a onClick={this.actionAddSelectedToCart} className="add-to-cart button alt cl-btn btn-style-square btn-hover-darker">Add Selected</a>
								  </th>
								</tr>
							</thead>
							<tbody>
							
								{ list }
								
							</tbody>
						</table>
					</div>
			  })() /*END*/
			
		
		if(!_.isEmpty(this.props.store.menu)) {
			
			return ( 
				<div>
					<aside className="col-md-3">
						{ productBrands }
					</aside>
					
					<div className="col-md-9">
						{ productGrid }
						<div className="clearfix"></div>
						{ easyOrderForm }
						
						
					{
						(()=>{
							if(!this.state.reloaded) {
								return <div class="product-content-overlay" style={ {background:'#ffffffc7', width:'100%', position:'absolute', top: 0, height: '100%' } }>
									<h3>Loading...</h3>
								</div>
							}
						})()
					}
					
					</div>
					<NotificationSystem ref="notificationSystem" />
				</div>
			 )
		}
		return (<h3>Loading...</h3>)
		
    }
}


export default connect((store) => {
    return {
		store:store.main
    }
})(JsTableShop);

/**<!---a className="cl-action expand cl-quick-view" data-id="4898">
											<i className="cl-icon-arrow-expand"></i>
										</a-*/