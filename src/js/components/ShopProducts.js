import React, { Component } from "react"
import { connect } from "react-redux"
import axios from "axios"
import NotificationSystem from 'react-notification-system';

import { deleteProduct, getProducts } from "../actions/productActions"
import Header from "./ShopProducts/Header";
import List from "./ShopProducts/List";
import ProductDetail from "./ShopProducts/ProductDetail";

class ShopProducts extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
            list: [],
            limitProducts : 5,
            images: [],
            token: ''
        };
        
        //this.changeState = this.changeState.bind(this);  
        //this.actionDeleteProduct = this.actionDeleteProduct.bind(this);
        this.deleteProduct = this.deleteProduct.bind(this);
        this.addNotification = this.addNotification.bind(this);
    }

    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    
    /*
     * Run system intial state
     * @return initialialization of state
     */
    componentDidMount() {
        
        const _this = this;
        
        _this.notificationSystem = _this.refs.notificationSystem;

        /*return;
        axios.get('/admin/shop/products', {
                params: { json: 1 } 
        }).then( (response) => {

            var { products, count_products:countProducts, images } = response.data;

            _this.setState( { 
                products, 
                countProducts,
                images,
            } );
        })
        .catch(function (error) {
            //console.log(error);
        });

        return*/
        
    }


    /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        const params = {
                limit : this.state.limitProducts,
                json: 1
            }
        this.props.dispatch(getProducts(params));
    }


    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        this.setState(nextProps.products);
    }


    /*
     * Get current update
     * @return change state 
     */
    /*getData(params) {

        var _this = this;

        var url = '/admin/shop/products';

        var params = (params) ? params : { json: 1 };

        axios.get( url, {
                params: params
            })
            .then( (response) => {
                
                var { products, images } = response.data;
                
                _this.setState( { 
                    products, 
                    images,
                } );
                return
            })
            .catch(function (error) {
                //console.log(error);
            });
        return;
    }*/

    /*
     * Changing state
     * @action event action
     */
    /*changeState(params) {

        if(params)
            this.getData(params);
        else
            this.getData();
    
    }*/


    /*
     * @return action for deleting product
     */
    deleteProduct(e) {

        e.preventDefault();
        
        const _this = this;
        const _url = '/admin/shop/products/delete';
        const _data = {
            id : e.target.getAttribute('data-id'),
            name: e.target.getAttribute('data-name'),
            action : e.target.getAttribute('data-action'),
        }

        //temporary action -       
        axios.post(_url, _data).then( (response) => {
            
            const { result, message } = response.data;

            if(result == 'success') {
                _this.addNotification(
                    {
                        message: message,
                        level: 'success',
                        position: 'tl'
                    }
                );
                
                const _params = {
                    offset : (this.state.offset) ? this.state.offset : 0,
                    limit : this.state.limit,
                    json: 1
                }

                _this.props.dispatch(getProducts(_params));
            } else {
                response.data.forEach((v,i) => {
                    _this.addNotification(
                        {
                            message: v,
                            level: 'error',
                        }
                    );
                });
                return;
            }

        }).catch( (error) =>{
            error.forEach((v,i) => {
                _this.addNotification(
                    {
                        message: v,
                        level: 'error',
                    }
                );
            });
        });
    }


    /*deleteProductx(e) {
        
        e.preventDefault();
        
        const _this = this;
        const _url = '/admin/shop/products/delete';
        const _data = {
            id : e.target.getAttribute('data-id'),
            name: e.target.getAttribute('data-name'),
            action : e.target.getAttribute('data-action'),
        }

        this.props.dispatch(deleteProduct(_data,this.triggerChanges()));
    }

    
    triggerChanges() {

        const _params = {
                offset : (this.state.offset) ? this.state.offset : 0,
                limit : this.state.limit,
                json: 1
            }

        console.log(_params);
        this.props.dispatch(getProducts(_params));
    }


    triggerNofication() {
        
        const _this = this;
        let   _notify = (this.props.products.notify) ? this.props.products.notify : false;


        //console.log(this.props);

        if(_notify) {
            if(typeof _notify.messages != 'string') {
                _notify.messages.map((v,i) => {
                    _this.addNotification({
                        level: _notify.level,
                        message: v
                    })
                });
            } else {
                _this.addNotification({
                    level: _notify.level,
                    message: _notify.messages
                })  
            }
        }
    }*/


    render() {

        
        return ( 

<div>
    

    <div className="page-content-wrapper ">
        <div className="padding-25"></div>
        <div className="content">
            <div className="container-fluid container-fixed-lg">
                <div className="row">
                    <div className="col-md-12 main-content">
            

        <div className="panel panel-transparent">
            
            
            
            <Header />

            <List deleteProduct={this.deleteProduct}/>
        </div>  

                    </div>
                </div>
            </div>
         </div>
    </div>


    <ProductDetail addNotification={this.addNotification} />
    
    <NotificationSystem ref="notificationSystem" />
</div>
         );  
    }
}


export default connect((store) => {
    return {
        products: store.products.products
    }
})(ShopProducts);



/*

<a href="#" onClick={this.deleteProduct}>Hellox</a>

<List 
                actionDeleteProduct={this.actionDeleteProduct}
                changeState={this.changeState} 
                products={ this.state.list } 
                countProducts={ this.state.countProducts } 
                limitProducts={ this.state.limitProducts } />


<ProductDetail addNotification={this.addNotification} 
                   changeState={this.changeState} 
                   token={ this.state.token } 
                   images={ this.state.images } />

var find = {
    Closest : (e, element,deep=5) => {
        let i = 0;
        let target = e.target;
        for(i=0;i<deep;i++){
            target = target.parentNode;
            if(target.nodeName == element) {
                return target;
            }
        }
        //return false;
    },
    Child : (el) => {
        //
    }
};*/
