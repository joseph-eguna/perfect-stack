import React, { Component } from "react"
import { connect } from "react-redux"
import axios from "axios"


class Header extends Component {
  
    constructor() {
        super();
        this.state = { };

        this.actionAdd = this.actionAdd.bind(this);
        this.actionFilter = this.actionFilter.bind(this);
    }

    componentDidMount() {
 
        var _this = this;
       
    }

    actionFilter(e) {

        e.preventDefault();

        console.log('Change response from laravel');
        return;


        const _params = {
                limit : this.state.limit,
                status : _status,
                json: 1
            }
        
        this.setState({ status:_status});
        this.props.dispatch(getProducts(_params));

    }

    actionAdd(e) {
        const product = { detail : {name:"",sku:"",price:"",description:""} };
              this.props.dispatch( { type:"FETCH_PRODUCTS_FULLFILLED", payload:{ product } } );
    }

    render() {

            
        return ( 


<div>
    <div className="panel-heading row">
        <a href="#" onClick={this.actionAdd} className="btn btn-default pull-right modal-popup" data-target="#modal-slide-up">
            Add Product
        </a>
        <div className="panel-title">
            <h3>Products</h3> </div>
    </div>
    <div className="row">
        <div className="col-md-3 col-md-offset-9 pull-right">
            <form className="" onSubmit={this.actionFilter}>
                <input type="text" placeholder="Seach" className="form-control" /> 
            </form>
        </div>
    </div>
</div>

         );  
    }
}

export default connect((store) => {
    return {
        products: store.products.products
    }
})(Header);
