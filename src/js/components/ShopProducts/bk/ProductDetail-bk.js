import React from "react";
import axios from "axios"


export default class ProductDetail extends React.Component {
  
    constructor() {
        super();

        this.state = { };

        this.actionFormSubmit = this.actionFormSubmit.bind(this);
        this.actionUploadImage = this.actionUploadImage.bind(this);
        this.actionDeleteImage = this.actionDeleteImage.bind(this);
        //this.allocateSubmit = this.allocateSubmit.bind(this);
    }


    /*
     * Initialization of component
     */
    componentDidMount() {
 
        var _this = this;
       
    }

    /*
     * Image uploader
     * @return action ajax
     */
    imageUploader(url, data) {

        axios.post( url, data )
            .then( (response) =>  {
                console.log(response.data);
                
                //_this.props.changeState();

                return true;
            })
            .catch( (error) =>  {
                return false
            });
        return false;
    }

    /**
     * Uploading product images
     */ 
    actionUploadImage(e) {

        //console.log(e.target);

        var _this = this;
        
        var url = '/admin/shop/products/upload'; //e.target.getAttribute('action');

        //var data = new FormData();
            //data.append( 'file', e.target.files );
            //data.append( 'filename', e.target.files[0].name.split(' ').join('-').toLowerCase())
            //data.append( 'action', 'upload' );
           
            //console.log(e.target.files[0]);
            //console.log(e.target.files[0].name.split(' ').join('-').toLowerCase());
            //console.log("Screenshot-from 2017-05-25 13-47-23.png".replace(' ', '+'))
            //return;

        //console.log(e.target.files.length);

        //let i = 0;
        //for(i=0;i<e.target.files.length;i++) {

            var data = new FormData();
                data.append( 'file', e.target.files[i] );
                data.append( 'action', 'upload' );

            this.imageUploader(url, data);

            console.log(e.target.files[i]);
        
        //}

       return; 

        axios.post( url, data )
            .then( (response) =>  {
                console.log(response.data);
                //_this.props.changeState();
            })
            .catch( (error) =>  {
                    //
            });


        /*var config = {
            headers: { 'X-CSRF-TOKEN': _this.props.token },
            onUploadProgress: function(progressEvent) {
              var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
            }
        };*/

        //console.log(this.props.token);
        //console.log(data);

        

        /*console.log(data);
        axios.post( url, {
                params: data
            })
            .then( (response) => {
                
                console.log(response.data);

            })
            .catch(function (error) {
                //console.log(error);
            });*/


    }



    /**
     * Deleting product images
     */
    actionDeleteImage(e) {

        console.log(e.target.getAttribute('src'));

        var _this = this;

        var url = '/admin/shop/products/upload';

        axios.post( url, {
            action : 'delete',
            file_url : e.target.getAttribute('data-src')
        } )
            .then( (response) =>  {
                console.log(response.data);
                _this.props.changeState();
            })
            .catch( (error) =>  {
                    //
            });

    }



    /**
     * On form submission
     * @return action 
     */
    actionFormSubmit(e) {

        e.preventDefault();
        
        var url = e.target.getAttribute('action');

        /*axios.post( url, {
                params: { json: 1 }
            })
            .then( (response) => {
                
                console.log(response.data);

            })
            .catch(function (error) {
                //console.log(error);
            });*/
    }


    render() {

        //console.log(this.state);

        let images = this.props.images.map( (image, i) => {
            return  <div className="col-md-3">
                        <img onClick={this.actionDeleteImage} src={ "http://dev.legiontraining.com/uploads/app/"+ image } data-src={image} style={ {padding:'5px', maxWidth:'100%'} } />
                    </div>
        });

        return ( 


    <div className="modal fade slide-right in" id="modal-slide-up" tabindex="-1" role="dialog" aria-hidden="false">
        <div className="modal-dialog modal-lg">
            <div className="modal-content-wrapper">
                <div className="modal-content">
                    <div className="modal-header clearfix text-left">
                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><i className="pg-close fs-14"></i> </button>
                        <h5> ADD PRODUCT </h5>
                        <div className="response"></div>
                    </div>
                    <div className="modal-body" style={ {minHeight:'100vh'} }>

            
        <form className="workouts-action" onSubmit={this.actionFormSubmit} method="POST" action="/admin/shop/products/upload">

            <div className="response"></div>

            <div className="panel panel-transparent tabs">
                <ul className="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                    <li className="active">
                        <a data-toggle="tab" href="#add-product-content"><span>Main</span></a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#add-product-image"><span>Select Image</span></a>
                    </li>
                    
                </ul>
                <div className="tab-content">

                    <div className="tab-pane active" id="add-product-content">
                        <div className="form-group">
                            <div className="col-md-6">
                                <img src="https://s3.amazonaws.com/weighttraining.com/data/512/content.jpg?1318811220" style={ {width:'100%'} } />
                            </div>
                            <div className="col-md-6">
                                <label>SKU:</label>
                                <input type="text" placeholder="Enter SKU:" name="name" className="form-control" />
                                <br />
                                <label>Price:</label>
                                <input type="text" placeholder="Enter Price:" name="name" className="form-control" />
                            </div>
                            <br />
                            <label>Name:</label>
                            <input type="text" placeholder="Enter Nmae:" name="name" className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Enter Full Description:</label>
                            <div className="summernote-wrapper">
                                <div id="description"></div>
                            </div>
                        </div>
                        <div className="form-group row" style={ {marginTop:'20px'} }>
                            <div className="col-md-6">
                                <label>SKU:</label>
                                <input type="text" placeholder="Enter Product Name:" name="name" className="form-control" />
                                <br />
                                <label>Categories:</label>
                                <select className="form-control">
                                    <option> option1 </option>
                                    <option> option2 </option>
                                    <option> option3 </option>
                                    <option> option4 </option>
                                    <option> option5 </option>
                                </select>
                            </div>
                             <div className="col-md-6">
                                <label>Price:</label>
                                <input type="text" placeholder="Enter Price:" name="name" className="form-control" />
                            </div>
                         </div>
                         <div className="col-md-12 row">
                            <div className="col-sm-8">

                            </div>
                            <div className="col-md-4 m-t-10 sm-m-t-10">
                                <button type="submit" className="btn btn-primary btn-block m-t-5">SAVE</button>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                    <div className="tab-pane" id="add-product-image">
                        <div className="form-group text-right">
                            <input type="file" onChange={this.actionUploadImage} id="file-upload" action="/admin/shop/products/upload" data-url="/admin/ingredients/upload" name="photo" className="form-control" multiple/>
                        </div>
                        <div className="form-group">
                            { images }
                        </div>
                    </div>

                </div>
            </div>
        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

         );  
    }
}
