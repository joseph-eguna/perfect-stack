import React, { Component } from "react"
import { Table, Row, Col, ButtonGroup, Button, ListGroup, ListGroupItem, Form, FormGroup, Label, Input, FormText } from 'reactstrap'
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom"
import DatePicker from "react-datepicker"
import Moment from 'react-moment'
import { connect } from "react-redux"
import axios from "axios"
import _ from "lodash"
import qs from "qs"
import NotificationSystem from 'react-notification-system'
import CSSTransitionGroup  from 'react-transition-group/CSSTransitionGroup'
import Pagination from "react-js-pagination";
import serialize from "form-serialize"


import { retrieveSettings, updateStack } from "../actions/synchWooZoho/admin"


class JsWoozohoConnect extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
            settings:{
                reoccurance:"daily",
                time:2
            }                 
        };
        
        /*
        stackListsPaginate: {
                active:1,
                offset:1,
                limit:10,
                total:100
            }
        */
        this.actionChangeText = this.actionChangeText.bind(this);
        this.actionSaveStack = this.actionSaveStack.bind(this);
        this.actionSelectTime = this.actionSelectTime.bind(this);
        
    }

    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    /*addNotification(data){
        this.notificationSystem.addNotification(data);
    }*/

    /*
     * Dispatch initially the list of props from redux
     */
    componentDidMount() {

        this._notificationSystem = this.refs.notificationSystem;

        let _params = {
            action:'swz_ajax',
            job: 'retrieve-settings'            
        }
        this.props.dispatch(retrieveSettings(qs.stringify(_params)));
        
    }

    

    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        this.setState(nextProps.index);

        if(this.state.stack) {
            const { name:stackName, description:stackDescription } = this.state.stack;
            const fields = {...this.state.fields, stackName, stackDescription};
            this.setState( {fields} );
        }
       
    }

     actionChangeText(e) {
        
        const target = e.target.getAttribute('name');
        
        let value = e.target.value;

        let settings = {...this.state.settings, [target]:value};

        this.setState( {settings} );

    }

    actionSaveStack(e) {

        let _params = {
            action:'swz_ajax',
            job: 'save-settings',
            APIkey: this.state.settings.APIkey,
            organizationId: this.state.settings.organizationId,
            customerId:this.state.settings.customerId
        }

        this.props.dispatch(retrieveSettings(qs.stringify(_params), this._notificationSystem));
        
        /*if(_.isEmpty(this.state.stack)) 
            this.props.dispatch(saveStack(qs.stringify(_params), this._notificationSystem));
        else {
            _params.job = 'update-stack';
            this.props.dispatch(updateStack(qs.stringify(_params), this._notificationSystem)); //updateStack
        }*/

    }
    
    actionSelectTime(e) {
        
        const target = e.target.getAttribute('data-v');
        
        let value = e.target.value;

        console.log(target);
        
        //let settings = {...this.state.settings, [target]:value};

        //this.setState( {settings} );
        
    }

    render() {
        
        /*const calendarStrings = {
            lastDay : '[Yesterday at] LT',
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            lastWeek : '[last] dddd [at] LT',
            nextWeek : 'dddd [at] LT',
            sameElse : 'L'
        };
        
        const dateToFormat = '1976-04-19T12:59-0500';*/
        //console.log(this.state)
        
        const _this = this;
        
        return ( 
            
<div>
    
    <Col sm="12">
        <h2 style={ {marginTop:'40px'} } >Synch Zoho Inventory</h2>
        <hr />
    </Col>
    
    {(()=>{
        if(!_.isEmpty(this.state.settings)) {
            return <Col sm="4">
                
                <FormGroup>
                  <h4>API Key</h4>
                  <Input type="text" name="APIkey" value={this.state.settings.APIkey} placeholder="Enter Zoho API Key" onChange={this.actionChangeText} ref={(input)=>{ this.APIkey = input}}/>
                </FormGroup>
                
                <FormGroup>
                  <h4>Organization ID</h4>
                  <Input type="text" name="organizationId" value={this.state.settings.organizationId} placeholder="Enter Zoho Organization ID" onChange={this.actionChangeText} ref={(input)=>{ this.organizationId = input}}/>
                </FormGroup>
                
                <FormGroup>
                  <h4>Zoho Customer ID</h4>
                  <Input type="text" name="customerId" value={this.state.settings.customerId} placeholder="Enter Zoho Customer ID" onChange={this.actionChangeText} ref={(input)=>{ this.customerId = input}}/>
                </FormGroup>
               
                <FormGroup style={ {marginTop: '40px'} }>
                    <h4>Synchronize Schedule</h4>
                    <ButtonGroup>
                        <Button color="primary" size="lg">Daily</Button>
                        <Button color="secondary" size="lg">Weekly</Button>
                    </ButtonGroup>
                </FormGroup>

                <FormGroup>
                    <h4>Select Time </h4>
                    
                    <ListGroup style={ {maxHeight:'200px',overflow:'hidden', overflowY:'scroll',borderRadius:'10px', border:'1px solid #DDD'} }>
                    {(()=>{
                        
                            const scheduledTime = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
                     
                            return _.map( scheduledTime, function(v, k){
                                
                                let sT = new Date()
                                    sT.setHours(v,0,0,0)
                                
                                const active = (_this.state.settings.time == v) ? true : false;
                                
                     console.log(active);
                     
                                return <ListGroupItem active={active} onClick={_this.actionSelectTime} data-v={v}>
                                            <Moment format="hh:mm:ss A">
                                                {sT}
                                            </Moment>
                                        </ListGroupItem>
                            } );
                        
                    })()}
                    </ListGroup>






                </FormGroup>

                <FormGroup>
                    <br />
                    <Button color="primary" onClick={this.actionSaveStack}>Save Changes</Button>
                </FormGroup>
            </Col> 
        }
    })()}
    
    <Col sm="8">
    </Col>

    
    <NotificationSystem ref="notificationSystem" />
</div>

         );  
    }
}

export default connect((store) => {
    return {
        index: store.wooZoho
    }
})(JsWoozohoConnect);

