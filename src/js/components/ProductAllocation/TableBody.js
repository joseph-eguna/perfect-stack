import React from "react";
import axios from "axios"

import TableRowAction from "./TableRowAction";
import FormAllocate from "./FormAllocate";

export default class TableBody extends React.Component {
  
    constructor() {
        super();

        
    }

    componentDidMount() {

        //

    }


    render() {

        /*if(this.props.data) {
            var products = this.props.data;
            Object.keys(this.props.data).forEach( function(key,i) {
                console.log(products[key].ID);
            });
        }*/

                    /*<tr id={"product-"} className="iedit author-self level-0 post-39 type-product status-publish hentry">
                        <th scope="row" className="check-column">
                            <input id={"cb-select-"+ID} type="checkbox" value={ID} />
                        </th>
                        <td className="thumb" style={ {'text-align':'center'} }>
                            <a href="http://local.templates.cru.zone/wp-admin/post.php?post=39&action=edit">
                                <img src={product.image_url} alt="Placeholder" height="200" className="woocommerce-placeholder wp-post-image" />
                            </a>
                        </td>
                        <td className="name column-name has-row-actions column-primary" data-colname="Name">
                            <strong><a className="row-title" href="http://local.templates.cru.zone/wp-admin/post.php?post=39&action=edit">{products[ID].name}</a></strong>
                            
                            <TableRowAction />

                            <button type="button" className="toggle-row">
                                <span className="screen-reader-text">Show more details</span>
                            </button>
                        </td>
                        <td className="sku column-sku" data-colname="SKU"><span className="na"> </span>
                        </td>
                        <td className="is_in_stock column-is_in_stock" data-colname="Stock">
                            <mark className="instock">{product.stock_status}</mark>
                        </td>
                        <td className="price column-price" data-colname="Price"><span className="woocommerce-Price-amount amount"><span className="woocommerce-Price-currencySymbol">$
                            </span>{product.price}</span>
                        </td>
                        <td className="product_cat column-product_cat" data-colname="Categories"><span className="na">–</span>
                        </td>
                        <td className="product_tag column-product_tag" data-colname="Tags"><span className="na">–</span>
                        </td>
                        <td className="featured column-featured" data-colname="Featured"><a href="http://local.templates.cru.zone/wp-admin/admin-ajax.php?action=woocommerce_feature_product&product_id=39&_wpnonce=8256c9a7d0" aria-label="Toggle featured"><span className="wc-featured not-featured tips">No</span></a>
                        </td>
                        <td className="product_type column-product_type" data-colname="Type"><span className="product-type tips simple"></span>
                        </td>
                        <td className="date column-date" data-colname="Date">Published
                            2017/06/22
                        </td>
                    </tr>*/

        var products = this.props.data;
        
        var display = Object.keys(this.props.data).map( ID => {
                
            var product = {
                name : products[ID].name,
                image_url : (products[ID].image_url) ? products[ID].image_url : '/wp-content/uploads/2017/06/wine-1.png',
                stock_status : products[ID].stock_status,
                price: products[ID].price
            };

            return <tr id={"product-"} className="iedit author-self level-0 post-39 type-product status-publish hentry">
                        <th scope="row" className="check-column">
                            <input id={"cb-select-"+ID} type="checkbox" value={ID} />
                        </th>
                        <td className="thumb" style={ {textAlign:'center'} }>
                            <a href={ "/wp-admin/post.php?post="+ID+"&action=edit" }>
                                <img src={product.image_url} alt="Placeholder" height="200" className="woocommerce-placeholder wp-post-image" />
                            </a>
                        </td>
                        <td className="name column-name has-row-actions column-primary" style={ {paddingTop:'20px'} }>
                            <strong>
                                <a className="row-title" href={ "/wp-admin/post.php?post="+ID+"&action=edit" }>
                                    { product.name }
                                </a>
                            </strong>
                            <TableRowAction />
                            <p style={ {marginBottom:0} }> 
                                {"SKU: 213KKS01"}
                            </p>
                            <mark className="instock" style={ {textTransform:'uppercase'} }>
                                {product.stock_status}
                            </mark>
                            <p style={ {marginBottom:0} }> 
                                {"PRICE:" + product.price}
                            </p>

                            <button type="button" className="toggle-row">
                                <span className="screen-reader-text">Show more details</span>
                            </button>
                        </td>
                        
                        <td style={ {paddingTop:'20px'} }>
                            
                            <FormAllocate product={product} users={this.props.users}/>
                        
                        </td>
                        <td className="" style={ {paddingTop:'20px'} }>
                            { "{ JOHN PAUL:SUBMITTED, PAULINE:APPROVED, JOSEPH:PENDING }" }
                        </td>
                        <td className="date column-date">
                            <p>{ "Brgy. Fiesta" }</p>
                            <mark className="instock" style={ {textTransform:'uppercase'} }>active</mark>
                            <p><strong>{ "15 June" }</strong> { " - " }<strong>{ "30 June 2017" }</strong></p>
                        </td>
                    </tr>;

        });
       

        //console.log(this.props.data);

        return <tbody id="the-list"> {display} </tbody>;  
    }
}
