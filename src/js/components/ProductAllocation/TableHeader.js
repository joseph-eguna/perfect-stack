import React from "react";
import axios from "axios"

export default class TableHeader extends React.Component {
  
    constructor() {
        super();

        
    }

    componentDidMount() {

        //

    }


    render() {

        /*
        <tr>
            <td className="manage-column column-cb check-column">
                <label className="screen-reader-text">Select All</label>
                <input id="cb-select-all-2" type="checkbox" />
            </td>
            <th scope="col" className="manage-column">
                <span className="wc-image tips">Image</span>
            </th>
            <th scope="col" className="manage-column column-namex column-primary sortable desc" style={ {width:'300px'} }>
                <a href="#">
                    <span>Name</span><span className="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" className="manage-column column-skux sortable desc">
                <a href="http://local.templates.cru.zone/wp-admin/edit.php?post_type=product&amp;orderby=sku&amp;order=asc">
                    <span>SKU</span><span className="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" className="manage-column column-is_in_stock">Stock</th>
            <th scope="col" className="manage-column column-price sortable desc">
                <a href="http://local.templates.cru.zone/wp-admin/edit.php?post_type=product&amp;orderby=price&amp;order=asc">
                    <span>Price</span><span className="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" className="manage-column column-product_cat">Categories</th>
            <th scope="col" className="manage-column column-product_tag">Tags</th>
            <th scope="col" className="manage-column column-featured">
                <span className="wc-featured parent-tips" data-tip="Featured">Featured</span>
            </th>
            <th scope="col" className="manage-column column-product_type">
                <span className="wc-type parent-tips" data-tip="Type">Type</span>
            </th>
            <th scope="col" className="manage-column column-date sortable asc">
                <a href="http://local.templates.cru.zone/wp-admin/edit.php?post_type=product&amp;orderby=date&amp;order=desc">
                    <span>Date</span><span className="sorting-indicator"></span>
                </a>
            </th>       
        </tr>
        */
        //console.log(this.state.products);

        return (
            
        <tr>
            <td className="manage-column column-cb check-column">
                <label className="screen-reader-text">Select All</label>
                <input id="cb-select-all-2" type="checkbox" />
            </td>
            <th scope="col" className="manage-column">
                <span className="wc-image tips">Image</span>
            </th>
            <th scope="col" className="manage-column column-namex column-primary sortable desc" style={ {width:'300px'} }>
                <a href="#">
                    <span>Name</span><span className="sorting-indicator"></span>
                </a>
            </th>
            
            <th scope="col" className="manage-column" style={ {width:'150px'} }>Allocate</th>
            <th scope="col" className="manage-column" style={ {width:'400px'} }>Assigned</th>
            <th scope="col" className="manage-column column-date sortable asc">
                <a href="#">
                    <span>PERIOD</span><span className="sorting-indicator"></span>
                </a>
            </th>       
        </tr>

        );  
    }
}
