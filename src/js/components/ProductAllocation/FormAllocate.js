import React from "react";
import axios from "axios"

export default class FormAllocate extends React.Component {
  
    constructor() {
        super();
        
        this.state = {
            users: false,
            usersearch:"",
            userselected:[]
        };
        
        this.allocateSubmit = this.allocateSubmit.bind(this);
        this.updateUsersearch = this.updateUsersearch.bind(this);
        this.selectUser = this.selectUser.bind(this);
    }

    componentDidMount() {
        this.setState({users:this.props.users});
    }

    allocateSubmit(e) {
        
        e.preventDefault();
        console.log(this.state);

    }

    updateUsersearch(e) {
        //e.target.value;
        this.setState({ usersearch : e.target.value });
    }

    selectUser(e) {
        
        e.preventDefault();
        var value = e.target.getAttribute("value") ;
        var selected = this.state.userselected.slice();
        
        if(selected.indexOf(value) == -1)
            selected.push( value );

        this.setState({ userselected: selected });

        this.setState({ usersearch : "" });
    }

    render() {

        let product = this.props.product;
        
        //console.log(this.state.usersearch);   

        var _this = this; 
        let filteredUsers = this.props.users.filter(function(user){
            //console.log(user.display_name);
            return (user.user_email.toLowerCase().indexOf( _this.state.usersearch ) !== -1 && _this.state.usersearch !== "");
        });

        var addedUser = "";
        if(this.state.userselected.length > 0) {
            var addUserlist = this.state.userselected.map( user => {
                return <a>{user}</a>
            });
            addedUser = <div className="added-user">{addUserlist}</div>;
        }

        //var  test = <ul className="added-userx"></ul>
        //console.log(filteredUsers.slice(0,2));
        //console.log(this.state.userselected);

        return ( <form className="allocate" onSubmit={this.allocateSubmit}>
                    <label style={ {display:'block'} }>set pricex</label>
                        <input type="text" style={ {maxWidth:'100%'} } value={product.price}/>
                    <label style={ {display:'block'} }>set quantity</label>
                        <input type="text" style={ {maxWidth:'100%'} }/>
                    <label style={ {display:'block'} }>select user</label>
                        {addedUser}
                        <input type="text" style={ {maxWidth:'100%'} } value={this.state.usersearch} users={this.props.users} onChange={this.updateUsersearch}/>
                        <ul className="users-list">
                            { filteredUsers.slice(0,10).map( (user) => {
                                return <li >  
                                            <a href="#" value={user.user_email} onClick={this.selectUser} >{user.user_email}</a>
                                       </li>  
                            }) }
                        </ul>
                    <input type="submit" className="btn btn-primary" value="allocate" style={ {marginTop:'5px'} }/>
                </form> );  
    }
}
