import React from "react";
import axios from "axios"

export default class Action extends React.Component {
  
    constructor() {
        super();

        
    }

    componentDidMount() {

        //

    }


    render() {


        //console.log(this.state.products);

        return (
            
            <div className="tablenav top">
                <div className="alignleft actions bulkactions">
                    <label for="bulk-action-selector-top" className="screen-reader-text">Select bulk action</label>
                    <select name="action" id="bulk-action-selector-top">
                        <option value="-1">Bulk Actions</option>
                        <option value="edit" className="hide-if-no-js">Edit</option>
                        <option value="trash">Move to Trash</option>
                    </select>
                    <input type="submit" id="doaction" className="button action" value="Apply"/> 
                </div>
                <div className="alignleft actions">
                    <label for="filter-by-date" className="screen-reader-text">Filter by date</label>
                    <select name="m" id="filter-by-date" defaultValue="0">
                        <option value="0">All dates</option>
                        <option value="201706">June 2017</option>
                    </select>
                    <select name="product_type" id="dropdown_product_type">
                        <option value="">Show all product types</option>
                        <option value="simple">Simple product</option>
                        <option value="downloadable"> → Downloadable</option>
                        <option value="virtual"> → Virtual</option>
                    </select>
                    <input type="submit" name="filter_action" id="post-query-submit" className="button" value="Filter"/> 
                </div>
                <div className="tablenav-pages one-page">
                    <span className="displaying-num">5 items</span> 
                    <span className="pagination-links">
                        <span className="tablenav-pages-navspan" aria-hidden="true">«</span> 
                        <span className="tablenav-pages-navspan" aria-hidden="true">‹</span> 
                        <span className="paging-input">
                            <label for="current-page-selector" className="screen-reader-text">Current Page</label>
                            <input className="current-page" id="current-page-selector" type="text" name="paged" value="1" size="1" aria-describedby="table-paging"/>
                            <span className="tablenav-paging-text"> of 
                                <span className="total-pages">1</span>
                            </span>
                        </span> 
                        <span className="tablenav-pages-navspan" aria-hidden="true">›</span> 
                        <span className="tablenav-pages-navspan" aria-hidden="true">»</span>
                    </span>
                </div>
            </div>

        );  
    }
}
