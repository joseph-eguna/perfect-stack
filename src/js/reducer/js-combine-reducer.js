import { combineReducers } from "redux"

import wooZoho from "./js-reducer"

export default combineReducers({
	wooZoho
})