export default function reducer(state={
	settings : { 
        reoccurance:"daily",
        time:1 }
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "RETRIEVE-SETTINGS": {

			const { settings } = action;

			state = { ...state, settings };

			return state;
		}
            
        case "GET-STACKS": {

			const { total, limit, offset, active, stacks } = action;

			state.stackListsPaginate = { ...state.stackListsPaginate, total, limit, offset, active }

			state = {...state, stackLists:stacks};

			return state;
		}
		
		case "GET-STACK": {
			return {...state, stack:action.stack };
		}

		case "GET-STACK-FINDER": {

			let { stackFinder } = state;

				//let  { gender, mealsPerDay } = action.stackFinder;

			const { gender, 
	              meals_per_day:mealsPerDay, 
	              age:ageGroup, 
	              goal, 
	              weight_training:weightTraining, 
	              cardio, 
	              structured_meal_plan:structuredMealPlan, 
	              meal_consistency:mealConsistency, 
	              meal_plan:mealPlan } = action.stack;			

			const actionStackFinder = {
				gender, ageGroup, goal, weightTraining, cardio, 
				structuredMealPlan, mealConsistency, mealPlan
			};

			_.forEach(actionStackFinder, (v,i)=>{

					let object = JSON.parse(v);
					//set all values to false
					_.forEach(stackFinder[i], (vv,ii)=>{

						let indexOf = _.indexOf(object, ii);

						if(indexOf != -1) 
							stackFinder[i][ii] = true;
						else
							stackFinder[i][ii] = false;

					});

			})	
			
			return {...state, stackFinder}
		
		}
		case "UPDATE-STACK": {
			return {...state, productStackLists:action.products }
		}
		case "ADD-TO-STACK": {
			return {...state, productStackLists:action.products }
		}
		case "REMOVE-FROM-STACK": {
			return {...state, productStackLists:action.products }
		}

		case "GET-SEARCH-PRODUCTS": {
			//console.log(action.products);
			return {...state, productSearchLists:action.products }
		}
		case "GET-PRODUCT-TYPES": {
			return {...state, productTypes:action.types }
		}
		case "GET-PRODUCT-BRANDS": {
			return {...state, productBrands:action.brands }
		}
		default: 
      		return state;
	}

	//return state;

	/*switch(action.type) {
		case "FETCH_ORDERS": {
			return {...state, fetching:true}
		}
		case "FETCH_ORDERS_REJECTED": {
			return {...state, fetching:false, error:action.payload}
		}
		case "FETCH_ORDERS_FULLFILLED": {
			return {...state, fetching:false, fetched:true, orders:action.payload}
		}
		default: 
      		return state;
	}*/
}