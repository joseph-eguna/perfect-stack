import { combineReducers } from "redux"

import wooZoho from "./wooZoho"

export default combineReducers({
	wooZoho
})