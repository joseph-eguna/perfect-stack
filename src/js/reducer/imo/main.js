export default function reducer(state={
	game:[],
	element:[]
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "INITIALIZE-GAMES": {

			const { load } = action;

			state = { ...state, game:load };

			//console.log(load);

			return state;
		}
            
        case "ELEMENT-OVERLOWED": {

			const { load } = action;

			state = { ...state, element:load };

			//console.log(load);

			return state;
			
		}
		
		default: 
      		return state;
	}


}