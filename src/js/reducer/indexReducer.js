export default function reducer(state={
	index : {
		currentUser:[],
		getGoals:[
				  'Drop Body fat levels',
		          'Increase Tone and Muscle condition',
		          'Drop Body fat while increasing Tone and Muscle condition',
		          'Increase Lean Muscle mass',
		          'Drop Body fat while increasing Lean Muscle mass'
		          ],
		getPrograms:['Pre-conditioning',
					 'Legion for life',
					 'Boulders for shoulders']
	},
	fetching : false,
	fetched : false,
	error: null
}, action)  {

	//const { query } = action; 
	
	let payload = {};

	if(action.payload) {
		payload.currentUser = ('undefined' != typeof action.payload.currentUser) ?  action.payload.currentUser : state.index.currentUser;
		payload.getGoals    = ('undefined' != typeof action.payload.getGoals) ?  action.payload.getGoals : state.index.getGoals;
		payload.getPrograms = ('undefined' != typeof action.payload.getPrograms) ?  action.payload.getPrograms : state.index.getPrograms;
	}

	switch(action.type) {
		case "FETCH_INDEX": {
			return {...state, fetching:true}
		}
		case "FETCH_INDEX_REJECTED": {
			return {...state, fetching:false, error:payload}
		}
		case "FETCH_INDEX_FULLFILLED": {
			return {...state, fetching:false, fetched:true, payload}
		}

		default:
      		return state;
	}
}	