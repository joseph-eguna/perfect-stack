import _ from "lodash"
import cookie from 'react-cookies'

export default function reducer(state={
	products: [],
	productOffset:0,
	children: [],
	selected:[],
	cart:{},
	cartTotal: { sub:0, items:0 },
	categories:[],
	sidebar:'inactive',
    user: (!_.isEmpty(cookie.load('logUser'))) ? cookie.load('logUser') : null,
    showLogin:false,
    paymentGateways:[],
    loading:false,
    response:[],
    error:null,
    headers:[]
    
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "INITIALIZE": {

			let { products } = action;

			//	products = Object.assign(state.products, products)

			//console.log(Object.assign({}, state.products, products), products)

			state = { ...state, products, sidebar:'inactive' };

			//console.log(load);

			return state;
		}


		case "PRODUCTS": {

			let { products, loadmore, headers } = action;

			let count = state.products.length

			let Products = state.products 


			if(loadmore) {

				let k
				if(products.length > 0) {
					for (k in products) {
					  	Products[parseInt(k) + parseInt(count)] = products[k]
					}
				}

			} else {
				
				Products = products 
			
			}

			
			
			//console.log(Products)	

				//products = (loadmore) ? Object.assign(state.products, products) : products

			state = { ...state, products:Products, headers, sidebar:'inactive' };

			//console.log(load);

			return state;
		}


		case "PRODUCT-CHILDREN": {

			const { children } = action

			let load = state.children

				load[Object.keys(children)[0]] = children

			state = { ...state, children:load }

			return state
		}


		case "PRODUCT-CATEGORIES" : {

			const { categories } = action

			state = { ...state, categories }

			return state

		}

		case "UPDATE-CART" : {

			const { cart, cartTotal, selected } = action

			state = { ...state, cart, cartTotal, selected }

			return state

		}

        case "GET-PAYMENT_GATEWAYS": {
            
            const { gateways } = action;

			state = { ...state, paymentGateways:gateways };
		
			return state;
            
		}
            
        case "GET-ORDER-RESPONSE": {
            
            const { response } = action;

			state = { ...state, response, cart:{}, cartTotal:{ sub:0, items:0 } };
		
			return state;
            
		}

        case "REGISTER-USER": {

        	let { user, error, showLogin } = action
            
            state = { ...state, user, error, showLogin }
            
			return state
		}

		case "LOGOUT-USER": {

            const { user } = action
            
            state = { ...state, user }
            
			return state
		}

		case "TOGGLE-SIDEBAR": {

            const { sidebar } = action
            
            state = { ...state, sidebar }
            
			return state
		}

		case "TOGGLE-LOGIN": {

            const { showLogin } = action
            
            state = { ...state, showLogin }
            
			return state
		}


		case "CLEAR-ERROR": {

            const { error } = action
            
            state = { ...state, error }
            
			return state
		}

		//TO BE DELETED
		case "CLEAR-SELECTED": {

            const { selected } = action
            
            state = { ...state, selected }
            
			return state
		}

         
		default: 
      		return state;
	}


}