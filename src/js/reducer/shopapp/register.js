export default function reducer(state={
	products: [],
	cart:{},
	cartTotal: { sub:0, items:0 },
	categories:[],
	children: [],
	sidebar:'inactive',
    user:null,
    error:null
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "INITIALIZE": {

			const { products } = action;

			state = { ...state, products, sidebar:'inactive' };

			//console.log(load);

			return state;
		}


		case "PRODUCT-CHILDREN": {

			const { children } = action

//console.log(Object.keys(children)[0]);
//return state;

			let load = state.children

				load[Object.keys(children)[0]] = children

			state = { ...state, children:load }

			return state
		}


		case "PRODUCT-CATEGORIES" : {

			const { categories } = action

			state = { ...state, categories }

			return state

		}
            
        case "REGISTER-USER": {

            const { user, error } = action
            
            state = { ...state, user, error }
            
			return state
		}

		case "LOGOUT-USER": {

            const { user } = action
            
            state = { ...state, user }
            
			return state
		}

		case "TOGGLE-SIDEBAR": {

            const { sidebar } = action
            
            state = { ...state, sidebar }
            
			return state
		}
         
		default: 
      		return state;
	}


}