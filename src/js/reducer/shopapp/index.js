import { combineReducers } from "redux"

import main from "./main"
import checkout from "./checkout"

export default combineReducers({
	main,
    checkout
})