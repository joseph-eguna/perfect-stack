import { combineReducers } from "redux"

import main from "./mainReducer"
import index from "./indexReducer"
import products from "./productsReducer"
import overview from "./overviewReducer"

export default combineReducers({
	index,
	overview,
	products
})