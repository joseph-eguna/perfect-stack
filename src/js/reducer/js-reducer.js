export default function reducer(state={
	product : { test: 'product1'},
	productStackLists: [],
	productSearchLists: {},
	productBrands:[],
	productTypes:[],
	stack:[],
	stackLists:[],
	stackListsPaginate: {},
	stackFinder: {
		gender: {male:false,female:false},
		ageGroup: {'age-20-and-under':false, 'age-20-to-30':false, 'age-30-to-40':false, 'age-40-to-50':false, 'age-50-to-60':false, 'age-60-plus':false},
		goal: {'goal-sports-performance':false, 'goal-primary-fat-loss':false, 'goal-lean-muscle-development':false, 'goal-increase-in-averall-muscle-and-mass':false, 'goal-general-health-fitness':false, 'goal-body-transformation':false},
		weightTraining: {'weight_training-50-less':false,'weight_training-50-greater':false},
		cardio: {'undefined':false, 'cardio-steady-state':false, 'cardio-hiit':false, 'cardio-combination-of-steady-state-and-hiit':false},
		structuredMealPlan: {'structured_meal-plan':false,'undefined':false},
		mealConsistency: {'meal_consistency-50-less':false,'meal_consistency-50-greater':false},
		mealPlan: {'meal-1to2':false,'meal-2to3':false,'meal-3to4':false,'meal-4to5':false,'meal-5to6':false,'meal-6-plus':false},
	},
	fetching : false,
	fetched : false,
	error: null
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "GET-STACKS": {

			const { total, limit, offset, active, stacks } = action;

			state.stackListsPaginate = { ...state.stackListsPaginate, total, limit, offset, active }

			state = {...state, stackLists:stacks};

			return state;
		}
		
		case "GET-STACK": {
			return {...state, stack:action.stack };
		}

		case "GET-STACK-FINDER": {

			let { stackFinder } = state;

				//let  { gender, mealsPerDay } = action.stackFinder;

			const { gender, 
	              meals_per_day:mealsPerDay, 
	              age:ageGroup, 
	              goal, 
	              weight_training:weightTraining, 
	              cardio, 
	              structured_meal_plan:structuredMealPlan, 
	              meal_consistency:mealConsistency, 
	              meal_plan:mealPlan } = action.stack;			

			const actionStackFinder = {
				gender, ageGroup, goal, weightTraining, cardio, 
				structuredMealPlan, mealConsistency, mealPlan
			};

			_.forEach(actionStackFinder, (v,i)=>{

					let object = JSON.parse(v);
					//set all values to false
					_.forEach(stackFinder[i], (vv,ii)=>{

						let indexOf = _.indexOf(object, ii);

						if(indexOf != -1) 
							stackFinder[i][ii] = true;
						else
							stackFinder[i][ii] = false;

					});

			})	
			
			return {...state, stackFinder}
		
		}
		case "UPDATE-STACK": {
			return {...state, productStackLists:action.products }
		}
		case "ADD-TO-STACK": {
			return {...state, productStackLists:action.products }
		}
		case "REMOVE-FROM-STACK": {
			return {...state, productStackLists:action.products }
		}

		case "GET-SEARCH-PRODUCTS": {
			//console.log(action.products);
			return {...state, productSearchLists:action.products }
		}
		case "GET-PRODUCT-TYPES": {
			return {...state, productTypes:action.types }
		}
		case "GET-PRODUCT-BRANDS": {
			return {...state, productBrands:action.brands }
		}
		default: 
      		return state;
	}

	//return state;

	/*switch(action.type) {
		case "FETCH_ORDERS": {
			return {...state, fetching:true}
		}
		case "FETCH_ORDERS_REJECTED": {
			return {...state, fetching:false, error:action.payload}
		}
		case "FETCH_ORDERS_FULLFILLED": {
			return {...state, fetching:false, fetched:true, orders:action.payload}
		}
		default: 
      		return state;
	}*/
}