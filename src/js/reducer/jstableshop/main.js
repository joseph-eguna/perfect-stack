export default function reducer(state={
	menu: [],
	products: [],
	responses:[]
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "INITIALIZE": {

			const { menu, products } = action.load;

			state = { ...state, menu, products };

			//console.log(load);

			return state;
		}
		
		case "ADDED-TO-CART": {
			
			const { product_id, parent_id, quantity, subtotal } = action.load;
			
			let products = state.products
				products[parent_id]["children"][product_id].cart = {...products[parent_id]["children"][product_id].cart, subtotal }
			
			let responses = state.responses
			
				responses = { ...responses, action: 'added-to-cart', 
											status:'Added', 
											buttonColor: '#4CAF50',  
											id: product_id, 
											qty:quantity }
				
			state = { ...state, products, responses };

			return state;
			
		}
		
		case "SUCCESSFULLY-ADDED-TO-CART": {

			state = { ...state, responses:{} };

			return state;
			
		}
		
        case "ELEMENT-OVERLOWED": {

			const { load } = action;

			state = { ...state, element:load };

			//console.log(load);

			return state;
			
		}
		
		default: 
      		return state;
	}


}