import { combineReducers } from "redux"

import main from "./mainReducer"
import products from "./productsReducer"
import orders from "./ordersReducer"

export default combineReducers({
	main,
	products,
	orders
})