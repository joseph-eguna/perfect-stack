export default function reducer(state={
	game:[],
	element:[]
}, action) {

	//stackListsPaginate: {},

	switch(action.type) {

		case "INITIALIZE-GAMES": {

			const { load } = action;

			state = { ...state, game:load };

			//console.log(load);

			return state;
		}
            
        case "ELEMENT-OVERLOWED": {

			const { load } = action;

			state = { ...state, element:load };

			//console.log(load);

			return state;
			
		}
		
		case "GET-STACK-FINDER": {

			let { stackFinder } = state;

				//let  { gender, mealsPerDay } = action.stackFinder;

			const { gender, 
	              meals_per_day:mealsPerDay, 
	              age:ageGroup, 
	              goal, 
	              weight_training:weightTraining, 
	              cardio, 
	              structured_meal_plan:structuredMealPlan, 
	              meal_consistency:mealConsistency, 
	              meal_plan:mealPlan } = action.stack;			

			const actionStackFinder = {
				gender, ageGroup, goal, weightTraining, cardio, 
				structuredMealPlan, mealConsistency, mealPlan
			};

			_.forEach(actionStackFinder, (v,i)=>{

					let object = JSON.parse(v);
					//set all values to false
					_.forEach(stackFinder[i], (vv,ii)=>{

						let indexOf = _.indexOf(object, ii);

						if(indexOf != -1) 
							stackFinder[i][ii] = true;
						else
							stackFinder[i][ii] = false;

					});

			})	
			
			return {...state, stackFinder}
		
		}
		
		default: 
      		return state;
	}


}