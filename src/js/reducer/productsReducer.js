export default function reducer(state={
	products : {
		list : false,
		count: 0,
		images :[],
		limit:10
	},
	fetching : false,
	fetched : false,
	error: null
}, action)  {

	//const { query } = action; 

	let payload = {};

		if(action.payload) {
			payload.list 	= ('undefined' != typeof action.payload.list) ?  action.payload.list : state.products.list;
			payload.count 	= ('undefined' != typeof action.payload.count) ?  action.payload.count : state.products.count;
			payload.images 	= ('undefined' != typeof action.payload.images) ?  action.payload.images : state.products.images;
			payload.limit 	= ('undefined' != typeof action.payload.limit) ?  action.payload.limit : state.products.limit;
			payload.offset 	= ('undefined' != typeof action.payload.offset) ?  action.payload.offset : state.products.offset;
				//payload.sort 	= ('undefined' != typeof action.payload.sort) ?  action.payload.sort : state.products.sort;
				//payload.order_by 	= ('undefined' != typeof action.payload.order_by) ?  action.payload.order_by : state.products.order_by;
			payload.sizes 	= ('undefined' != typeof action.payload.sizes) ?   action.payload.sizes : state.products.sizes;
			payload.colors 	= ('undefined' != typeof action.payload.colors) ?  action.payload.colors : state.products.colors;
			payload.categories 	= ('undefined' != typeof action.payload.categories) ?  action.payload.categories : state.products.categories;
			payload.tags 	= ('undefined' != typeof action.payload.tags) ?  action.payload.tags : state.products.tags;
			
			//payload.product = ('undefined' != typeof action.payload.product) ?  action.payload.product : {};
			payload.product = action.payload.product;
			payload.notify = action.payload.notify;
		}
		

	switch(action.type) {
		case "FETCH_PRODUCTS": {
			return {...state, fetching:true}
		}
		case "FETCH_PRODUCTS_REJECTED": {
			return {...state, fetching:false, error:payload}
		}
		case "FETCH_PRODUCTS_FULLFILLED": {
			return {...state, fetching:false, fetched:true, products:payload}
		}

		default:
      		return state;
	}
}	