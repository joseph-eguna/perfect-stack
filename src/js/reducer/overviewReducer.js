export default function reducer(state={
	overview : {
		bodyStat : [],
		programStat : [],
		currentStat : [],
		generalStat: []
	},
	fetching : false,
	fetched : false,
	error: null
}, action)  {

	//const { query } = action; 
	
	let payload = (action.payload) ? action.payload : state.overview;
		payload = action.payload;
	/*if(action.payload) {
		payload.list 	= ('undefined' != typeof action.payload.list) ?  action.payload.list : state.products.list;
		payload.count 	= ('undefined' != typeof action.payload.count) ?  action.payload.count : state.products.count;
		payload.images 	= ('undefined' != typeof action.payload.images) ?  action.payload.images : state.products.images;
		payload.limit 	= ('undefined' != typeof action.payload.limit) ?  action.payload.limit : state.products.limit;
		payload.offset 	= ('undefined' != typeof action.payload.offset) ?  action.payload.offset : state.products.offset;
			//payload.sort 	= ('undefined' != typeof action.payload.sort) ?  action.payload.sort : state.products.sort;
			//payload.order_by 	= ('undefined' != typeof action.payload.order_by) ?  action.payload.order_by : state.products.order_by;
		payload.sizes 	= ('undefined' != typeof action.payload.sizes) ?   action.payload.sizes : state.products.sizes;
		payload.colors 	= ('undefined' != typeof action.payload.colors) ?  action.payload.colors : state.products.colors;
		payload.categories 	= ('undefined' != typeof action.payload.categories) ?  action.payload.categories : state.products.categories;
		payload.tags 	= ('undefined' != typeof action.payload.tags) ?  action.payload.tags : state.products.tags;
		
		//payload.product = ('undefined' != typeof action.payload.product) ?  action.payload.product : {};
		
		payload.product = action.payload.product;
		payload.notify = action.payload.notify;
	}*/
		

	switch(action.type) {
		case "FETCH_OVERVIEW": {
			return {...state, fetching:true}
		}
		case "FETCH_OVERVIEW_REJECTED": {
			return {...state, fetching:false, error:payload}
		}
		case "FETCH_OVERVIEW_FULLFILLED": {
			return {...state, fetching:false, fetched:true, payload}
		}

		default:
      		return state;
	}
}	