import axios from "axios"

/*
 * @Initialize 
 */
export function initialize() {
	
	return (dispatch) => {

		axios.get( WPajax.ajax_url, {
				params: {
				  action: 'jsTablehop',
				  
				}
			} ).then((response) => {
			
				console.log(response);
				
			let data = response.data
	
			dispatch( { type:"INITIALIZE", load:data } );
			
			return;
			
		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})

	}
}


/*
 * @Change the menu navigation
 */
export function changeNavigation( _params ) {

	return (dispatch) => {
		
		axios.get( WPajax.ajax_url, { params: _params } ).then((response) => {
	
		//console.log(response)
		
			let data = response.data
	
			dispatch( { type:"INITIALIZE", load:data } );
			
			return;
			
		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})

	}
}

/*
 * @
 */
export function addToCart( _params, _rawparams ) {

	return (dispatch) => {
		
		/*
		 *  Add to cart
		 * */
		return axios.post( WPajax.ajax_url, _params ).then((response) => {
		
			/*
			 *  reloaded first shop tool icon
			 * */
			axios.get( "", { params: _params } ).then((response) => {
				let el = document.createElement('html')
					el.innerHTML = response.data
				
				let html = el.getElementsByClassName( 'shop tool' )[0].children[0].innerHTML
				document.getElementsByClassName( 'shop tool' )[0].children[0].innerHTML = html;
				return;
			}).catch((error) => { /*dispatch({type:"FETCH_INDEX_REJECTED", payload:error});*/ })
			
			/*
			 *  reloaded first shoppping widget
			 * */
			axios.get( WPajax.ajax_url, { params: { 'action': 'mode_theme_update_mini_cart' } } ).then((response) => {

					let html = response.data
					document.getElementsByClassName( 'widget_shopping_cart_content' )[0].innerHTML = html
					return;
				}).catch((error) => { /*dispatch({type:"FETCH_INDEX_REJECTED", payload:error}); */ })	
			
			
			dispatch( { type:"ADDED-TO-CART", load:_rawparams } );
			
			setTimeout(()=>{
					dispatch( { type:"SUCCESSFULLY-ADDED-TO-CART" } );
				}, 2000)
			
		}).catch((error) => { /*dispatch({type:"FETCH_INDEX_REJECTED", payload:error});*/ })
		
	}
}



/*
 * @
 */
export function addProductsToCart( array ) {

	return (dispatch) => {
		
		/*
		 *  Add to cart
		 * */
		return axios.get( '', {} ).then((response) => {
		
			console.log(array) 
			
			array = array.shift()
			
			return  addProductsToCart(array)
			
		}).catch((error) => { /*dispatch({type:"FETCH_INDEX_REJECTED", payload:error});*/ })
		
	}
}