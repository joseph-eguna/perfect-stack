import axios from "axios"
import NotificationSystem from 'react-notification-system';

const notificationSystem = null;

export function fetchProducts() {
	return {
		type : "FETCH_PRODUCTS_FULLFILLED",
		payload : {
			notify : 'hello'
		}
	}
}

/*
 * Get the list of products
 * @param params object parameter reference
 */
export function getProducts(params) {

	params = (params) ? params : { json: 1 };

	return (dispatch) => {

		axios('/admin/shop/products',{ params }).then((response) => {
			
			const { products:list, count_products:count, limit, offset, status, sort, order_by, images } = response.data;
			const payload = { list, count, limit, offset, status, sort, order_by, images };
				  
				  dispatch( { type:"FETCH_PRODUCTS_FULLFILLED", payload } );
		
		}).catch((error) => {
			dispatch({type:"FETCH_PRODUCTS_REJECTED", payload:error});
		})
	}
}

/*
 * Get the list of products
 * @param params object parameter reference
 */
export function getProduct(id, params) {

	params = (params) ? params : {};

	return (dispatch) => {

		axios('/admin/shop/products/'+id,{ params }).then((response) => {
			
			let { product } = response.data;
			product.forEach((v,i)=>{
				product[v.key] = JSON.parse(v.value);
				product.detail = v;
			});

			const payload = { product };
	
		    dispatch( { type:"FETCH_PRODUCTS_FULLFILLED", payload } );
		
		}).catch((error) => {
			dispatch({type:"FETCH_PRODUCTS_REJECTED", payload:error});
		})
	}
}

/*
 * Get the list of product attributes
 * @param string name
 */
export function getProductAttributes(name) {

	let payload = {};
	switch(name) {
		case "sizes": {
			payload.sizes = ["Small","Medium","Large","XL","XXL","XXXL"]
			break;
		}
		case "colors": {
			payload.colors = ["Yellow","Red","Blue","Orange","White","Black"]
			break;
		}
		case "categories": {
			payload.categories = ["Men","Women","Kids"]
			break;
		}
		case "tags": {
			payload.tags = ["T-shirts","Pants","Underwear","Accesories"]
			break;
		}
		default: {
			payload = []
      		break;
		}
      		
	}
	return {
		type : "FETCH_PRODUCTS_FULLFILLED",
		payload
	}
}


/*
 * @param object reference
 */
export function deleteProduct(param,callback) {


	/*const _this = this;
    const _url = '/admin/shop/products/delete';
    const _param = (param) ? param : {};

    
    return (dispatch) => {
    	axios.post(_url, _param).then( (response) => {

	        const { result, message } = response.data;

	        let notify = {};

	        if(result == 'success') {
	            
	            notify.level = 'success';
	            notify.messages = message;

	            callback();
	            dispatch({type : "FETCH_PRODUCTS_FULLFILLED", payload : { notify }});
	        
	        } else {

        		notify.level = 'error';
        		notify.messages = [];

	            response.data.forEach((v,i) => {
	            	notify.messages[i] = v;
				});

	            dispatch({type : "FETCH_PRODUCTS_FULLFILLED", payload : { notify }});
	        }

	    }).catch( (error) =>{
			let notify = {};
	    		notify.level = 'error'
	    		notify.messages = error;
			dispatch({type : "FETCH_PRODUCTS_REJECTED", payload : notify });
	    });
    }*/
}



export function fetchProductsReal(data) {
	return (dispatch) => {
		axios('/admin/shop/products',{ params: { json: 1 } }).then((response) => {

			const reducer = {type:"FETCH_PRODUCTS_FULLFILLED", payload:data};
				  //reducer = {...reducer, data };
			
			dispatch( reducer );

		}).catch((error) => {
			dispatch({type:"FETCH_PRODUCTS_REJECTED", payload:error});
		})
		//dispatch({type:"FETCH_PRODUCTS_FULLFILLED", payload:{name: 'dispatching'}});
	}
	//return function(){
		/*axios('/admin/shop/products',{ params: { json: 1 } }).then((response) => {

			return {type:"FETCH_PRODUCTS_FULLFILLED", payload:response.data.products};
		
		}).catch((error){
			dispatch({type:"FETCH_PRODUCTS_REJECTED", payload:error});
		})*/
	//}
}


/*getProducts() {
        return axios.get('http://local.templates.cru.zone/wp-json/product-allocation/allocate/get-allocation-products/',{
            headers: {'X-WP-Nonce': wpApiSettings.nonce}
        });
    }*/
    
/*axios.all([this.getProducts(), getProducts()])
            .then(axios.spread(function (products, users, period) {
                
                _this.setState({ 
                    products : products.data,
                    users : users.data,
                    period: period.data });
        }));*/