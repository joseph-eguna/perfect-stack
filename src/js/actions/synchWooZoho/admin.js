import axios from "axios"


/*
 * @retrieveSettings
 */
export function retrieveSettings(_params, _notificationSystem) {

	return (dispatch) => {


		axios.post(ajaxurl, _params ).then((response) => {
			
			//console.log(response.data);
            
            //return;
            
			const { message, settings } = response.data;

            if(message == "success") {
               
                dispatch( { type:"RETRIEVE-SETTINGS", settings } );

                _notificationSystem.addNotification({
                    message: 'Successfully saved!',
                    level: 'success', 
                    position: 'tl'
                });
            
            }                

			return;

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}


/*
 * @updateStack
 */
export function updateStack(_params, _notificationSystem) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {

				//console.log(response.data);
			
			const { stack, products } = response.data;
		

			dispatch( { type:"UPDATE-STACK", products } );
			dispatch( { type:"GET-STACK", stack } );
			dispatch( { type:"GET-STACK-FINDER", stack } );

			_notificationSystem.addNotification({
		        message: 'Changes has successfully saved!',
		        level: 'success',
		        position: 'tl'
		    });

			return;
			
		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

