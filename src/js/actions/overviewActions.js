import axios from "axios"

/*
 * @body statistics
 */
export function getOverview(params) {

	params = (params) ? params : { json: 1 };

	return (dispatch) => {

		axios.get('/',{ params }).then((response) => {
			
			//const { products:list, count_products:count, limit, offset, status, sort, order_by, images } = response.data;
			//const payload = { list, count, limit, offset, status, sort, order_by, images };
		
			const { body_stat:bodyStat, program_stat:programStat, current_stat:currentStat, general_stat:generalStat } = response.data;
			const payload = { bodyStat, programStat, currentStat, generalStat };	  

				  dispatch( { type:"FETCH_OVERVIEW_FULLFILLED", payload } );
		
		}).catch((error) => {
			dispatch({type:"FETCH_OVERVIEW_REJECTED", payload:error});
		})
	}
}
