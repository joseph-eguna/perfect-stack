import axios from "axios"
/*
 * @Initialize games
 */
export function initializeGames() {
	
//let ajax_url = "/json/games-json-easy.json"

	return (dispatch) => {

		axios.get( ajax_url ).then((response) => {
			
			//let data = response.data
			
			let data = response.data.data[0]
			
			//console.log( data )         

			dispatch( { type:"INITIALIZE-GAMES", load:data } );

			return

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})

	}
}


/*
 * @Save coins, status, active
 */
export function saveGames( params ) {

	let ajaxurl = ajax_url + '&coins=' + params.coins + '&active=' + params.active + '&status=' + params.status
	
		//console.log(ajaxurl)
		//return;
	
	return (dispatch) => {

		axios.get( ajaxurl ).then((response) => {
			
			console.log(response.data)
			
			//let data = response.data.data[0]
	
			//dispatch( { type:"INITIALIZE-GAMESx", load:data } );

			return

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})

	}
}

/*
 * @page overflow
 */
export function elementOverflow() {
	
	return (dispatch) => {
		
		setTimeout(()=>{
			let contentDisplay = document.getElementById('display-content').getBoundingClientRect()
			
			let piggyBankPosition = document.getElementById('piggy-bank').getBoundingClientRect()
				piggyBankPosition = { 
					top:piggyBankPosition.top + (window.pageYOffset || document.documentElement.scrollTop) + document.getElementById('piggy-bank').clientHeight - contentDisplay.top,
					left: piggyBankPosition.left +(window.pageXOffset || document.documentElement.scrollLeft)
				}
			
			/*let coinBankPosition = document.getElementById('coin-bank').getBoundingClientRect()
				coinBankPosition = { 
					top:coinBankPosition.top + (window.pageYOffset || document.documentElement.scrollTop) + document.getElementById('coin-bank').clientHeight - contentDisplay.top,
					left: coinBankPosition.left +(window.pageXOffset || document.documentElement.scrollLeft)
				}*/
			
			dispatch( { type:"ELEMENT-OVERLOWED", load:{ piggyBankPosition, coinBankPosition:{}, contentDisplay } } );
				
		}, 100)

	}
}