import axios from "axios"
import _ from "lodash"

export function toggleSidebar( sidebar ) {

	sidebar = (sidebar == 'inactive') ? 'active' : 'inactive';

	return (dispatch)=>{

		return dispatch( {type: 'TOGGLE-SIDEBAR', sidebar} )

	}
}