import axios from "axios"
import _ from "lodash"
import qs from 'qs'
import cookie from 'react-cookies'

/*
 * @Logged In 
 */
export function login( _params ) {
	
	return (dispatch) => {
        
        axios( {
            url: __WcAPI__.url + '/wp-json/wcapp/v1/register/',
            method: 'post',
            data: _params
        }).then( (response) => {
			
            const output =  response.data
            
            if(_.isEmpty(output.errors)){
                
                const user = {
                    id: output.auth.ID,
                    email: output.auth.data.user_email,
                    key: output.auth.data.user_pass,
                    roles: output.auth.roles,
                    fields: output.fields
                }
                
                const expires = new Date()
                      expires.setDate(expires.getDate() + 30)

console.log(expires)

                cookie.save('logUser', user, { path: '/', expires })
                
                return dispatch( { type:"REGISTER-USER", user, error:null, showLogin:false } )
            }
            
            return dispatch( { type:"REGISTER-USER", user:null, error:output.errors, showLogin:true } )
            
		}).catch( (error) =>{

			console.log(error);
		
		}); 

	}
}


/*
 * Logout
 */
export function logout() {
	
	return (dispatch) => {
        
        const user = null
                
        cookie.remove('logUser', { path: '/' })

        return dispatch( { type:"LOGOUT-USER", user } )

	}
}