import axios from "axios"
import WooCommerceAPI from 'woocommerce-api'
import _ from "lodash"
import qs from 'qs'

const WooCommerce = new WooCommerceAPI({
	  url: __WcAPI__.url,
	  consumerKey: __WcAPI__.key,
	  consumerSecret: __WcAPI__.sec,
	  wpAPI: true,
	  version: 'wc/v3'
});

const globalVar = {
	categoryOnMobile: 263,
	categorySidebarExclude: [263, 15],
	categoryIntialize:219,
	productsPerPage:10
}

/*
 * @Initialize 
 */
export function getProducts( _params, loadmore ) {
	
	return (dispatch) => {
		
			let { category, offset } = _params


			_params = { per_page: globalVar.productsPerPage,
						offset:parseInt( offset * globalVar.productsPerPage ),
						parent:0, 
						status:'publish', 
						type: 'grouped', 
						category:globalVar.categoryIntialize }


			_params = (!_.isEmpty(category)) ? { ..._params, category } : _params

//console.log(_params)

			_params = '?' + qs.stringify(_params)


			WooCommerce.get('products' + _params, function(err, data, result) {
			
				const httpHeaders = data.getAllResponseHeaders().split(/\r?\n/)

				let productArray = [], headers = {}, k, key, value

				for( k in httpHeaders) {

					key = httpHeaders[k].split(':')
					value = key[1]
					key = key[0]

					if(key != "")
						headers[key] = value

				}

//console.log(parseInt(headers['x-wp-totalpages']))

				result = JSON.parse(result)

				_.map(result, (v,i)=>{
				
					if(!_.isEmpty(v.grouped_products)) {

						let include = v.grouped_products;

						WooCommerce.get('products?' + qs.stringify({ include, order:'asc' }), function(err, data, resultii) {
			
							let children = []

								children[v.id] = JSON.parse(resultii)

							dispatch( { type:"PRODUCT-CHILDREN", children } )

						});
							
					}

					//v = { [v.id] : v }

					//productArray.push(v)

				})

				//console.log(productArray)

			  	dispatch( { type:"PRODUCTS", products:result, headers, loadmore } )

				return;
		
			});

	}
}


export function getCategories() {

	return (dispatch) => {

		let categories = []

		let _params = { per_page: 100, hide_empty:false, order: 'desc', orderby: 'count', exclude:globalVar.categorySidebarExclude }
			_params = '?' + qs.stringify(_params)

		WooCommerce.get('products/categories' + _params, function(err, data, result) {
			
			result = JSON.parse(result)

			if(!_.isEmpty(result)) {

				_.map(result, (v,i)=>{
					categories[v.parent] = Object.assign( {[i]:v}, categories[v.parent] )
				})

			}

			return dispatch( { type:"PRODUCT-CATEGORIES", categories } )
			
		});

	}
	
}



/*
 * CREATE ORDER
 */
export function createOrder( _params ) {
	
	return (dispatch) => {
        
        axios( {
            url: __WcAPI__.url + '/wp-json/wcapp/v1/order/',
            method: 'post',
            data: _params
        }).then( (response) => {
			
            const output =  response.data
            
            //console.log(output);
            
            return dispatch( { type:"GET-ORDER-RESPONSE", response:output } )
            
		}).catch( (error) =>{

			console.log(error);
		
		});
        
	}
}


export function getPaymentGateways() {
	
	return (dispatch) => {
        
        WooCommerce.get('payment_gateways', function(err, data, result) {
            
            result = JSON.parse(result)
            
            dispatch( { type:"GET-PAYMENT_GATEWAYS", gateways:result } )
        
        });
        
	}
}