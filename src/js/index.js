import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import reducers from './reducer/shopapp/index';
import ShopApp from "./components/ShopApp";

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

ReactDOM.render(
  	<Provider store={createStoreWithMiddleware(reducers)}>
    	<ShopApp />
  	</Provider>,
  	document.getElementById('display-content')
);
