import axios from "axios"

/*
 * @body statistics
 */
export function getIndexInfo(params) {

	params = (params) ? params : { json: 1, option:'index' };

	return (dispatch) => {

		axios.get('/',{ params }).then((response) => {
			
			//const { products:list, count_products:count, limit, offset, status, sort, order_by, images } = response.data;
			//const payload = { list, count, limit, offset, status, sort, order_by, images };
		
			const { current_user:currentUser } = response.data;
			const payload = { currentUser };	  

				  dispatch( { type:"FETCH_INDEX_FULLFILLED", payload } );
		
		}).catch((error) => {
			dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * Get the list of static data
 * @param string name
 */
export function getOptionsx() {

	let payload = {};
		payload.getGoals = ["Small","Medium","Large","XL","XXL","XXXL"];
		payload.getPrograms = ["Small","Medium","Large","XL","XXL","XXXL"];

	/*switch(name) {
		case "sizes": {
			payload.sizes = ["Small","Medium","Large","XL","XXL","XXXL"]
			break;
		}
		case "colors": {
			payload.colors = ["Yellow","Red","Blue","Orange","White","Black"]
			break;
		}
		case "categories": {
			payload.categories = ["Men","Women","Kids"]
			break;
		}
		case "tags": {
			payload.tags = ["T-shirts","Pants","Underwear","Accesories"]
			break;
		}
		default: {
			payload = []
      		break;
		}
      		
	}*/

	return {
		type : "FETCH_INDEX_FULLFILLED",
		payload
	}
}