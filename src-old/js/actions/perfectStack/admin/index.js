import axios from "axios"


/*
 * @getStack
 */
export function getStacks(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {
			
			
			//console.log(response.data.sql);

			const { stacks, total, limit, active, offset } = response.data;

			dispatch( { type:"GET-STACKS", stacks, total, limit, active, offset } );
			

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}


/*
 * @getStack
 */
export function getStack(_params) {

	return (dispatch) => {


		axios.post(ajaxurl, _params ).then((response) => {
			
			
			const { stack, products } = response.data;
			
			/*let { gender, 
	              meals_per_day:mealsPerDay, 
	              age:ageGroup, 
	              goal, 
	              weight_training:weightTraining, 
	              cardio, 
	              structured_meal_plan:structuredMealPlan, 
	              meal_consistency:mealConsistency, 
	              meal_plan:mealPlan } = stack;			

			let stackFinder = {
				gender, ageGroup, goal, weightTraining, cardio, structuredMealPlan, mealConsistency, mealPlan
			};*/

				//console.log(stackFinder);

			dispatch( { type:"GET-STACK", stack } );
			dispatch( { type:"UPDATE-STACK", products } );
			dispatch( { type:"GET-STACK-FINDER", stack } );
			
		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * @saveStack
 */
export function saveStack(_params, _notificationSystem) {

	return (dispatch) => {


		axios.post(ajaxurl, _params ).then((response) => {
			
			//console.log(response.data);

			const { stack } = response.data;

			dispatch( { type:"GET-STACK", stack } );
			dispatch( { type:"GET-STACK-FINDER", stack } );

			_notificationSystem.addNotification({
		        message: 'Successfully saved!',
		        level: 'success', 
		        position: 'tl'
		    });

			return;

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * @updateStack
 */
export function updateStack(_params, _notificationSystem) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {

				//console.log(response.data);
			
			const { stack, products } = response.data;
		

			dispatch( { type:"UPDATE-STACK", products } );
			dispatch( { type:"GET-STACK", stack } );
			dispatch( { type:"GET-STACK-FINDER", stack } );

			_notificationSystem.addNotification({
		        message: 'Changes has successfully saved!',
		        level: 'success',
		        position: 'tl'
		    });

			return;
			
		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}



/*
 * @duplicateStack
 */
export function duplicateStack(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {
			
			console.log(response.data);

			const { stacks, total, limit, active, offset } = response.data;

			dispatch( { type:"GET-STACKS", stacks, total, limit, active, offset } );


		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * @deleteStack
 */
export function deleteStack(_params, _notificationSystem) {

	

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {
			
			const { stacks, total, limit, active, offset, deleted } = response.data;

			dispatch( { type:"GET-STACKS", stacks, total, limit, active, offset } );

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * @addToStack
 *
export function addToStack(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {

				//console.log(response.data);

			const { stack, products } = response.data;

			dispatch( { type:"ADD-TO-STACK", products } );
			dispatch( { type:"GET-STACK", stack } );

			return;			

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}*/

/*
 * @removeFromStack
 */
export function removeFromStack(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {

				//console.log(response.data);
			const { stack, products } = response.data;
			
			dispatch( { type:"REMOVE-FROM-STACK", products } );
			dispatch( { type:"GET-STACK", stack } );

			return;
			
		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * @getSearchProducts
 */
export function getSearchProducts(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {
			
			//console.log(response);

			//if(response.data) 
				return dispatch( { type:"GET-SEARCH-PRODUCTS", products:response.data } );
			//else
			//	return response.data;

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}


/*
 * @getProductTypes
 */
export function getProductTypes(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {
			
			//console.log(response);

			if(response.data) 
				return dispatch( { type:"GET-PRODUCT-TYPES", types:response.data } );
			else
				return response.data;

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}

/*
 * @getProductBrands
 */
export function getProductBrands(_params) {

	return (dispatch) => {

		axios.post(ajaxurl, _params ).then((response) => {
			
			//console.log(response);

			if(response.data) 
				return dispatch( { type:"GET-PRODUCT-BRANDS", brands:response.data } );
			else
				return response.data;

		}).catch((error) => {
			//dispatch({type:"FETCH_INDEX_REJECTED", payload:error});
		})
	}
}