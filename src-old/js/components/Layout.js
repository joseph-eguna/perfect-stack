import React, { Component } from "react"
import { connect } from "react-redux"
import { fetchProducts,fetchProductsReal } from "../actions/productActions"

class Layout extends Component {
  
    constructor() {
        super();
        this.state = {
          title: "Hello There",
        };

        this.actionClick = this.actionClick.bind(this);
    }

 
    componentWillMount() {
        this.props.dispatch(fetchProducts());
        //this.props.fetchProductsReal();
    }


    actionClick() {
        this.props.fetchProductsReal({another:'name'});
        console.log(this.props);
    }


    render() {
        
        console.log(this.props.products);

        return (
            <div>
                <h1 onClick={this.actionClick} style={ {marginTop:'100px',position:'absolute'} }>HELLO</h1>
            </div>
        );
    }
}


export default connect((store) => {
    return {
        products: store.products.products
    }
})(Layout);

//export default connect(state => {return { ...state.orders }}, actions)(AdminOrders);