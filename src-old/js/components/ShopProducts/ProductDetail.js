import React, { Component } from "react"
import ReactDOM from 'react-dom'
import { connect } from "react-redux"
import axios from "axios"
import serialize from "form-serialize"

import { getProducts, getProductAttributes } from "../../actions/productActions"


var protractor = {
    
    findByClass : function(_element, _by) {

        if(Array.isArray(_by)) {
            for (let i = 0; i < _element.childNodes.length; i++) {
                if (_element.childNodes[i].className == _by[0]) {
                    
                    if(_by.length == 1) 
                        return _element.childNodes[i];
                    else {
                        _by.shift();
                        return this.findByClass(_element.childNodes[i], _by); 
                    }
                    break;
                }      
            }
        }
        return;
    }
}

class ProductDetail extends Component {
    
    constructor() {
        super();

        this.state = {
            products:[]
        };

        this.actionFormSubmit  = this.actionFormSubmit.bind(this);
        this.actionUploadImage = this.actionUploadImage.bind(this);
        this.actionDeleteImage = this.actionDeleteImage.bind(this);
        this.actionSelectImage = this.actionSelectImage.bind(this);
        this.actionCheckbox    = this.actionCheckbox.bind(this);
    }


    /*
     * Initialization of component
     */
    componentDidMount() {
        var _this = this;
    }

    /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {

        ['sizes','colors','categories','tags'].forEach((v,i) => {
            this.props.dispatch(getProductAttributes(v)); 
        });
    }

   /*
    * Extracting props and set it to component state
    */
    componentWillReceiveProps(nextProps) {
        const { products  } = nextProps;
              this.setState({ products });
        //this.setState(nextProps.products);
    }

    /*
     * Image uploader
     * @return action ajax
     */
    imageUploader(url, files, file, i=0) {

        var _this = this;

        if(i < files.length) {

            var data = new FormData();
                data.append( 'file', file );
                data.append( 'filename', file.name.split(' ').join('-').toLowerCase())
                data.append( 'action', 'upload' );

            axios.post( url, data )
                .then( (response) =>  {
                    //console.log(response.data);
                    _this.props.dispatch(getProducts( {
                        limit : this.props.products.limit,
                        offset : (this.props.products.offset) ? this.props.products.offset : 0,
                        json: 1
                    } ));
                    
                    _this.imageUploader(url, files, files[i+1], i+1);
                
                })
                .catch( (error) =>  {
                    return false
                });
        
        }

    }


    /**
     * Uploading product images
     */ 
    actionUploadImage(e) {
        //console.log(e.target);
        var _this = this;
        var url = '/admin/shop/products/upload'; //e.target.getAttribute('action');
        return this.imageUploader(url, e.target.files, e.target.files[0]);
    }

    /**
     * Deleting product images
     */
    actionDeleteImage(e) {

        //console.log(e.target.getAttribute('src'));
        var _this = this;
        var url = '/admin/shop/products/upload';

        axios.post( url, {
            action : 'delete',
            file_url : e.target.getAttribute('data-src')
        } )
            .then( (response) =>  {
                    //console.log(response.data);
                response = response.data;
                if(response.result == 'success') {
                    _this.props.changeState();
                    _this.props.addNotification(
                        {
                            message: response.message,
                            level: 'success'
                        }
                    );
                } 
                //document.getElementById('detail-main-nav').click();
            })
            .catch( (error) =>  {
                //
            });

    }

    /**
     * Selecting primary product image
     */
    actionSelectImage(e) {
        
        const _this = this;
        
        _this.primaryImage.setAttribute('src', e.target.getAttribute('data-site')+"/uploads/app/" + e.target.getAttribute('data-src'));
        _this.primaryImage.setAttribute('data-src', e.target.getAttribute('data-src'));
        _this.navMain.click();
    }


    /**
     * On form submission
     * @return action 
     */
    actionFormSubmit(e) {

        e.preventDefault();
        
        const _this = this;
        const _form = this.formSubmit
        const _url  = (this.state.products.product.detail.ID) ? '/admin/shop/products/update/'+this.state.products.product.detail.ID : '/admin/shop/products/';

        let _data = serialize(_form, { hash: true });

            _data = Object.assign({
                        description:protractor.findByClass(this.productDescription, ['note-editor','note-editable']).innerHTML,
                        primary_image: _this.primaryImage.getAttribute('data-src')
                    }, _data); 


console.log(_data);
return;

        axios.post(_url, _data).then( (response) => {
            
            response = response.data;

            if(response.result == 'success') {
                
                _this.props.addNotification(
                    {
                        message: response.message,
                        level: 'success',
                    }
                );

                _form.reset();
                _form.getElementsByClassName('note-editable')[0].innerHTML = '';
                
                _this.props.dispatch(getProducts( {
                        limit : _this.props.products.limit,
                        offset : _this.props.products.offset,
                        json: 1
                    } ));

                return;
            
            } else {

                response.forEach((v,i) => {
                    console.log(v);
                    _this.props.addNotification(
                        {
                            message: v,
                            level: 'error',
                        }
                    );
                });
                return;

            }
            
        }).catch( (error) => {
            error.forEach((v,i) => {
                console.log(v);
                _this.props.addNotification(
                    {
                        message: v,
                        level: 'error',
                    }
                );
            });
            return;
        });
     
    }

    /*
     * Changes on checkbox data attr
     */
    actionCheckbox(e) {
        
        let products = this.state.products;
        
        const { value, checked } = e.target;
        const attr  = e.target.getAttribute('data-attr');
        
        products.product = (products.product) ? products.product : {}; 
        products.product[attr] = (products.product[attr]) ? products.product[attr] : [];
       
        if(checked) {
            products.product[attr] = products.product[attr].concat([e.target.value]); 
        } else {

            const index = products.product[attr].indexOf(value)
            if( index != -1 ) {
                 products.product[attr].splice(index, 1);
            }
        }
        this.setState({ products });
    }

    actionRenderEditData(_detail) {
        
        if(!_detail)
            return;

        this.title.innerHTML = (_detail.name.length > 0) ? "EDIT "+_detail.name : "ADD NEW";
        this.sku.value       = _detail.sku;
        this.price.value     = _detail.price;
        this.name.value      = _detail.name;
        protractor.findByClass(this.productDescription, ['note-editor','note-editable']).innerHTML = _detail.description;
    }


    /*
     * Rendering product detail
     */
    render() {

        const { detail,categories,tags,sizes,colors } = (this.state.products.product) ? this.state.products.product : {detail:false,categories:false,tags:false,colors:false,sizes:false};
        const product = { detail,categories,tags,sizes,colors };
        
        this.actionRenderEditData(detail);

        const images = (()=>{
            if(this.state.products.images) {
                return this.state.products.images.map( (image, i) => {
                    return  <div className="col-md-3 box-image">
                                <img src={ this.props.main.siteURL+"/uploads/app/"+ image } />
                                <i className="pg-trash" onClick={this.actionDeleteImage} data-src={image}></i>
                                <span onClick={this.actionSelectImage} className="btn btn-info select-image" data-site={this.props.main.siteURL} data-src={ image }>Select Image</span>
                            </div>
                });
            }
        })();

        const _const = {};
        ['sizes','colors','categories', 'tags'].forEach((v,i)=>{
            _const[v] = (()=>{

                if(this.state.products[v]){
                    return this.state.products[v].map((vv,ii)=>{
                        let checked = (product[v] && product[v].indexOf(vv) != -1) ? "checked" : "";
                        return  <div className="checkbox" style={ {display:"inline-block"} }>
                                    <input type="checkbox" onClick={this.actionCheckbox} data-attr={v} name={v+"[]"} value={vv} id={v+"-"+ii} checked={checked} />
                                    <label for={v+"-"+ii}>{vv}</label>
                                </div>
                        })
                    }
                })();

        });

        return ( 


    <div className="modal fade slide-right in" id="modal-slide-up" tabindex="-1" role="dialog" aria-hidden="false">
        <div className="modal-dialog modal-lg">
            <div className="modal-content-wrapper">
                <div className="modal-content">
                    <div className="modal-header clearfix text-left">
                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><i className="pg-close fs-14"></i> </button>
                        <h5 ref={(input) => this.title = input }> ADD PRODUCT </h5>
                        <div className="response"></div>
                    </div>
                    <div className="modal-body" style={ {minHeight:'100vh'} }>

        
        <form id="product-action" name="product-action" ref={(input)=>{ this.formSubmit = input}} onSubmit={this.actionFormSubmit} method="POST" action="/admin/shop/products">                                                                                                                                                                                                                                                                                             

            <div className="response"></div>

            <div className="panel panel-transparent tabs">
                <ul className="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                    <li className="active">
                        <a data-toggle="tab" href="#add-product-content" ref={(input) => this.navMain = input} id="detail-main-nav"><span>Main</span></a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#add-product-image" ref={(input) => this.navSelectImage = input} id="detail-select-nav"><span>Select Image</span></a>
                    </li>
                    
                </ul>
                <div className="tab-content">

                    <div className="tab-pane active" id="add-product-content">
                        <div className="form-group">
                            <div className="col-md-6">
                                <img id="primary-image" ref={(input)=>{ this.primaryImage = input}} data-src="/placeholder-image" src="https://s3.amazonaws.com/weighttraining.com/data/512/content.jpg?1318811220" style={ {width:'100%'} } />
                            </div>
                            <div className="col-md-6">
                                <label>SKU:</label>
                                <input type="text" placeholder="Enter SKU:" name="sku" className="form-control" ref={(input)=>{this.sku = input}}/>
                                <br />
                                <label>Price:</label>
                                <input type="text" placeholder="Enter Price:" name="price" className="form-control" ref={(input)=>{this.price = input}} />
                                <br />
                                <label>Sizes:</label><br />
                                
                                { _const.sizes }
                                
                                <br /><br />
                                <label>Colors:</label><br />

                                { _const.colors }

                            </div>
                            <br />
                            <label>Name:</label>
                            <input type="text" placeholder="Enter Name:" name="name" className="form-control" ref={(input)=>{this.name = input}}/>
                        </div>
                        <div className="form-group">
                            <label>Enter Full Description:</label>
                            <div className="summernote-wrapper" id="description-wrapper" ref={(input)=>{this.productDescription = input}}>
                                <div id="description"></div>
                            </div>
                        </div>
                        <div className="form-group row" style={ {marginTop:'20px'} }>
                            <div className="col-md-6">
                                <label>Categories:</label><br />
                                { _const.categories }
                            </div>
                             <div className="col-md-6">
                                <label>Tags:</label><br />
                                { _const.tags }
                            </div>
                         </div>
                         <div className="col-md-12 row">
                            <div className="col-sm-8">

                            </div>
                            <div className="col-md-4 m-t-10 sm-m-t-10">
                                <button type="submit" className="btn btn-primary btn-block m-t-5">SAVE</button>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                    <div className="tab-pane" id="add-product-image">
                        <div className="form-group text-right">
                            <input type="file" onChange={this.actionUploadImage} id="file-upload" action="/admin/shop/products/upload" data-url="/admin/ingredients/upload" name="photo" className="form-control" multiple/>
                        </div>
                        <div className="form-group">
                            { images }
                        </div>
                    </div>

                </div>
            </div>
        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

         );  
    }
}

export default connect((store) => {
    return {
        main: store.main,
        products: store.products.products
    }
})(ProductDetail);

/*
const sizes = (()=>{
if(this.props.products.sizes){
    return this.props.products.sizes.map((v,i)=>{
        return  <div className="checkbox" style={ {display:"inline-block"} }>
                    <input type="checkbox" name="sizes[]" value={v} id={"size-"+i}/>
                    <label for={"size-"+i}>{v}</label>
                </div>
        })
    }
})();
*/