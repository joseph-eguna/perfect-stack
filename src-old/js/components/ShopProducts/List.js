import React, { Component } from "react"
import { connect } from "react-redux"
import axios from "axios"
import CSSTransitionGroup  from 'react-transition-group/CSSTransitionGroup'
import Pagination from "react-js-pagination";

import { getProducts, getProduct } from "../../actions/productActions"

class List extends Component {
  
    constructor(props) {
        super(props);
        this.state = { 
            list : [],
            count : 0,
            limit : 0,
            status : 'published',
            activePage: 1,
        };

        this.actionPaginate = this.actionPaginate.bind(this);
        this.actionFilter = this.actionFilter.bind(this);
        this.actionEdit = this.actionEdit.bind(this);
    }


    componentWillReceiveProps(nextProps) {
        this.setState(nextProps.products);
    }


    actionPaginate(e) {

        const _params = {
                offset : parseInt(e-1)*this.state.limit,
                limit : this.state.limit,
                status : this.state.status,
                json: 1
            }

        this.setState({activePage:e});
        this.props.dispatch(getProducts(_params));
    }

    actionFilter(e) {

        e.preventDefault();

        const _status = (e.target.getAttribute('data-status')) ? e.target.getAttribute('data-status') : 'published';

        const _params = {
                limit : this.state.limit,
                status : _status,
                json: 1
            }

        this.setState({ status:_status});
        this.props.dispatch(getProducts(_params));

    }

    actionEdit(e) {

        const _this = this;
        const _id = e.target.getAttribute('data-id');

        _this.props.dispatch(getProduct(_id));
    }

    /*
     * RENDERING
     */
    render() {

        return ( 

<div>
    <div className="row" style={ {marginTop:'20px'} }>
        <div className="col-md-9" style={ {marginBottom:'20px;'} }> 
            <div className="btn-group" role="group">
                <button type="button" onClick={this.actionFilter} data-status="any" className="btn btn-default">All</button>
                <button type="button" onClick={this.actionFilter} data-status="published" className="btn btn-default">Published</button>
                <button type="button" onClick={this.actionFilter} data-status="pending" className="btn btn-default">Pending</button>
                <button type="button" onClick={this.actionFilter} data-status="trashed" className="btn btn-default">Trashed</button>
            </div>
        </div>
        <div className="col-md-3 pull-right">
             <nav style={ {textAlign:'right'} }>
                
                {(()=>{
                    if(this.state.count > this.state.limit) {
                        return <Pagination
                                  activePage={this.state.activePage}
                                  itemsCountPerPage={this.state.limit}
                                  totalItemsCount={this.state.count}
                                  pageRangeDisplayed={3}
                                  onChange={this.actionPaginate} />
                    }
                })()}

            </nav>
        </div>
    </div>

    <div className="row products">
        <div className="col-md-12">
            <div className="panel panel-default">


        <div className="panel-body" style={ { padding:0, height:'auto' } }>
            <div className="table-responsive">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th style={ {width:'1%'} }>
                            </th>
                            <th style={ {width:'15%'} }></th>
                            <th style={ {width:'15%'} }>Name</th>
                            <th style={ {width:'25%'} }>Description</th>
                            <th>SKU</th>
                            <th>Price</th>
                            <th>Categories</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <CSSTransitionGroup
                        component="tbody"
                        transitionName="fade"
                        transitionEnterTimeout={500}
                        transitionLeaveTimeout={500}>
                        
                        {(()=>{
                            if(this.state.list.length > 0) {
                                return this.state.list.map( (product, i) => {
                                    return <tr key={product.ID}>
                                                <td className="v-align-middle">
                                                    <div className="checkbox" style={ {display:"inline-block"} }>
                                                        <input type="checkbox" name="list[]" value={product.ID} id={"list-"+product.ID}/>
                                                        <label for={"list-"+product.ID}></label>
                                                    </div>
                                                </td>
                                                <td className="v-align-middle text-center"> 
                                                    <img src="https://s3.amazonaws.com/weighttraining.com/data/512/content.jpg?1318811220" style={ {maxWidth:'100%'} } /> 
                                                    <div className="btn-group">
                                                        <button type="button" className="btn btn-default" onClick={this.props.deleteProduct} data-id={product.ID} data-name={product.name} data-action={"softdelete"}>
                                                            <i className="pg-trash text-danger" data-id={product.ID} data-name={product.name} data-action={"softdelete"}></i>
                                                        </button>
                                                        <button type="button" className="btn btn-default modal-popup" onClick={this.actionEdit} data-id={35} data-target="#modal-slide-up">
                                                            <i className="pg-settings"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                                <td className="v-align-middle">
                                                    <p>{ product.name }</p>
                                                </td>
                                                <td className="v-align-middle">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque </p>
                                                </td>
                                                <td className="v-align-middle">
                                                    <p>{product.sku}</p>
                                                </td>
                                                <td className="v-align-middle">
                                                    <p>{product.price}</p>
                                                </td>
                                                <td className="v-align-middle">
                                                    <p>Protein, Creatine, </p>
                                                </td>
                                                <td className="v-align-middle">
                                                    <p>{product.status}</p>
                                                </td>
                                            </tr>           

                                })
                            } else {
                                return <tr>
                                            <td colSpan={8} className="text-center">
                                                <h4>
                                                    { "{ Nothing Found! }" }
                                                </h4>
                                            </td>
                                       </tr> 
                            }
                        })()}

                    </CSSTransitionGroup>

                </table>
            </div>
        </div>

        

        <div className="panel-footer">
            <nav style={ {textAlign:'right'} }>
                
                {(()=>{
                    if(this.state.count > this.state.limit) {
                        return <Pagination
                                  activePage={this.state.activePage}
                                  itemsCountPerPage={this.state.limit}
                                  totalItemsCount={this.state.count}
                                  pageRangeDisplayed={5}
                                  onChange={this.actionPaginate} />
                    }
                })()}

            </nav>
        </div>
        
            </div>
        </div>
    </div>
</div>
         );  
    }
}


export default connect((store) => {
    return {
        products: store.products.products
    }
})(List);

/**/