import React from "react";
import axios from "axios"


export default class Pagination extends React.Component {
  
    constructor() {
        super();

        this.state = { 
            limit : 0,
            offset : 0,
            count : 0,
        };
        
        this.pagination = this.pagination.bind(this);
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({
            count : nextProps.count_products,
            limit : nextProps.limit_products
        });
    }


    componentDidMount() {
 
        var _this = this;
       
    }

    pagination() {

        let pagination = '';
        let i = 0;
        for(i=0;i<=ceil(this.state.count/this.state.limit);i++) {
            pagination +=  <li>
                                <a href="#">{i}<span className="sr-only"></span></a> 
                           </li>
        }

        return pagination;
    }

    render() {
        
        
        return ( 

<div className="panel-footer">
    <nav style={ {textAlign:'right'} }>
        <ul className="pagination">

            <li className="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a> </li>
            <li className="active"><a href="#">1 <span className="sr-only">(current)</span></a> </li>
            <li><a href="#">2 <span className="sr-only"></span></a> </li>
            <li><a href="#">3<span className="sr-only"></span></a> </li>
            <li><a href="#">4<span className="sr-only"></span></a> </li>
            <li><a href="#">5<span className="sr-only"></span></a> </li>
            <li><a href="#">6<span className="sr-only"></span></a> </li>
            <li className="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a> </li>

        </ul>
    </nav>
</div>

         );  
    }
}


/*
<li className="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a> </li>
            <li className="active"><a href="#">1 <span className="sr-only">(current)</span></a> </li>
            <li><a href="#">2 <span className="sr-only"></span></a> </li>
            <li><a href="#">3<span className="sr-only"></span></a> </li>
            <li><a href="#">4<span className="sr-only"></span></a> </li>
            <li><a href="#">5<span className="sr-only"></span></a> </li>
            <li><a href="#">6<span className="sr-only"></span></a> </li>
            <li className="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a> </li>
*/