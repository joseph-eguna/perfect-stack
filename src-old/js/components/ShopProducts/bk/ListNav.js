import React from "react";
import axios from "axios"
import Pagination from "react-js-pagination";

export default class ListNav extends React.Component {
  
    constructor(props) {
        super(props);
        this.state = {};
    }
   
    componentWillReceiveProps(nextProps) {
        this.setState( nextProps.state );
    }


    render() {

    //console.log(this.state);

        return ( 


<div>
    <div className="row" style={ {marginTop:'20px'} }>
        <div className="col-md-9"> 
            <a href="#" className="btn btn-primary">ALL</a> <a href="#" className="btn btn-primary">Published</a>  <a href="#" className="btn btn-primary">Pending</a>  <a href="#" className="btn btn-primary">Trashed</a> 
        </div>
        <div className="col-md-3 pull-right">
             <nav style={ {textAlign:'right'} }>
                
                {(()=>{
                    if(this.state.countProducts > this.state.limitProducts) {
                        return <Pagination
                                  activePage={this.state.activePage}
                                  itemsCountPerPage={this.state.limitProducts}
                                  totalItemsCount={this.state.countProducts}
                                  pageRangeDisplayed={3}
                                  onChange={this.props.actionPaginate} />
                    }
                })()}

            </nav>
        </div>
    </div>
</div>

         );  
    }
}

/*
<nav style={ {textAlign:'right'} }>
                
                {(()=>{
                    if(this.state.countProducts > this.state.limitProducts) {
                        return <Pagination
                                  activePage={this.state.activePage}
                                  itemsCountPerPage={this.state.limitProducts}
                                  totalItemsCount={this.state.countProducts}
                                  pageRangeDisplayed={5}
                                  onChange={this.actionPaginate} />
                    }
                })()}

            </nav>
            */