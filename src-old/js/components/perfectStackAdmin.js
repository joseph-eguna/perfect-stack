import React, { Component } from "react"
import { Table, Row, Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"
import _ from "lodash"
import qs from "qs"
import NotificationSystem from 'react-notification-system'
import CSSTransitionGroup  from 'react-transition-group/CSSTransitionGroup'
import Pagination from "react-js-pagination";
import serialize from "form-serialize"


import { getStacks, getStack, saveStack, duplicateStack, updateStack, deleteStack, addToStack, removeFromStack, getSearchProducts, getProductTypes, getProductBrands } from "../actions/perfectStack/admin/index"


class perfectStackAdmin extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
            create : { action : false },
            actionStackDetail:false,
            search: {
                brand:false,
                type:false
            },
            fields:{
                stackName:'',
                stackDescription:'',
                product_id:0
            },

            stack:{},
            stackLists:{},
            stackListsPaginate:{ limit:10 }                     
        };
        
        /*
        stackListsPaginate: {
                active:1,
                offset:1,
                limit:10,
                total:100
            }
        */
        this.actionCreateStack = this.actionCreateStack.bind(this);
        this.actionSaveStack = this.actionSaveStack.bind(this);
        this.actionAddToStack = this.actionAddToStack.bind(this);
        this.actionRemoveFromStack = this.actionRemoveFromStack.bind(this);
        this.actionEditStack = this.actionEditStack.bind(this);
        this.actionDuplicateStack = this.actionDuplicateStack.bind(this);
        this.actionDeleteStack = this.actionDeleteStack.bind(this);

        this.actionChangeText = this.actionChangeText.bind(this);
        this.actionCheckBox = this.actionCheckBox.bind(this);

        this.actionSelectBrand = this.actionSelectBrand.bind(this);
        this.actionSelectType = this.actionSelectType.bind(this);
        this.actionInsertKeyword = this.actionInsertKeyword.bind(this);

        this.paginateStacks = this.paginateStacks.bind(this);
        this.viewStackingList = this.viewStackingList.bind(this);
    }

    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    /*addNotification(data){
        this.notificationSystem.addNotification(data);
    }*/

    /*
     * Dispatch initially the list of props from redux
     */
    componentDidMount() {

        this._notificationSystem = this.refs.notificationSystem;

        let _params = {
            action:'ps_ajax',
            job: 'search-products'            
        }
        this.props.dispatch(getSearchProducts(qs.stringify(_params)));

        _params.job = 'get-product-types';
        this.props.dispatch(getProductTypes(qs.stringify(_params)));

        _params.job = 'get-product-brands';
        this.props.dispatch(getProductBrands(qs.stringify(_params)));


        /*let _params = {
            action:'ps_ajax',
            job: 'get-stacks',
            limit:this.state.stackListsPaginate.limit,
            offset:parseInt(e-1)
        }*/

        _params.job = 'get-stacks';
        _params.limit = parseInt(this.state.stackListsPaginate.limit);

        this.props.dispatch(getStacks(qs.stringify(_params)));

    }

    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        this.setState(nextProps.index);

        if(this.state.stack) {
            const { name:stackName, description:stackDescription } = this.state.stack;
            const fields = {...this.state.fields, stackName, stackDescription};
            this.setState( {fields} );
        }
       
    }

    
    actionCreateStack(data) {

        let state = {...this.state, actionStackDetail:true, stack:[], fields:{...this.state.fields, stackName:'',stackDescription:''} }

        this.setState( state );

    }


    actionSaveStack(e) {

        let _params = {
            action:'ps_ajax',
            job: 'save-stack',
            name: this.state.fields.stackName,
            desc: this.state.fields.stackDescription,
            id:this.state.stack.ID
        }

        if(_.isEmpty(this.state.stack)) 
            this.props.dispatch(saveStack(qs.stringify(_params), this._notificationSystem));
        else {
            _params.job = 'update-stack';
            this.props.dispatch(updateStack(qs.stringify(_params), this._notificationSystem)); //updateStack
        }

    }

    actionAddToStack(e) {
        
        let id = e.target.getAttribute('data-id');

        let product_ids = _.sortedUniq(_.compact(_.concat(JSON.parse(this.state.stack.products), id)));

        //console.log(product_ids);

            //let product_ids = {...state.stack.products} 

        let _params = {
            action:'ps_ajax',
            job: 'update-stack',
            id: this.state.stack.ID,
            product_ids
        }
            //his.state.stack.products
       
        this.props.dispatch(updateStack(qs.stringify(_params)));

    }

    actionRemoveFromStack(e) {
        
        let id = e.target.getAttribute('data-id');

        let product_ids = _.pull(_.compact(JSON.parse(this.state.stack.products)), id);
            product_ids = (!_.isEmpty(product_ids)) ? product_ids : '';

        let _params = {
            action:'ps_ajax',
            job: 'remove-from-stack',
            id: this.state.stack.ID,
            product_ids
        }

        this.props.dispatch(removeFromStack(qs.stringify(_params)));

        //console.log(this.state.productStackLists);

    }

    actionEditStack(e) {

        let id = e.target.getAttribute('data-id');

        let _params = {
            action:'ps_ajax',
            job: 'get-stack',
            id             
        }

        this.setState( {...this.state, actionStackDetail:true} );

        this.props.dispatch(getStack(qs.stringify(_params)));

        //this.props.dispatch(removeFromStack(qs.stringify(_params)));
        //console.log(this.state.productStackLists);

    }

    actionDuplicateStack(e) {
        
        let id = e.target.getAttribute('data-id');

        let { limit, active:offset } = this.state.stackListsPaginate;

        let _params = {
            action:'ps_ajax',
            job: 'duplicate-stack',
            limit, 
            offset,
            id             
        }

        this.props.dispatch(duplicateStack(qs.stringify(_params)));

    }

    actionDeleteStack(e) {
        
        let id = e.target.getAttribute('data-id');

        let name = e.target.getAttribute('data-name');

        let { limit, active:offset } = this.state.stackListsPaginate;

        let _params = {
            action:'ps_ajax',
            job: 'delete-stack',
            limit,
            offset,
            id,
            name             
        }
        
        this.props.dispatch(deleteStack(qs.stringify(_params), this._notificationSystem));

    }


    actionSelectBrand(e) {

            //console.log(e.target.value);
        let brand = e.target.value  
        let _params = {
            action:'ps_ajax',
            job: 'search-products',
            brand
        }

        let state = {...state, search:{brand, type:this.state.search.type}};
        this.setState( state );

        _params = (!this.state.search.type) ? _params : {..._params, type:this.state.search.type};
        this.props.dispatch(getSearchProducts(qs.stringify(_params)));

    }

    actionSelectType(e) {

        let type = e.target.value
        let _params = {
            action:'ps_ajax',
            job: 'search-products',
            type
        }

        let state = {...state, search:{type,brand:this.state.search.brand}};
        this.setState( state );

        _params = (!this.state.search.brand) ? _params : {..._params, brand:this.state.search.brand};
        this.props.dispatch(getSearchProducts(qs.stringify(_params)));

    }

    actionInsertKeyword(e) {

        let keyword = e.target.value
        let _params = {
            action:'ps_ajax',
            job: 'search-products',
            keyword
        }

        this.props.dispatch(getSearchProducts(qs.stringify(_params)));

    }

    actionChangeText(e) {
        
        const target = e.target.getAttribute('name');
        
        let value = e.target.value;

        let fields = {...this.state.fields, [target]:value};

        this.setState( {fields} );

    }

    actionCheckBox(e) {

        const field = e.target.getAttribute('data-field'); //gender
        const target = e.target.getAttribute('name'); //male

        let value = {...this.state.stackFinder[[field]], [[target]]:!this.state.stackFinder[[field]][[target]] };

        let stackFinder = {...this.state.stackFinder, [field]:value };
        
        

        const { gender, 
                ageGroup:age, 
                goal, 
                weightTraining:weight_training, 
                cardio, 
                structuredMealPlan:structured_meal_plan, 
                mealConsistency:meal_consistency, 
                mealPlan:meal_plan } = stackFinder; 
         
        let _params = {
            action:'ps_ajax',
            job: 'update-stack',
            id: this.state.stack.ID,
            stack_finder: {gender, age,goal,weight_training,cardio,structured_meal_plan,meal_consistency, meal_plan}
        }   

        //this.state.stack.ID

        //console.log(_params);

        //console.log(this.state.stackFinder);
         
       
        this.props.dispatch(updateStack(qs.stringify(_params)));

        //this.setState( {stackFinder} );

        //console.log(this.state.stackFinder.mealsPerday['1-2']);
        //updateStackFinder
        //console.log(fields);
        //this.setState
    }

    paginateStacks(e) {

        let { limit } =  this.state.stackListsPaginate;

        let _params = {
            action:'ps_ajax',
            job: 'get-stacks',
            limit,
            offset:parseInt(e)
        }
        
        console.log(_params);
        //console.log(parseInt(e-1));
        this.props.dispatch(getStacks(qs.stringify(_params)));

    }

    viewStackingList() {
        
        let { limit, active:offset } = this.state.stackListsPaginate;

        let _params = {
            action:'ps_ajax',
            job: 'get-stacks',
            limit, 
            offset            
        }
        
        this.props.dispatch(getStacks(qs.stringify(_params)));

        this.setState( {...this.state, actionStackDetail:false} );
    
    }

    render() {
        
        console.log(this.state.stackLists);

        const stackNames = {
            gender:'Gender', male:'Male', female:'Female',
            ageGroup:'Age Group', 'age-20-and-under':'Age 20 & under', 'age-20-to-30':'Age 20 to 30', 'age-30-to-40':'Age 30 to 40', 'age-40-to-50':'Age 40 to 50', 'age-50-to-60':'Age 50 to 60', 'age-60-plus':'Age 60+',
            goal:'Goal', 'goal-sports-performance':'Sport Performance', 'goal-primary-fat-loss':'Primary Fat Loss', 'goal-lean-muscle-development':'Muscle Lean Development', 'goal-increase-in-averall-muscle-and-mass':'Increase in Overall Muscle Mass', 'goal-general-health-fitness':'General Health Fitness', 'goal-body-transformation':'Body Transformation',
            weightTraining:'Weight Training','weight_training-50-less':'Less than 50%','weight_training-50-greater':'Greater than 50%',
            cardio:'Cardio', undefined:'None', 'cardio-steady-state':'Steady State','cardio-hiit':'HIIT','cardio-combination-of-steady-state-and-hiit':'Combination of Steady State & HIIT',
            structuredMealPlan:'Structured Meal Plan','structured_meal-plan':'Yes', 
            mealConsistency:'Meal Consistency', 'meal_consistency-50-less':'Less than 50%', 'meal_consistency-50-greater':'Greater than 50%',
            mealPlan:'Meal Plan', 'meal-1to2':'1-2', 'meal-2to3':'2-3', 'meal-3to4':'3-4', 'meal-4to5':'4-5', 'meal-5to6':'5-6', 'meal-6-plus':'6+',
        }

        return ( 

<div>

    <h1 style={ {marginTop:'40px'} } >Perfect Stack</h1>
        
    {(()=>{


        if(this.state.actionStackDetail) {
            return <Form style={ {marginTop:"20px"} }>

                <Col sm="12">
                    <Button color="primary" size="lg" onClick={this.viewStackingList}>View Stacking Lists</Button>
                </Col>
                <Col sm="4">
                    <FormGroup>
                      <h3>Enter Name</h3>
                      <Input type="text" name="stackName" value={this.state.fields.stackName} placeholder="Ex. Perfect Stack 1" onChange={this.actionChangeText} ref={(input)=>{ this.stackName = input}}/>
                    </FormGroup>
                    <FormGroup>
                      <h3>Description</h3>
                      <Input type="textarea" name="stackDescription" value={this.state.fields.stackDescription}  placeholder="Ex. Perfect Stack 1" onChange={this.actionChangeText} ref={(input)=>{ this.stackDescription = input}}/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" onClick={this.actionSaveStack}>Save</Button>
                    </FormGroup>

                    {(()=>{
                        if(!_.isEmpty(this.state.stack)) {

                            return <div>

                                <h3>Stack Finder Filter</h3>


                                {(()=>{
                                    if(!_.isEmpty(this.state.stackFinder)) {
                                        return Object.keys(this.state.stackFinder).map((v,i)=>{
                                            return  <FormGroup>
                                                        <Label for={v}>{stackNames[v]}</Label> <br />

                                                        {(()=>{
                                                            
                                                            return Object.keys(this.state.stackFinder[[v]]).map((vv,ii)=>{
                                                                
                                                                return <label style={{fontWeight:'normal'}}> 
                                                                            <Input onChange={this.actionCheckBox} type="checkbox" checked={this.state.stackFinder[[v]][[vv]]}  data-field={v} name={vv} style={ {margin:"3px 5px", position:'relative'} } /> 
                                                                            {stackNames[vv]}
                                                                        </label>

                                                                    
                                                            })   

                                                        })()} 
                                                    </FormGroup>
                                            
                                        });
                                    }
                                })()}

                            </div>

                        } 
                    })()}

                            
                    
                </Col>

                <Col sm="8">
                    

                    {(()=>{
                        if(!_.isEmpty(this.state.stack)){

                            return <div>

                                <h3>Product Lists</h3>
                                <Table>
                                    <thead>
                                      <tr>
                                        <th>Thumb</th>
                                        <th>Name</th>
                                        <th>Brand</th>
                                        <th>Type</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                        {(()=>{
                                            if(!_.isEmpty(this.state.productStackLists)) {

                                                return this.state.productStackLists.map((v, i)=>{
                                                    return <tr>
                                                                <td>
                                                                    {(()=>{
                                                                        if(v.image) {
                                                                            return <img style={ {width:"40px"} } src={v.image} />
                                                                        }
                                                                    })()}
                                                                </td>
                                                                <td>{v.title}</td>
                                                                <td>{v.brand}</td>
                                                                <td>{v.type}</td>
                                                                <td>
                                                                    <Button color="danger" size="sm" onClick={this.actionRemoveFromStack} data-id={v.id}>Remove from stack</Button>
                                                                </td>
                                                            </tr>
                                                })
                                            } else {
                                                return <tr>
                                                        <td colSpan="5">
                                                            <h3 style={{textAlign:'center'}}>{" {Empty!} "}</h3>
                                                        </td>
                                                    </tr>
                                            }

                                        })()}

                                       
                                    </tbody>
                                </Table>

                                <h3>Search Product</h3>
                                
                                <Col sm="4">
                                    <FormGroup>
                                        <label>Brand:</label>
                                        <Input type="select" name="brand" onClick={this.actionSelectBrand} ref={(input)=>{ this.brand = input}}>
                                            <option value="0">Any</option>
                                            {(()=>{
                                                return _.map(this.state.productBrands).map((v,i)=>{
                                                    return <option value={v.term_id}>{v.name}</option>
                                                })
                                            })()}
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <label>Type:</label>
                                        <Input type="select" name="type" onClick={this.actionSelectType} ref={(input)=>{ this.type = input}}>
                                            <option value="0">Any</option>
                                            {(()=>{
                                                return _.map(this.state.productTypes).map((v,i)=>{
                                                    return <option value={v.term_id}>{v.name}</option>
                                                })
                                            })()}
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <label>Keyword:</label>
                                        <Input type="text" name="keyword" onKeyUp={this.actionInsertKeyword} placeholder="Enter Keyword"/>
                                    </FormGroup>
                                </Col>

                                <Table>
                                    <thead>
                                      <tr>
                                        <th>Thumb</th>
                                        <th>Name</th>
                                        <th>Brand</th>
                                        <th>Type</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                        {(()=>{
                                            if(!_.isEmpty(this.state.productSearchLists)) {

                                                return this.state.productSearchLists.map((v, i)=>{
                                                    return <tr>
                                                                <td>
                                                                    {(()=>{
                                                                        if(v.image) {
                                                                            return <img style={ {width:"40px"} } src={v.image} />
                                                                        }
                                                                    })()}
                                                                    
                                                                </td>
                                                                <td>{v.title}</td>
                                                                <td>{v.brand}</td>
                                                                <td>{v.type}</td>
                                                                <td>
                                                                    <Button color="primary" size="sm" onClick={this.actionAddToStack} data-id={v.id}>Add to Stack</Button>
                                                                </td>
                                                            </tr>
                                                })
                                            } else {
                                                return <tr>
                                                        <td colSpan="5">
                                                            <h3 style={{textAlign:'center'}}>{" {Empty!} "}</h3>
                                                        </td>
                                                    </tr>
                                            }

                                        })()}

                                       

                                    </tbody>
                                </Table>
                                
                            </div>

                        } 
                    })()}

                    
                </Col>
            </Form>      
        } else {
            return <Col sm="12">
                
                <Button color="primary" size="lg" onClick={this.actionCreateStack}>Create Stack</Button>

                <h2 style={ {marginTop:'40px'} } >Stacking Lists</h2>

                <Table>
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                        

                            {(()=>{
                                if(!_.isEmpty(this.state.stackLists)) {

                                    return this.state.stackLists.map((v, i)=>{
                                        return <tr key={v.ID}>
                                                    <td>
                                                        <Button color="danger" onClick={this.actionDeleteStack} size="sm" data-name={v.name} data-id={v.ID}>
                                                            DELETE
                                                        </Button> {v.name}</td>
                                                    <td width="50%">{v.description}</td>
                                                    <td>
                                                        <Button color="info" size="sm" onClick={this.actionEditStack} data-id={v.ID}>
                                                            EDIT
                                                        </Button> &nbsp;
                                                        <Button color="success" size="sm" onClick={this.actionDuplicateStack} data-id={v.ID}>
                                                            DUPLICATE
                                                        </Button> 
                                                    </td>
                                                </tr>
                                    })

                                } else {
                                    return <tr>
                                            <td colSpan="3">
                                                <h3 style={{textAlign:'center'}}>{" {Empty!} "}</h3>
                                            </td>
                                        </tr>
                                }

                            })()}

                    </tbody>
                </Table>

                <nav style={ {textAlign:'right'} }>
                    
                    {(()=>{
                        if(this.state.stackListsPaginate) {

                            return <Pagination
                                      activePage={this.state.stackListsPaginate.active}
                                      itemsCountPerPage={this.state.stackListsPaginate.limit}
                                      totalItemsCount={this.state.stackListsPaginate.total}
                                      pageRangeDisplayed={3}
                                      onChange={this.paginateStacks} />
                        }
                    })()}

                </nav>
            </Col>
        }

    })()}
    
    <NotificationSystem ref="notificationSystem" />
</div>

         );  
    }
}

export default connect((store) => {
    return {
        index: store.stackAdmin
    }
})(perfectStackAdmin);


/*
<CSSTransitionGroup
    component="tbody"
    transitionName="fade"
    transitionEnterTimeout={500}
    transitionLeaveTimeout={500}>
</CSSTransitionGroup>

{(()=>{
            if(!_.isEmpty(this.state.stackFinder)) {
                return Object.keys(this.state.stackFinder).map((v,i)=>{
                    return  <FormGroup>
                                <Label for={v}>{v}</Label> <br />

                                {(()=>{
                                    
                                    return Object.keys(this.state.stackFinder[[v]]).map((vv,ii)=>{
                                     return <label style={{fontWeight:'normal'}}> 
                                                <Input onChange={this.actionCheckBox} type="checkbox" checked={this.state.stackFinder[[v]][[vv]]}  data-field={v} name={vv} style={ {margin:"3px 5px", position:'relative'} } /> 
                                                {vv}
                                            </label>

                                            
                                    })   

                                })()} 
                            </FormGroup>
                    
                });
            }
        })()}*/
