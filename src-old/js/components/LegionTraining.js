import React, { Component } from "react"
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"
import NotificationSystem from 'react-notification-system';

import { getIndexInfo } from "../actions/indexActions"

import Overview from "./LegionTraining/Overview";
import Workout from "./LegionTraining/Workout";
import Nutrition from "./LegionTraining/Nutrition";

import Sidebar from "./LegionTraining/Sections/Sidebar";

class LegionTraining extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }

    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        this.props.dispatch(getIndexInfo());
    }

    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }



    render() {
        
        //console.log(this.props);

        return ( 

<Router>
    <root>

        <Sidebar />

        <Switch>
            <Route exact={true} path="/" component={Overview}/>
            <Route exact={true} path="/workout" component={Workout} />
            <Route exact={true} path="/nutrition" component={Nutrition} />
        </Switch>
    </root>
</Router>
         );  
    }
}

export default connect((store) => {
    return {
        index: store.index.payload
    }
})(LegionTraining);

/*
<Route path="/" exact={true} component={Overview}/>
<Route path="/:workout" component={Workout}/>
<NotificationSystem ref="notificationSystem" />
*/