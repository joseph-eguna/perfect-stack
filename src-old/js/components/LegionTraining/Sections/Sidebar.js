import React, { Component } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"

class Sidebar extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    render() {
        return ( 

<nav className="page-sidebar" data-pages="sidebar">
    <div className="sidebar-overlay-slide from-top" id="appMenu">
        <div className="row">
            <div className="col-xs-6 no-padding">
                <a href="#" className="p-l-40">
                <img src="/assets/img/demo/social_app.svg" alt="socail" /> </a>
            </div>
            <div className="col-xs-6 no-padding">
                <a href="#" className="p-l-10">
                <img src="/assets/img/demo/email_app.svg" alt="socail" /> </a>
            </div>
        </div>
        <div className="row">
            <div className="col-xs-6 m-t-20 no-padding">
                <a href="#" className="p-l-40">
                <img src="/assets/img/demo/calendar_app.svg" alt="socail" /> </a>
            </div>
            <div className="col-xs-6 m-t-20 no-padding">
                <a href="#" className="p-l-10">
                <img src="/assets/img/demo/add_more.svg" alt="socail" /> </a>
            </div>
        </div>
    </div>
    <div className="sidebar-header">
        <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" alt="Legion Training" className="brand" style={{height:"30px", marginTop:"0px"}} />
        <div className="sidebar-header-controls">
            <button type="button" className="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu">
                <i className="fa fa-angle-down fs-16"></i> 
            </button>
            <button type="button" className="btn btn-link visible-lg-inline" data-toggle-pin="sidebar">
                <i className="fa fs-12"></i> 
            </button>
        </div>
    </div>
    <div className="sidebar-menu">
        <ul className="menu-items">
            <li className="m-t-30 ">
                <img src="/img/legion-armour-with-sword.png" alt="Legion Training" className="brand" style={{width:"70%",margin:"auto",display:"block"}} />
                <h4 className="title-st1 text-center">RANK: LEGION RECRUIT</h4>
            </li>
            <li className="m-t-30 ">
                <Link exact={true} to="/" className="detailed"> 
                    <span className="title">Overview</span> 
                    <span className="details">Hey!</span> 
                </Link> 
                <span className="bg-success icon-thumbnail"><i className="fa fa-gears"></i></span> 
            </li>
            <li className="">
                <Link exact={true} to="/workout" className="detailed"> 
                    <span className="title">My Workout</span> 
                </Link> 
                <span className="icon-thumbnail">
                    <i className="fa fa-anchor"></i>
                </span> </li>
            <li className="">
                <Link exact={true} to="/nutrition" className="detailed"> 
                    <span className="title">My Nutrition</span> 
                </Link>
                <span className="icon-thumbnail">
                    <i className="fa fa-cutlery"></i>
                </span> 
            </li>
            <li className=""> 
                <a href="/dashboard/social"><span className="title">Social</span></a> 
                <span className="icon-thumbnail"><i className="pg-social"></i></span> 
            </li>
            <li> 
                <a href="/dashboard/calendar"><span className="title">Calendar</span></a>
                <span className="icon-thumbnail"><i className="pg-calender"></i></span>
            </li>
        </ul>
    <div className="clearfix"></div>
    </div>
</nav>

        );  
    }
}


export default connect((store) => {
    return {
        overview: store.overview
    }
})(Sidebar);

/*
<NotificationSystem ref="notificationSystem" />
*/