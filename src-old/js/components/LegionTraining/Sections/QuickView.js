import React, { Component } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"

class QuickView extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    render() {
        return ( 

        <div id="quickview" className="quickview-wrapper" data-pages="quickview">
            <ul className="nav nav-tabs">
                <li className=""> <a href="#quickview-notes" data-toggle="tab">Notes</a> </li>
                <li> <a href="#quickview-alerts" data-toggle="tab">Alerts</a> </li>
                <li className="active"> <a href="#quickview-chat" data-toggle="tab">Chat</a> </li>
            </ul> 
            <a className="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i className="pg-close"></i></a>
            
            <div className="tab-content">
                <div className="tab-pane fade  in no-padding" id="quickview-notes">
                    <div className="view-port clearfix quickview-notes" id="note-views">
                        <div className="view list" id="quick-note-list">
                            <div className="toolbar clearfix">
                                <ul className="pull-right ">
                                    <li> <a href="#" className="delete-note-link"><i className="fa fa-trash-o"></i></a> </li>
                                    <li> <a href="#" className="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i className="fa fa-plus"></i></a> </li>
                                </ul>
                                <button className="btn-remove-notes btn btn-xs btn-block hide"><i className="fa fa-times"></i> Delete</button>
                            </div>
                            <ul>
                                <li data-noteid="1">
                                    <div className="left">
                                        <div className="checkbox check-warning no-margin">
                                            <input id="qncheckbox1" type="checkbox" value="1" />
                                            <label for="qncheckbox1"></label>
                                        </div>
                                        <p className="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                    </div>
                                    <div className="right pull-right">
                                        <span className="date">12/12/14</span> 
                                        <a href="#" data-navigate="view" data-view-port="#note-views" data-view-animation="push">
                                            <i className="fa fa-chevron-right"></i>
                                        </a>
                                    </div>
                                </li>
                                <li data-noteid="2">
                                    <div className="left">
                                        <div className="checkbox check-warning no-margin">
                                            <input id="qncheckbox2" type="checkbox" value="1" />
                                            <label for="qncheckbox2"></label>
                                        </div>
                                        <p className="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                    </div>
                                    <div className="right pull-right">
                                        <span className="date">12/12/14</span> <a href="#"><i className="fa fa-chevron-right"></i></a>
                                    </div>
                                </li>
                                <li data-noteid="2">
                                    <div className="left">
                                        <div className="checkbox check-warning no-margin">
                                            <input id="qncheckbox3" type="checkbox" value="1" />
                                            <label for="qncheckbox3"></label>
                                        </div>
                                        <p className="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                    </div>
                                    <div className="right pull-right">
                                        <span className="date">12/12/14</span> <a href="#"><i className="fa fa-chevron-right"></i></a>
                                    </div>
                                </li>
                                <li data-noteid="3">
                                    <div className="left">
                                        <div className="checkbox check-warning no-margin">
                                            <input id="qncheckbox4" type="checkbox" value="1" />
                                            <label for="qncheckbox4"></label>
                                        </div>
                                        <p className="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                    </div>
                                    <div className="right pull-right">
                                        <span className="date">12/12/14</span> <a href="#"><i className="fa fa-chevron-right"></i></a>
                                    </div>
                                </li>
                                <li data-noteid="4">
                                    <div className="left">
                                        <div className="checkbox check-warning no-margin">
                                            <input id="qncheckbox5" type="checkbox" value="1" />
                                            <label for="qncheckbox5"></label>
                                        </div>
                                        <p className="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                    </div>
                                    <div className="right pull-right">
                                        <span className="date">12/12/14</span> <a href="#"><i className="fa fa-chevron-right"></i></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className="view note" id="quick-note">
                            <div>
                                <ul className="toolbar">
                                    <li><a href="#" className="close-note-link"><i className="pg-arrow_left"></i></a> </li>
                                    <li><a href="#" data-action="Bold"><i className="fa fa-bold"></i></a> </li>
                                    <li><a href="#" data-action="Italic"><i className="fa fa-italic"></i></a> </li>
                                    <li><a href="#" className=""><i className="fa fa-link"></i></a> </li>
                                </ul>
                                <div className="body">
                                    <div>
                                        <div className="top"> <span>21st april 2014 2:13am</span> </div>
                                        <div className="content">
                                            <div className="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="tab-pane fade no-padding" id="quickview-alerts">
                    <div className="view-port clearfix" id="alerts">
                        <div className="view bg-white">
                            <div className="navbar navbar-default navbar-sm">
                                <div className="navbar-inner">
                                    <a href="javascript:;" className="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax"> <i className="pg-more"></i> </a>
                                    <div className="view-heading"> Notications </div>
                                    <a href="#" className="inline action p-r-10 pull-right link text-master"> <i className="pg-search"></i> </a>
                                </div>
                            </div>
                            <div data-init-list-view="ioslist" className="list-view boreded no-top-border">
                                <div className="list-view-group-container">
                                    <div className="list-view-group-header text-uppercase"> Calendar </div>
                                    <ul>
                                        <li className="alert-list">
                                            <a href="javascript:;" className="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                                <p className="col-xs-height col-middle"> <span className="text-warning fs-10"><i className="fa fa-circle"></i></span> </p>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12"> <span className="text-master">David Nester Birthday</span> </p>
                                                <p className="p-r-10 col-xs-height col-middle fs-12 text-right"> <span className="text-warning">Today <br /></span> <span className="text-master">All Day</span> </p>
                                            </a>
                                        </li>
                                        <li className="alert-list">
                                            <a href="#" className="" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                                <p className="col-xs-height col-middle"> <span className="text-warning fs-10"><i className="fa fa-circle"></i></span> </p>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-9 overflow-ellipsis fs-12"> <span className="text-master">Meeting at 2:30</span> </p>
                                                <p className="p-r-10 col-xs-height col-middle fs-12 text-right"> <span className="text-warning">Today</span> </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="list-view-group-container">
                                    <div className="list-view-group-header text-uppercase"> Social </div>
                                    <ul>
                                        <li className="alert-list">
                                            <a href="javascript:;" className="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                                <p className="col-xs-height col-middle"> <span className="text-complete fs-10"><i className="fa fa-circle"></i></span> </p>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12"> <span className="text-master link">Jame Smith commented on your status<br /></span> <span className="text-master">“Perfection Simplified - Company Revox"</span> </p>
                                            </a>
                                        </li>
                                        <li className="alert-list">
                                            <a href="javascript:;" className="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                                <p className="col-xs-height col-middle"> <span className="text-complete fs-10"><i className="fa fa-circle"></i></span> </p>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12"> <span className="text-master link">Jame Smith commented on your status<br /></span> <span className="text-master">“Perfection Simplified - Company Revox"</span> </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="list-view-group-container">
                                    <div className="list-view-group-header text-uppercase"> Sever Status </div>
                                    <ul>
                                        <li className="alert-list">
                                            <a href="#" className="p-t-10 p-b-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                                <p className="col-xs-height col-middle"> <span className="text-danger fs-10"><i className="fa fa-circle"></i></span> </p>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12 overflow-ellipsis fs-12"> <span className="text-master link">12:13AM GTM, 10230, ID:WR174s<br /></span> <span className="text-master">Server Load Exceeted. Take action</span> </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-pane fade in active no-padding" id="quickview-chat">
                    <div className="view-port clearfix" id="chat">
                        <div className="view bg-white">
                            <div className="navbar navbar-default">
                                <div className="navbar-inner">
                                    <a href="javascript:;" className="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax"> <i className="pg-plus"></i> </a>
                                    <div className="view-heading"> Chat List
                                        <div className="fs-11">Show All</div>
                                    </div>
                                    <a href="#" className="inline action p-r-10 pull-right link text-master"> <i className="pg-more"></i> </a>
                                </div>
                            </div>
                            <div data-init-list-view="ioslist" className="list-view boreded no-top-border">
                                
                                <div className="list-view-group-container">
                                    <div className="list-view-group-header text-uppercase">c</div>
                                    <ul>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/4x.jpg" data-src="/assets/img/profiles/4.jpg" src="/assets/img/profiles/4x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">carole roberts</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/5x.jpg" data-src="/assets/img/profiles/5.jpg" src="/assets/img/profiles/5x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">christopher perez</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="list-view-group-container">
                                    <div className="list-view-group-header text-uppercase">d</div>
                                    <ul>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/6x.jpg" data-src="/assets/img/profiles/6.jpg" src="/assets/img/profiles/6x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">danielle fletcher</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/7x.jpg" data-src="/assets/img/profiles/7.jpg" src="/assets/img/profiles/7x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">david sutton</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="list-view-group-container">
                                    <div className="list-view-group-header text-uppercase">e</div>
                                    <ul>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/8x.jpg" data-src="/assets/img/profiles/8.jpg" src="/assets/img/profiles/8x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">earl hamilton</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/9x.jpg" data-src="/assets/img/profiles/9.jpg" src="/assets/img/profiles/9x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">elaine lawrence</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/1x.jpg" data-src="/assets/img/profiles/1.jpg" src="/assets/img/profiles/1x.jpg" className="col-top" />
                                                    </span>
                                                 </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">ellen grant</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                        <li className="chat-user-list clearfix">
                                            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" href="#"> 
                                                <span className="col-xs-height col-middle">
                                                    <span className="thumbnail-wrapper d32 circular bg-success">
                                                        <img width="34" height="34" alt="" data-src-retina="/assets/img/profiles/2x.jpg" data-src="/assets/img/profiles/2.jpg" src="/assets/img/profiles/2x.jpg" className="col-top" />
                                                    </span> 
                                                </span>
                                                <p className="p-l-10 col-xs-height col-middle col-xs-12"> <span className="text-master">erik taylor</span> <span className="block text-master hint-text fs-12">Hello there</span> </p>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="view chat-view bg-white clearfix">
                            <div className="navbar navbar-default">
                                <div className="navbar-inner">
                                    <a href="javascript:;" className="link text-master inline action p-l-10 p-r-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax"> <i className="pg-arrow_left"></i> </a>
                                    <div className="view-heading"> John Smith
                                        <div className="fs-11 hint-text">Online</div>
                                    </div>
                                    <a href="#" className="link text-master inline action p-r-10 pull-right "> <i className="pg-more"></i> </a>
                                </div>
                            </div>
                            <div className="chat-inner" id="my-conversation">
                                <div className="message clearfix">
                                    <div className="chat-bubble from-me"> Hello there </div>
                                </div>
                                <div className="message clearfix">
                                    <div className="profile-img-wrapper m-t-5 inline"> <img className="col-top" width="30" height="30" src="/assets/img/profiles/avatar_small.jpg" alt="" data-src="/assets/img/profiles/avatar_small.jpg" data-src-retina="/assets/img/profiles/avatar_small2x.jpg" /> </div>
                                    <div className="chat-bubble from-them"> Hey </div>
                                </div>
                                <div className="message clearfix">
                                    <div className="chat-bubble from-me"> Did you check out Pages framework ? </div>
                                </div>
                                <div className="message clearfix">
                                    <div className="chat-bubble from-me"> Its an awesome chat </div>
                                </div>
                                <div className="message clearfix">
                                    <div className="profile-img-wrapper m-t-5 inline"> <img className="col-top" width="30" height="30" src="/assets/img/profiles/avatar_small.jpg" alt="" data-src="/assets/img/profiles/avatar_small.jpg" data-src-retina="/assets/img/profiles/avatar_small2x.jpg" /> </div>
                                    <div className="chat-bubble from-them"> Yea </div>
                                </div>
                            </div>
                            <div className="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                                <div className="row">
                                    <div className="col-xs-1 p-t-15"> <a href="#" className="link text-master"><i className="fa fa-plus-circle"></i></a> </div>
                                    <div className="col-xs-8 no-padding">
                                        <input type="text" className="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something" /> </div>
                                    <div className="col-xs-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top"> <a href="#" className="link text-master"><i className="pg-camera"></i></a> </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        );  
    }
}


export default connect((store) => {
    return {
        products: store.products.products
    }
})(QuickView);

/*
<NotificationSystem ref="notificationSystem" />
*/