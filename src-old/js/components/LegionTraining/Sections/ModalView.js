import React, { Component } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"
import NotificationSystem from 'react-notification-system';

import { Modal, OverlayTrigger, popover, tooltip, Button } from "react-bootstrap"
import DatePicker from "react-datepicker"
import serialize from "form-serialize"
import qs from "qs"
import moment from 'moment';


class ModalView extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        //bodyStatistics,nutritionSubstitutes,currentWorkout
        //basicInfo,measurement,workoutInfo,workoutImages
        this.state = {
            open: { bodyStatistics : false, initialProgram:false },
            activeContent: { basicInfo:'active' },
            backdrop:true,
            close:true,
            data: {
                dob: moment().subtract(25, 'years').calendar(),
                unit: false
            }
        };
        
        this.addNotification = this.addNotification.bind(this);

        this.actionClick = this.actionClick.bind(this);
        this.actionNavClick = this.actionNavClick.bind(this);
        this.actionChangeSelect = this.actionChangeSelect.bind(this);
        this.actionChangeDateDOB = this.actionChangeDateDOB.bind(this);
        this.actionFormSubmit = this.actionFormSubmit.bind(this);
    
    }
    
    /*
     * Execute before mounting
     */
    componentDidMount() {

        const _this = this;

        _this.notificationSystem = _this.refs.notificationSystem;

        //document.body.addEventListener('click', this.myHandler);
    } 

    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        
        const _this = this;

        let { currentUser } = nextProps.index;

        let { measurement } = (currentUser) ? currentUser : { measurement:false };
            measurement = JSON.parse(measurement)

        /*
         * Open modal for basic required information for first log and new program
         */
        //if(!measurement) {
            _this.setState({
                open: { bodyStatistics : true, initialProgram:true },
                activeContent: { basicInfo:'active' },
                backdrop:'static',
                close:false 
            });
        //}

        console.log(measurement);
        
            currentUser = {...currentUser, measurement};
        let data = Object.assign(_this.state.data, currentUser);

        _this.setState({ data });

        //this.setState(nextProps.products);
        //console.log(currentUser,nextProps.index);
    }


    /*
     * Add notification 
     * @param data => object, must fill up required field
     */
    addNotification(data){
        this.notificationSystem.addNotification(data);
    }

    /*
     * Click
     */
    actionClick(e){

        switch(e.target.getAttribute('data-name')) {

            case 'program':

                const program = (!this.state.open.initialProgram) ? e.target.getAttribute('data-value') : 0;

                const measurement = {...this.state.data.measurement, program};

                const data = {...this.state.data, measurement};

                this.setState({data});
                
                break;

            default:
                return;

        }
    }

    /*
     * handle navigation click
     */
    actionNavClick(e){

        const activeContent = { [e.target.getAttribute('data-id')]:'active' }

        this.setState({ activeContent });

    }

    /*
     * On Change select field
     */
    actionChangeSelect(e) {

        switch(e.target.getAttribute('name')) {
            
            case 'unit':
                
                const unit = (e.target.value == 'metric') ? { weight : 'kg', height : 'cm' } : { weight : 'lb', height : 'in' };
                const data = {...this.state.data, unit };
                return this.setState({ data });
                break;

            default:
                
                return;

        }

    }

    /*
     * Change of date of birth
     */
    actionChangeDateDOB(date) {

        const data = {...this.state.data, dob: date};
        
        this.setState({ data });
    }

    /*
     * handle submit
     */
    actionFormSubmit(e){
        
        e.preventDefault();

        const _this = this;
        const _form = e.target;

        const program = (_this.state.data.measurement) ? _this.state.data.measurement.program : 0;
        const programStarted = (_this.state.data.measurement) ? _this.state.data.measurement.programStart : moment().format();
        //moment().format();  

        let _data = serialize(_form, { hash: true });
            _data = {..._data, program };
            _data = {..._data, programStarted };


        console.log(_data);

        _this.addNotification(
                    {
                        message: 'Hello there',
                        level: 'success',
                        position: 'tl'
                    }
                );

        /*axios.post('/', qs.stringify(_data, {arrayFormat:'brackets'})).then((response)=>{

            console.log(response);

        }).catch((error)=>{
            console.log(error);
        });*/
    
        //this.setState({ open:false });

    } 

    render() {

        console.log(this.props);

        const currentUser = (this.props.index) ? this.props.index.currentUser : false;

        let { measurement,userInfo,goals,programs } = {
            measurement:(currentUser) ? JSON.parse(currentUser.measurement) : { height: 0, weight: 0, units: 'metric' },
            userInfo: (currentUser) ? currentUser : [],
            goals: (this.props.index) ? this.props.index.getGoals : [],
            programs: (this.props.index) ? this.props.index.getPrograms : [],
        }
        
        let unit = (measurement.units == 'metric') ? { weight : 'kg', height : 'cm' } : { weight : 'lb', height : 'in' };
            unit = (!this.state.data.unit) ? unit : this.state.data.unit;

        
        return ( 

<div>

    {/*body statistics*/}
    <Modal  show={this.state.open.bodyStatistics} 
            onHide={()=>{this.setState({open:false})}}
            backdrop={this.state.backdrop}
            bsClass="stick-up modal" 
            dialogClassName="modal-dialog dark"
        >
        <div className="modal-content">
            <Modal.Header closeButton={this.state.close}>
                <Modal.Title bsClass="text-left" componentClass="h5">
                    Body Statistics
                </Modal.Title>
            </Modal.Header>
            <form ref={(input)=>{ this.formBodyStat = input}} onSubmit={this.actionFormSubmit}>
            <Modal.Body>
                <div className="panel">
                    <ul className="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
                        <li className={this.state.activeContent.basicInfo}>
                            <a style={{cursor:'pointer'}} onClick={this.actionNavClick} data-id="basicInfo">Basic Info</a>
                        </li>
                        <li className={this.state.activeContent.measurement}>
                            <a style={{cursor:'pointer'}} onClick={this.actionNavClick} data-id="measurement">Measurement</a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className={"tab-pane " + this.state.activeContent.basicInfo} id="basic-info">
                            <div className="row column-seperation">
                                <div className="col-md-12" style={{padding:'5px 7px'}}>
                                    <div className="form-group form-group-default">
                                        <label className="fade">Transformation Goal</label>
                                        
                                {/*GOALS*/}  
                                {(()=>{
                                    return  <select name="goal" className="form-control">
                                        {(()=>{
                                            return goals.map((v, i)=>{
                                                let checked = (parseInt(measurement.goal) == i) ? 'selected' : '';
                                                return <option selected={checked} value={i}>{v}</option>
                                            })
                                        })()}
                                            </select>
                                })()}
                                        
                                    </div>
                                 </div>
                                
                                <div className="col-md-6">
                                    <div className="form-group form-group-default input-group" style={{overflow:"visible"}}>
                                        <label>Date of birth:</label>
                                        <DatePicker selected={moment(this.state.data.dob)} 
                                                    onChange={this.actionChangeDateDOB} 
                                                    ref={(input) => this.date = input}
                                                    id="dob"
                                                    className="form-control date-picker" placeholderText="Select Date"/>
                                        <span className="input-group-addon" style={{background:"transparent"}}>
                                            <i className="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                    <div className="form-group form-group-default input-group">
                                        <label>Gender: </label>
                                        <select name="gender" className="form-control">
                                            {(()=>{
                                                return ['M','F'].map((v)=>{
                                                    let selected = (userInfo.gender == v) ? 'selected' : '';
                                                    return <option selected={selected}>{v}</option>
                                                })
                                            })()}
                                        </select>
                                        <span className="input-group-addon" style={{background:"transparent"}}>
                                            <i className="fa fa-user-circle"></i>
                                        </span>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="form-group form-group-default">
                                        <label>Unit: </label>
                                        <select className="form-control" name="unit" onChange={this.actionChangeSelect}>
                                            <option value="metric" selected={(measurement.units == 'metric') ? 'selected' : ''}>Metric (kg/cm)</option>
                                            <option value="english" selected={(measurement.units == 'english') ? 'selected' : ''} >English (lb/in)</option>
                                        </select>
                                    </div>
                                    <div className="form-group form-group-default input-group">
                                        <label>Weight: </label>
                                        <input type="text" name="weight" value={parseInt(measurement.weight)} className="form-control" placeholder="Enter Weight:" />
                                        <span className="input-group-addon" style={{background:"#8a8686",color:"#2b2b2b !important"}}>
                                            {unit.weight}
                                        </span>
                                    </div>
                                    <div className="form-group form-group-default input-group">
                                        <label>Height: </label>
                                        <input type="text" name="height" value={parseInt(measurement.height)} className="form-control" placeholder="Enter Height:" />
                                        <span className="input-group-addon" style={{background:"#8a8686",color:"#2b2b2b !important"}}>
                                            {unit.height}
                                        </span>
                                    </div>
                                </div>
                                

                                <h5 className="col-md-12 text-uppercase">Legion Program</h5>

                                {(()=>{

                                    //console.log(this.props.index, currentUser);
                                    if(this.props.index) {

                                        const _thisArray = this.props.index.getPrograms; 
                                        
                                        return _thisArray.map((v, i)=>{

                                                let active = '';
                                                if(this.state.data.measurement) {
                                                    active = (this.state.data.measurement.program == i) ? 'active' : active;    
                                                }
                                                
                                                return  <div className="col-md-4 program">
                                                            <div className={"form-group form-group-default " + active} onClick={this.actionClick} data-name="program" data-value={i} style={{minHeight:'60px',cursor:'pointer'}}>
                                                                <h6 style={{cursor:'default'}}>
                                                                    <span className="fa fa-file-image-o" style={{fontSize:"20px",float:"left",marginRight:"10px"}}></span> 
                                                                    {v}
                                                                </h6>
                                                            </div>
                                                        </div>
                                            })
                                    }
                                        
                                })()}

                            
                                <div className="col-md-12">
                                    <p className="pull-right" style={{margin:"20px -5px auto auto"}}>
                                        <a style={{cursor:'pointer'}} onClick={this.actionNavClick} data-id="measurement">NEXT</a>&nbsp;
                                        <button type="submit" className="btn btn-default btn-cons">SUBMIT</button>
                                    </p>
                                </div>
                                
                            </div>
                        </div>
                        <div className={"tab-pane " + this.state.activeContent.measurement} id="measurement">
                            <div className="row">
                                <div className="col-md-12" style={{padding:'0 8px'}}>
                                    <p>Enter your body measurements.</p>
                                </div>
                                <div className="col-md-4">
                                    
                                    {(()=>{
                                        const bodymeasure = {neck:'Neck',shoulder:'Shoulder',chest:'Chest',waist:'Waist'}
                                        return Object.keys(bodymeasure).map((v,i)=>{
                                            return  <div className="form-group form-group-default">
                                                        <label>{bodymeasure[v]}: </label>
                                                        <input type="text" name={v} placeholder={unit.height} className="form-control" />
                                                    </div>
                                        })
                                    })()}

                                </div>
                                <div className="col-md-4">

                                    {(()=>{
                                        const bodymeasure = {rightarm:'Right Arm',leftarm:'Left Arm',hips:'Hips',glutes:'Glutes'}
                                        return Object.keys(bodymeasure).map((v,i)=>{
                                            return  <div className="form-group form-group-default">
                                                        <label>{bodymeasure[v]}: </label>
                                                        <input type="text" name={v} placeholder={unit.height} className="form-control" />
                                                    </div>
                                        })
                                    })()}

                                </div>
                                <div className="col-md-4">
                                    
                                    {(()=>{
                                        const bodymeasure = {rightthigh:'Right Thigh',leftthigh:'Left Thigh',rightcalf:'Right Calf',leftcalf:'Left Calf'}
                                        return Object.keys(bodymeasure).map((v,i)=>{
                                            return  <div className="form-group form-group-default">
                                                        <label>{bodymeasure[v]}: </label>
                                                        <input type="text" name={v} placeholder={unit.height} className="form-control" />
                                                    </div>
                                        })
                                    })()}

                                </div>
                                <div className="col-md-12">
                                    <p className="pull-right" style={{margin:"20px -5px auto auto"}}>
                                        <button type="submit" className="btn btn-default btn-cons">SUBMIT</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal.Body>
            </form>
        </div>
    </Modal>

    {/*nutrition substitutes*/}
    <Modal  show={this.state.open.nutritionSubstitutes} 
            onHide={this.close}
            bsClass="stick-up modal" 
            dialogClassName="modal-dialog dark" 
        >
        <div className="modal-content">
            <div className="modal-header clearfix text-left">
                <h5 className="col-md-12">Substitution for - 100 g Mixed Berries</h5>
            </div>
            <form ref={(input)=>{ this.formNutritionSubstitutes = input}} onSubmit={this.actionFormSubmit}>
                <div className="modal-body">
                    <div className="col-md-12">
                        <table className="table text-uppercase">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><span className="fa fa-cutlery"></span> Pork Belly</td>
                                    <td>100gram</td>
                                </tr>
                                <tr>
                                    <td><span className="fa fa-cutlery"></span> Pork Belly</td>
                                    <td>100gram</td>
                                </tr>
                                <tr>
                                    <td><span className="fa fa-cutlery"></span> Pork Belly</td>
                                    <td>100gram</td>
                                </tr>
                            </tbody>
                        </table>
                        <h6 style={{lineHeight:"25px"}}>
                            Legion Training is all about 'Precision Nutrition' – where everything you see on your Meal Plan, 
                            the types of foods and the exact quantities are exactly what you need, taking into 
                            a account your current body composition and goals. The foods you have been prescribed 
                            are there because of their unique macronutrient and calorie makeup as well as how they 
                            interact and chemically work with your entire Meal Plan. Please understand your Meal Plan 
                            works as an entire context. Ingredients and individual meals are NOT to be viewed as separate.
                            All Meals and listed ingredients, work together as a whole to produce the nutrient delivery 
                            that you individually require over the course of a single day. What you have listed on your 
                            nutrition page is what s OPTIMAL for you right now, at this very moment in time. 
                            After 2 weeks the meal plan you have displayed will change and evolve based on your 
                            new measurements and stats that you input. Quantities and food combinations will change 
                            and evolve to whats optimal for you at that very time.
                        </h6>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </form>
        </div>
    </Modal>

        

    {/*current workout*/}
    <Modal  show={this.state.open.currentWorkout} 
            onHide={this.close}
            bsClass="slide-right modal" 
            dialogClassName="modal-dialog dark"
        >

        <div className="modal-content">
            <div className="modal-header clearfix text-left">
                <h5>Day 5 - Rest and recovery</h5>
            </div>
            <form ref={(input)=>{ this.formCurrentWorkout = input}} onSubmit={this.actionFormSubmit}>
                <div className="modal-body">
                    <div className="panel">
                        <ul className="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
                            <li className="active">
                                <a style={{cursor:'pointer'}} onClick={this.actionNavClick} data-id="workoutInfo">Workout Info</a>
                            </li>
                            <li>
                                <a style={{cursor:'pointer'}} onClick={this.actionNavClick} data-id="workoutImages">Workout Images</a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <div className={"tab-pane " + this.state.activeContent.workoutInfo} id="workout-info">
                                <div className="row column-seperation">
                                    <div className="col-md-12">
                                        <img src="https://www.bodybuilding.gr/images/articles/proponisi/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata_02.jpg" style={{width:"100%",margin:"10px 0"}} />
                                        <hr />
                                        <p className="text-justify">
                                            You’re putting all the hard work in at the gym and following your 
                                            nutritional plan to a tee but another factor that needs to be considered 
                                            is the importance of rest and recovery. How much is correct? And what’s 
                                            considered rest and recovery? Watch the video to find out. Don’t forget, 
                                            today you still need to perform your 45 minutes morning cardio.
                                        </p>
                                        <hr />
                                        <p><strong>Before you proceed to the next day, please complete the questions below</strong>
                                        </p>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group form-group-default">
                                            <label>Minutes:</label>
                                            <input type="text" className="form-control" placeholder="Enter total minutes rendered." />
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group form-group-default">
                                            <label>Weight:</label>
                                            <input type="text" className="form-control" placeholder="Intrument weights carried in lb/kg" />
                                        </div>

                                    </div>
                                    <p className="pull-right" style={{marginTop:"20px"}}>
                                        <input type="checkbox" name="completed" style={{position:"relative",top:"2px"}} /> I have completed this day &nbsp;&nbsp;
                                        <button type="submit" className="btn btn-default btn-cons text-uppercase" style={{marginRight:"15px"}}>Proceed to next day</button>
                                    </p>
                                </div>
                            </div>
                            <div className={"tab-pane " + this.state.activeContent.workoutImages} id="workout-images">
                                <div className="row">
                                    <div className="col-sm-4" style={{padding:"5px"}}><img src="https://www.bodybuilding.gr/images/articles/proponisi/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata_02.jpg" style={{border:"2px solid #7b7b7b",maxWidth:"100%",borderRadius:"3px"}} />
                                    </div>
                                    <div className="col-sm-4" style={{padding:"5px"}}><img src="https://www.bodybuilding.gr/images/articles/proponisi/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata_02.jpg" style={{border:"2px solid #7b7b7b",maxWidth:"100%",borderRadius:"3px"}} />
                                    </div>
                                    <div className="col-sm-4" style={{padding:"5px"}}><img src="https://www.bodybuilding.gr/images/articles/proponisi/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata_02.jpg" style={{border:"2px solid #7b7b7b",maxWidth:"100%",borderRadius:"3px"}} />
                                    </div>
                                    <div className="col-sm-4" style={{padding:"5px"}}><img src="https://www.bodybuilding.gr/images/articles/proponisi/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata/oi-sunthetes-askiseis-fernoun-suntheta-apotelesmata_02.jpg" style={{border:"2px solid #7b7b7b",maxWidth:"100%",borderRadius:"3px"}} />
                                    </div>
                                    <div className="col-md-12" style={{padding:"5px",marginTop:"20px"}}>
                                        <label>Enter your comments here:</label>
                                        <textarea className="form-control" style={{minHeight:"75px",background:"transparent",border:"2px solid #7b7b7b",borderRadius:"3px"}}>You’re putting all the hard work in at the gym and following your nutritional plan to a tee but another factor that needs to be considered is the importance of rest and recovery. </textarea>
                                        <p className="pull-right" style={{marginTop:"20px"}}>
                                            <button type="submit" className="btn btn-default btn-cons text-uppercase" style={{marginRight:"0"}}>Save Photos</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </Modal>
        
    <NotificationSystem ref="notificationSystem" />
</div>
        );  
    }
}


export default connect((store) => {
    return {
        index: store.index.payload
    }
})(ModalView);

/*
 * <NotificationSystem ref="notificationSystem" />
 {(()=>{
                                            return this.props.goals.map((v, i)=>{
                                                return <option value={i}>{v}</option>
                                            });
                                        })()}
 */