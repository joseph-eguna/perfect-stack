import React, { Component } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"

class Header extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    render() {
        return ( 

    <div className="header ">
        <div className="container-fluid relative">
            <div className="pull-left full-height visible-sm visible-xs">
                <div className="header-inner">
                    <a href="#" className="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                        
                    </a> 
                </div>
            </div>
            <div className="pull-center hidden-md hidden-lg">
                <div className="header-inner">
                    <div className="brand inline"> 
                        <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" alt="Legion Training" className="logo" style={{height:"60px",marginTop:"8px"}} />
                    </div>
                </div>
            </div>
            <div className="pull-right full-height visible-sm visible-xs">
                <div className="header-inner">
                    <a href="#" className="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview"> <span className="icon-set menu-hambuger-plus"></span> </a>
                </div>
            </div>
        </div>
        <div className=" pull-left sm-table hidden-xs hidden-sm">
            <div className="header-inner">
                <div className="brand inline">
                    <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" alt="Legion Training" className="logo" style={{height:"60px",marginTop:"8px"}} />
                </div>
                <a href="#" className="search-link" data-toggle="search">
                    <i className="pg-search"></i>Type anywhere to <span className="bold">search</span>
                </a> 
            </div>
        </div>

        <div className=" pull-right">
            <div className="header-inner">
                <a href="#" style={{color:"#fff",fontSize:"25px",margin:"10px 12px 0px 20px"}} className="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview">
                    <span className="fa fa-tasks"></span>
                </a>
            </div>
        </div>
        <div className=" pull-right">
            <div className="visible-lg visible-md m-t-10">
                <div className="pull-left p-r-10 p-t-10 fs-16 font-heading text-uppercase"> 
                    <span className="semi-bold">Joseph Eguna</span>
                </div>
                <div className="dropdown pull-right">
                    <button className="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                        <span className="thumbnail-wrapper d32 circular inline m-t-5">
                            <img src="/assets/img/profiles/avatar.jpg" alt="" data-src="/assets/img/profiles/avatar.jpg" data-src-retina="/assets/img/profiles/avatar_small2x.jpg" width="32" height="32" />
                        </span> 
                    </button>
                    <ul className="dropdown-menu profile-dropdown" role="menu">
                        <li><a href="#"><i className="pg-settings_small"></i> Settings</a> </li>
                        <li><a href="#"><i className="pg-outdent"></i> Feedback</a> </li>
                        <li><a href="#"><i className="pg-signals"></i> Help</a> </li>
                        <li className="bg-master-lighter">
                            <a href="/logout" className="clearfix"> <span className="pull-left">Logout</span> <span className="pull-right"><i className="pg-power"></i></span> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        );  
    }
}


export default connect((store) => {
    return {
        overview: store.overview
    }
})(Header);

/*
<NotificationSystem ref="notificationSystem" />
*/