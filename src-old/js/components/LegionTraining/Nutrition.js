import React, { Component } from "react"
import { connect } from "react-redux"

import Header from "./Sections/Header";
import Overlay from  "./Sections/Overlay";
import QuickView from "./Sections/QuickView";
import ModalView from "./Sections/ModalView";

class Nutrition extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
        
    }
    
    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    render() {
        return ( 

<div>
    <div className="page-container ">
        
        <Header />
        
        {/***NUTRITION CONTENT***/ }
        <div className="page-content-wrapper ">
            <div className="content sm-gutter">
                <div className="container-fluid padding-25 sm-padding-10">
                    <div className="row">
                        <h4 className="col-md-12 text-white text-uppercase">Nutrition</h4>
                    </div>
                </div>
            </div>
        </div>

        <QuickView />
        
        <Overlay />

        <ModalView />
    </div>
</div>
         );  
    }
}


export default connect((store) => {
    return {
        products: store.products.products
    }
})(Nutrition);

/*
<NotificationSystem ref="notificationSystem" />
*/