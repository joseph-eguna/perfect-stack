import React, { Component } from "react"
import { connect } from "react-redux"
import axios from "axios"

import { getOverview } from "../../actions/overviewActions"

import Header from "./Sections/Header";
import Overlay from  "./Sections/Overlay";
import QuickView from "./Sections/QuickView";
import ModalView from "./Sections/ModalView";

class Overview extends Component {
    
    notificationSystem: null

    constructor() {
        super();

        this.state = {
        };
            
        this.actionClick = this.actionClick.bind(this);
    }
    
   
    /*
     * Dispatch initially the list of props from redux
     */
    componentWillMount() {
        this.props.dispatch(getOverview());
    }


    /*
     * Extracting props and set it to component state
     */
    componentWillReceiveProps(nextProps) {
        //const { products, countProducts, limitProducts, changeState } = nextProps;
        //this.setState(nextProps.products);
    }

    /*
     * handle click
     */
    actionClick(e){
        //console.log($('body-statistics'));
        //console.log(e.target);
    }

    /*
     * handle submit
     */
    actionFormSubmit(e){
        //console.log($('body-statistics'));
        //console.log(e.target);
    }    

    render() {

        //console.log(this.props);

        return ( 

<div>
    <div className="page-container ">
        
        <Header />

{ /***OVERVIEW CONTENT***/ }
<div className="page-content-wrapper ">
    <div className="content sm-gutter">
        <div className="container-fluid padding-25 sm-padding-10">
            <div className="row">
                <h4 className="col-md-12 text-white text-uppercase">STATUS</h4>
                <div className="col-md-12">
                    <div className="panel panel-default dark">
                        <div className="panel-heading">
                            <div className="panel-title">Program - Pre Conditioning - day 5</div>
                            <div className="panel-controls">
                                <ul>
                                    <li>
                                        <a href="#" className="body-statistics" onClick={this.actionClick} >
                                            <i className="fa fa-edit"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="panel-body">

                            <div className="col-md-8">
                                <p className="small hint-text">65% remaining</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                        65%
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-1 text-center box">
                                <h5 className="text-uppercase">115</h5>
                                <h6 style={{fontSize:"9px",margin:"0"}}>WEIGHT/KG</h6>
                            </div>
                            <div className="col-md-1 text-center box">
                                <h5>175</h5>
                                <h6 style={{fontSize:"9px",margin:"0"}}>HEIGHT/CM</h6>
                            </div>
                            <div className="col-md-1 text-center box">
                                <h5>47</h5>
                                <h6 style={{fontSize:"9px",margin:"0"}}>AGE</h6>
                            </div>
                            <div className="col-md-1 text-center box">
                                <h5>M</h5>
                                <h6 style={{fontSize:"9px",margin:"0"}}>GENDER</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <h4 className="col-md-12 text-white text-uppercase">Statistics on program on your current program</h4>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading">
                            <div className="panel-title">Calories burned</div>
                        </div>
                        <div className="panel-body">

                            <h2 className="title-icon-big"><span className="fa fa-free-code-camp"></span></h2>
                            <div className="progress">
                                <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                    70%
                                </div>
                            </div>

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>720</span> <br />calories burned
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading">
                            <div className="panel-title">Cadio minutes</div>
                        </div>
                        <div className="panel-body">

                            <h2 className="title-icon-big"><span className="fa fa-heartbeat"></span></h2>
                            <div className="progress">
                                <div className="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                    90
                                </div>
                            </div>

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>90</span> <br />Minutes Completed
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading">
                            <div className="panel-title">Workout Sessions</div>
                        </div>
                        <div className="panel-body">

                            <h2 className="title-icon-big"><span className="fa fa-shield"></span></h2>
                            <div className="progress">
                                <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                    2
                                </div>
                            </div>

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>2</span> <br />Sessions completed
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading">
                            <div className="panel-title">Pre Conditioning</div>
                        </div>
                        <div className="panel-body">

                            <h2 className="title-icon-big"><span className="fa fa-tasks"></span></h2>
                            <div className="progress">
                                <div className="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{width:"70%"}}>
                                    29%
                                </div>
                            </div>

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>29%</span> <br />Completed
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <h4 className="col-md-12 text-white text-uppercase">General Statistics Since Day 1</h4>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading  text-center">
                            <div className="panel-title">Totla calories burned</div>
                        </div>
                        <div className="panel-body">

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>1020</span> <br />
                              Total Calories Burned
                                Since the beginning of your Legion journey
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading  text-center">
                            <div className="panel-title">Totla calories burned</div>
                        </div>
                        <div className="panel-body">

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>1020</span> <br />
                              Total Calories Burned
                                Since the beginning of your Legion journey
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading  text-center">
                            <div className="panel-title">Totla calories burned</div>
                        </div>
                        <div className="panel-body">

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>1020</span> <br />
                              Total Calories Burned
                                Since the beginning of your Legion journey
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading  text-center">
                            <div className="panel-title">Totla calories burned</div>
                        </div>
                        <div className="panel-body">

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>1020</span> <br />
                              Total Calories Burned
                                Since the beginning of your Legion journey
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="panel panel-default dark">
                        <div className="panel-heading  text-center">
                            <div className="panel-title">Totla calories burned</div>
                        </div>
                        <div className="panel-body">

                            <h6 className="text-center text-uppercase">
                              <span style={{fontSize:"30px"}}>1020</span> <br />
                              Total Calories Burned
                                Since the beginning of your Legion journey
                            </h6>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
            </div>
        </div>
    </div>
</div>
{ /***END OVERVIEW***/ }

        <QuickView />

        <Overlay />

        <ModalView />

    </div>
</div>
         );  
    }
}


export default connect((store) => {
    return {
        index: store.index.payload,
        overview: store.overview.payload
    }
})(Overview);

/*
<NotificationSystem ref="notificationSystem" />
*/