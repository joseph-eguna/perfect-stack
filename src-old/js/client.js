
/*import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux"

//import ShopProducts from "./components/ShopProducts";
import store from "./store"
import Layout from "./components/Layout";

const app = document.getElementById('shop-products-wrapper');

ReactDOM.render(<Provider store={store}>
					<Layout />
				</Provider>, app);*/




import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import reducers from './reducer';
import Layout from "./components/Layout";
import ShopProducts from "./components/ShopProducts";

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

ReactDOM.render(
  	<Provider store={createStoreWithMiddleware(reducers)}>
    	<ShopProducts />
  	</Provider>,
  	document.getElementById('shop-products-wrapper')
);




/*
<ShopProducts token={app.getAttribute('form-csrf')}
*/