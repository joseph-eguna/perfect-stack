export default function reducer(state={
	orders : [],
	fetching : false,
	fetched : false,
	error: null
}, action) {

	switch(action.type) {
		case "FETCH_ORDERS": {
			return {...state, fetching:true}
		}
		case "FETCH_ORDERS_REJECTED": {
			return {...state, fetching:false, error:action.payload}
		}
		case "FETCH_ORDERS_FULLFILLED": {
			return {...state, fetching:false, fetched:true, orders:action.payload}
		}
		default: 
      		return state;
	}
}