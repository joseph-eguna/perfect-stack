import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import reducers from './reducer/perfect-stack-admin';
import PerfectStackAdmin from "./components/perfectStackAdmin";

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

ReactDOM.render(
  	<Provider store={createStoreWithMiddleware(reducers)}>
    	<PerfectStackAdmin />
  	</Provider>,
  	document.getElementById('product-stack-admin')
);