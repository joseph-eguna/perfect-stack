//var debug = process.env.NODE_ENV == "production";
//var debug = false;
var webpack = require('webpack');
var path = require('path');


/*var WooCommerceAPI = require('woocommerce-api');
 
var WooCommerce = new WooCommerceAPI({
  url: "http://local.nastyjuice.co.nz",
  consumerKey: "ck_f1e6d8cce32aeb513b47e9663135dc16738ed165",
  consumerSecret: "cs_c2b64dc53f730e351fe8c8042bf579554b0dee18",
});*/

/*WooCommerce.get('products', function(err, data, res) {
      console.log(res);
    });*/

const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  context: path.join(__dirname, "src"),
  devtool: false, //debug ? "inline-sourcemap" : false,
  entry: "./js/index.js",

  devServer: {
    contentBase: path.join(__dirname, 'shop.app'),
    compress: true,
    port: 8081,
    historyApiFallback: true
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0'],
          plugins: ['react-html-attrs', 'transform-decorators-legacy', 'transform-class-properties'],
        }
      }
    ]
  },
  output: {
    path: __dirname + "../../react.development/shop.app/",
    filename: "js/shop-app.js",
    publicPath: '/'
  },
  plugins: [
    new webpack.DefinePlugin({
      __WcAPI__ : JSON.stringify({
        url: 'http://liqu1dlab.staging.wpengine.com',
        key: 'ck_3c62b9ba464858685ef0176a57c12ca79f1ae1ba',
        sec: 'cs_4c26c8666923ac5fcdf2cbaa0b2454c997088499'
      })
    })
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
			sourceMap: true
		  })
    ]
  }
  /*JSON.stringify(WooCommerce)*/
  /*plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: true }),
  ],*/
};